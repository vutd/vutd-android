package io.vutd.agenda.controller.activity;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.digits.sdk.android.AuthCallback;
import com.digits.sdk.android.DigitsException;
import com.digits.sdk.android.DigitsSession;
import com.google.firebase.iid.FirebaseInstanceId;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Extra;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.handler.UserRegisterHandler;
import io.vutd.agenda.rest.request.UserRegisterRequest;
import io.vutd.agenda.rest.response.UserRegisterResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.Utils;
import io.vutd.agenda.view.dialog.OperationResultProgressDialog;
import io.vutd.agenda.view.widget.VUTDDigitsAuthButton;

/**
 * Created by andresmariscal on 20/08/16.
 */
@EActivity(R.layout.activity_phone_verification)
public class PhoneVerificationActivity extends AbstractFormActivity<UserRegisterResponse>
        implements AuthCallback {

    public static final String TAG = "PhoneVerificationActivity";

    @Extra
    String mFbId;

    @Extra
    String mName;

    @Extra
    String mLastName;

    @Extra
    String mBirthday;

    @Extra
    String mEmail;

    @Extra
    int mGender;

    private String mPassword;

    @ViewById(R.id.tool_bar_title)
    TextView mToolBarTitle;

    @ViewById(R.id.phone_verification_email)
    EditText mEmailET;

    @ViewById(R.id.phone_verification_password)
    EditText mPasswordET;

    @ViewById(R.id.phone_verification_confirm_password)
    EditText mConfirmPassword;

    @ViewById(R.id.email_layout)
    View mEmailLayout;

    @ViewById(R.id.password_layout)
    View mPasswordLayout;

    @ViewById(R.id.confirm_password_layout)
    View mConfirmPasswordLayout;

    @Bean
    UserRegisterHandler mUserRegisterHandler;

    @Bean
    UserRegisterRequest mRequest;

    @ViewById(R.id.auth_button)
    VUTDDigitsAuthButton mDigitsAuthButton;

    @ViewById(R.id.phone_verification_create_account)
    Button mCreateAccounButton;

    private boolean isRequestCodeSend = false;
    private String mPhone;
    private OperationResultProgressDialog mProgress;

    @Override
    public void afterViews() {
        super.afterViews();
        mDigitsAuthButton.setCallback(this);
        mToolBarTitle.setText(R.string.phone_verification_title);

        if (!isRequestCodeSend) {
            mCreateAccounButton.setText(R.string.phone_verification_request_verification_code);
        }

        if (mFbId != null) {
            mEmailET.setVisibility(View.GONE);
            mPasswordET.setVisibility(View.GONE);
            mConfirmPassword.setVisibility(View.GONE);
        }
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mUserRegisterHandler.setHandlerResult(this);
        mProgress = new OperationResultProgressDialog(this);
    }

    @Click(R.id.navigation_back)
    public void onBackPressNavigation(){
        onBackPressed();
    }

    @Click(R.id.phone_verification_create_account)
    public void onCreateAccountClick() {
        //I don't really like this validation but i won't spend more time on this
        if (mFbId == null) {
            if (mEmailET.getText().toString().isEmpty()) {
                mEmailET.setError(getString(Rule.EMAIL));
                return;
            }
            if (!Utils.validateEmailFormat(mEmailET.getText().toString())) {
                mEmailET.setError(getString(Rule.INVALID_EMAIL));
                return;
            }
            if (mPasswordET.getText().toString().isEmpty()) {
                mPasswordET.setError(getString(Rule.PASSWORD));
                return;
            }
            if (!mConfirmPassword.getText().toString().equals(mPasswordET.getText().toString())) {
                mConfirmPassword.setError(getString(Rule.CONFIRM_PASSWORD));
                return;
            }
            mEmail = mEmailET.getText().toString();
            mPassword = mPasswordET.getText().toString();
        }
        mProgress.show();
        mDigitsAuthButton.onClick(mDigitsAuthButton);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mProgress.isShowing())
            mProgress.dismiss();
    }

    @Override
    public Map<EditText, List<Rule>> attachRules() {
        HashMap<EditText, List<Rule>> rules = new HashMap<>();
        ArrayList<Rule> emailRules = new ArrayList<>();
        emailRules.add(new Rule(Rule.EMAIL));
        emailRules.add(new Rule(Rule.INVALID_EMAIL));
        emailRules.add(new Rule(Rule.EMAIL_UNIQUE));
        rules.put(mEmailET, emailRules);

        ArrayList<Rule> passwordRules = new ArrayList<>();
        passwordRules.add(new Rule(Rule.PASSWORD));
        rules.put(mPasswordET, passwordRules);

        ArrayList<Rule> confirmPasswordRules = new ArrayList<>();
        confirmPasswordRules.add(new Rule(Rule.CONFIRM_PASSWORD));
        rules.put(mConfirmPassword, confirmPasswordRules);
        return rules;
    }

    @Override
    public void okResult(UserRegisterResponse result) {
        Logger.d(TAG, "okResult()");
        //Fabric Answers
        if (mFbId != null) {
            //Vutd Answers
            VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.FACEBOOK, true);
        }
        else {
            //Vutd Answers
            VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.EMAIL, true);
        }

        mRealm.beginTransaction();
        mRealm.delete(CurrentUser.class);
        CurrentUser currentUser = mRealm.createObject(CurrentUser.class);
        currentUser.name = mName;
        currentUser.lastName = mLastName;
        currentUser.roleId = 5;
        currentUser.email = mEmail;
        currentUser.gender = mGender == 0 ? "M" : "F";
        currentUser.phone = mPhone;
        currentUser.sessionId = result.getSessionId();
        if (mFbId != null)
            currentUser.facebookId = mFbId;
        mRealm.commitTransaction();

        mProgress.showSuccessfulResult();
        MainActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .start();

    }

    @Override
    public void badResult() {
        Logger.d(TAG, "badResult()");
        mProgress.showBadResult();
        //Fabric Answers
        if (mFbId != null) {
            //Vutd Answers
            VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.FACEBOOK, false);
        }
        else {
            //Vutd Answers
            VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.EMAIL, false);
        }
    }

    @Override
    public void success(DigitsSession session, String phoneNumber) {
        if (isRequestCodeSend) {
            mProgress.show();
            mPhone = phoneNumber;
            if (mFbId != null) {
                mRequest.setFacebookToken(mFbId);

                //Vutd Answers
                VutdAnswers.logEvent(VutdAnswers.EVENT.DIGITS,
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FROM, "facebook"),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));

            }
            else {
                mRequest.setPassword(mPassword);

                //Vutd Answers
                VutdAnswers.logEvent(VutdAnswers.EVENT.DIGITS,
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FROM, "email_password"),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));

            }
            mRequest.setRoleId("5");//TODO: PATIENT Change for a constant
            mRequest.setBirthday(mBirthday);
            mRequest.setName(mName);
            mRequest.setLastName(mLastName);
            mRequest.setGender(mGender == 0 ? "M" : "F");
            mRequest.setEmail(mEmail);
            mRequest.setPhone(mPhone);
            mRequest.setDeviceOs("Android " + Build.VERSION.SDK_INT);
            mRequest.setDeviceName(Utils.getDeviceName());
            mRequest.setDeviceModel(Build.MODEL);
            mRequest.setPushNotificationToken(FirebaseInstanceId.getInstance().getToken());
            mUserRegisterHandler.registerUser(mRequest);
        } else {

            if (mEmailLayout.getVisibility() != View.GONE)
                mEmailLayout.setVisibility(View.GONE);
            if (mPasswordLayout.getVisibility() != View.GONE)
                mPasswordLayout.setVisibility(View.GONE);
            if (mConfirmPasswordLayout.getVisibility() != View.GONE)
                mConfirmPasswordLayout.setVisibility(View.GONE);

            mCreateAccounButton.setText(R.string.phone_verification_create_account);
            isRequestCodeSend = true;
            mProgress.dismiss();
        }
    }

    @Override
    public void failure(DigitsException error) {
        Logger.e(TAG, "DigitsException", error.getCause());
        mProgress.showBadResult();

        if (mFbId != null) {
            //Vutd Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.DIGITS,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FROM, "facebook"),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "failure"));
        }
        else {
            //Vutd Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.DIGITS,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FROM, "email_password"),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "failure"));
        }
    }
}
