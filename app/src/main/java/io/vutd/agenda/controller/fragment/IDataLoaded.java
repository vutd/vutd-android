package io.vutd.agenda.controller.fragment;

/**
 * Created by andresmariscal on 09/07/16.
 */
public interface IDataLoaded {
    void onDataLoaded();
    void onDataLoading();
}
