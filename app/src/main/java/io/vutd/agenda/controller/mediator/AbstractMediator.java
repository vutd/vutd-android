package io.vutd.agenda.controller.mediator;

import android.content.Context;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import io.realm.Realm;
import io.realm.exceptions.RealmException;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.dialog.SimpleDialog;

/**
 * Created by andresmariscal on 08/07/16.
 */
@EBean
public abstract class AbstractMediator {

    @Bean
    SimpleDialog mSimpleDialog;

    protected Context mContext;
    protected Realm mRealm;
    protected CurrentUser mCurrentUser;
    protected boolean isUserSignedIn;
    protected boolean isAsyncActive = false;
    protected static final int ADMIN        = 1;
    protected static final int SALES_MAN    = 2;
    protected static final int DOCTOR       = 3;
    protected static final int MC_USER      = 4;
    protected static final int PATIENT      = 5;

    public abstract String getTag();
    protected abstract void defineFlow();

    public void disconnectAsyncTasks(){
        isAsyncActive = false;
    }

    public String getCurrentUserName(){
        if(mCurrentUser != null){
            return mCurrentUser.name + " " + mCurrentUser.lastName;
        }
        return "";
    }

    public void init(){
        Logger.d(getTag(), "init()");
        try{
            if(mContext != null)
                mSimpleDialog.init(mContext);
            mCurrentUser = mRealm.where(CurrentUser.class).findFirst();
            isUserSignedIn = mCurrentUser != null;
            isAsyncActive = true;
            defineFlow();
        } catch (RealmException e){
            Logger.e(getTag(), "RealmException: " + e.getMessage(), e.getCause());
        } catch (Exception e ){
            Logger.e(getTag(), "Exception: " + e.getMessage(), e.getCause());
        }
    }

    @AfterInject
    public void afterInjects(){
        Logger.d(getTag(), "afterInjects");
    }

    public Context getContext() {
        return mContext;
    }

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public Realm getRealm() {
        return mRealm;
    }

    public void setRealm(Realm mRealm) {
        this.mRealm = mRealm;
    }
}
