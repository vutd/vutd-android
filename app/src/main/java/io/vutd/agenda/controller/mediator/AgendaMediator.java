package io.vutd.agenda.controller.mediator;

import android.content.DialogInterface;
import android.view.MenuItem;
import android.widget.Toast;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.eventbus.DoubleCheck;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.realm.model.Workday;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.rest.common.AppointmentCommon;
import io.vutd.agenda.rest.common.WorkdayCommon;
import io.vutd.agenda.rest.handler.CreateAppointmentHandler;
import io.vutd.agenda.rest.handler.DeleteAppointmentHandler;
import io.vutd.agenda.rest.handler.GetDoctorScheduleByMCHandler;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.handler.SendConfirmationRequestHandler;
import io.vutd.agenda.rest.handler.SubscribeAppointmentHandler;
import io.vutd.agenda.rest.request.CreateAppointmentRequest;
import io.vutd.agenda.rest.request.DeleteAppointmentRequest;
import io.vutd.agenda.rest.request.GetDoctorScheduleByMCRequest;
import io.vutd.agenda.rest.request.SendConfirmationRequest;
import io.vutd.agenda.rest.request.SubscribeAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.rest.response.CreateAppointmentResponse;
import io.vutd.agenda.rest.response.GetDoctorScheduleByMCResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.VUTDDateTimeUtils;
import io.vutd.agenda.view.adapter.DaySelectorAdapter;
import io.vutd.agenda.view.dialog.CreateAppointmentDialog;
import io.vutd.agenda.view.dialog.OperationResultProgressDialog;

/**
 * Created by andresmariscal on 10/07/16.
 */
@EBean
public class AgendaMediator extends AbstractMediator {

    @Bean
    GetDoctorScheduleByMCHandler mGetDoctorScheduleByMCHandler;

    @Bean
    DeleteAppointmentHandler mDeleteAppointmentHandler;

    @Bean
    CreateAppointmentHandler mCreateAppointmentHandler;

    @Bean
    SubscribeAppointmentHandler mSubscribeAppointmentHandler;

    @Bean
    SendConfirmationRequestHandler mSendConfirmationRequestHandler;

    public static final String TAG = "AgendaMediator";
    private final int TOTAL_DAYS = 30;
    private long mDoctorId;
    private long mMedicalCoreId;
    private OperationResultProgressDialog mProgress;

    private IPaintAgenda mOnPaintAgenda;
    private IAppointmentListener mAppointmentListener;
    private boolean mMultiSelectionMode = false;

    @Override
    public String getTag() {
        return TAG;
    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    //Temporal
    public boolean isPatient() {
        return mCurrentUser.roleId == PATIENT;
    }
    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    //TODO: remove this

    /**
     * The doctor id only will be filled when the current user is a patient
     *
     * @param doctorId the doctor id.
     */
    public void setDoctorId(long doctorId) {
        this.mDoctorId = doctorId;
    }

    //TODO: remove this

    /**
     * The medical core id only will be filled when the current user is a patient
     *
     * @param medicalCoreId the medical core id.
     */
    public void setMedicalCoreId(long medicalCoreId) {
        this.mMedicalCoreId = medicalCoreId;
    }

    public void setIPaintAgenda(IPaintAgenda mOnPaintAgenda) {
        this.mOnPaintAgenda = mOnPaintAgenda;
    }

    public void setAppointmentListener(IAppointmentListener mAppointmentListener) {
        this.mAppointmentListener = mAppointmentListener;
    }

    public void setMultiSelectionMode(boolean selectionMode) {
        this.mMultiSelectionMode = selectionMode;
        onMultiSelectionStateChange();
    }

    //region Initializer. The entry point of the mediator
    @Override
    public void afterInjects() {
        super.afterInjects();
        mDeleteAppointmentHandler.setHandlerResult(mDeleteAppointmentCallback);
        mCreateAppointmentHandler.setHandlerResult(mCreateAppointmentCallback);
        mGetDoctorScheduleByMCHandler.setHandlerResult(mGetDoctorScheduleByMCCallback);
        mSubscribeAppointmentHandler.setHandlerResult(mSubscribeAppointmentCallback);
        mSendConfirmationRequestHandler.setHandlerResult(mSendConfirmationRequestCallback);
    }

    public void onStart() {
        EventBus.getDefault().register(this);
    }

    public void onStop() {
        EventBus.getDefault().unregister(this);
    }

    /************************************************************************
     * Agenda Initializer
     ************************************************************************/

    @Override
    protected void defineFlow() {
        mProgress = new OperationResultProgressDialog(mContext);
        List<DaySelectorAdapter.DaySelector> workDays;
        Doctor doctor;
        switch (mCurrentUser.roleId) {
            case DOCTOR:
                doctor = mRealm.where(Doctor.class).findFirst();
                workDays = retrieveRangeDays(mCurrentUser.reference, TOTAL_DAYS);
                mOnPaintAgenda.onDaysLoaded(workDays);
                mOnPaintAgenda.onDoctorLoaded(doctor);
                showAppointments(doctor.id, workDays.get(0).getDate());
                break;

            case PATIENT:
                doctor = mRealm.where(Doctor.class)
                        .equalTo("id", mDoctorId)
                        .findFirst();
                mOnPaintAgenda.onDoctorLoaded(doctor);
                retrieveDoctorSchedule(mDoctorId, mMedicalCoreId);
                break;
        }
    }

    /************************************************************************
     * Draw Agenda
     ************************************************************************/

    /**
     * Show appointment base on the current user
     *
     * @param dateTime Specify the day that you want to retrieve the schedule
     */
    public void showAppointments(long doctorId, DateTime dateTime) {
        retrieveScheduleByDoctor(doctorId, dateTime);
    }

    /**
     * Retrieve the all day of work according to <p>@param workday</p>
     *
     * @return schedule according to the specific work day
     */
    private void retrieveScheduleByDoctor(final long doctorId, final DateTime dateTime) {
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Appointment.debugTable(realm.where(Appointment.class).findAll());
                RealmResults<Appointment> temporalAppointments = realm.where(Appointment.class)
                        .equalTo("id", -1)
                        .findAll();
                temporalAppointments.deleteAllFromRealm();

                //Clear Dirty data
                RealmResults<Appointment> dirtyAppointments = realm.where(Appointment.class)
                        .equalTo("isDirty", 1)
                        .findAll();
                for (Appointment appointment : dirtyAppointments) {
                    appointment.isDirty = 0;
                }

                //Retrieve the doctor's workdays
                int day = dateTime.getDayOfWeek();
                RealmResults<Workday> workdays = realm.where(Workday.class)
                        .equalTo("day", day)
                        .equalTo("doctorId", doctorId)
                        .findAll();

                //We iterate through the doctor's workdays to search for booked appointments in the all day
                //if we find one then we marked'em as dirty in order to return the view-model
                //list, if not we create a temporal and dirty appointment
                for (Workday wd : workdays) {
                    int appointmentDuration = VUTDDateTimeUtils.getMinuteOfDay(wd.appointmentDuration);
                    DateTime currentHour = VUTDDateTimeUtils.parseForPattern(
                            wd.startTime, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT);
                    DateTime endTime = VUTDDateTimeUtils.parseForPattern(
                            wd.endTime, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT);

                    //If rest times are 00:00 this means it will be midnight, so the while will never reach it.
                    final DateTime restStartTime = VUTDDateTimeUtils.parseForPattern(
                            wd.restStartTime, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT);
                    final DateTime restEndTime = VUTDDateTimeUtils.parseForPattern(
                            wd.restEndTime, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT);

                    //Until the counterTime reach the end of the doctor's day
                    while (currentHour.isBefore(endTime)) {
                        Appointment appointment = realm.where(Appointment.class)
                                .equalTo("date", VUTDDateTimeUtils.convertToVUTDDateFormat(dateTime))
                                .equalTo("startTime", VUTDDateTimeUtils.parseForPattern(
                                        currentHour, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT))
                                .findFirst();

                        if (appointment != null) {
                            appointment.isDirty = 1;
                            currentHour = currentHour.plusMinutes(appointmentDuration);
                            continue;
                        } else {
                            appointment = realm.createObject(Appointment.class);
                            appointment.id = -1;
                            appointment.isDirty = 1;
                            appointment.startTime = VUTDDateTimeUtils.parseForPattern(
                                    currentHour, VUTDDateTimeUtils.VUTD_MILITARY_TIME_FORMAT);
                            appointment.selectionMode = mMultiSelectionMode ? 1 : 0;
                        }

                        //Depending on the time assign whether is free or rest time
                        if (restStartTime.isEqual(currentHour) ||
                                (currentHour.isAfter(restStartTime) && currentHour.isBefore(restEndTime))) {
                            appointment.bookTypeId = Appointment.REST_TIME;
                        } else {
                            appointment.bookTypeId = Appointment.FREE;
                        }

                        appointment.doctorId = wd.doctorId;
                        appointment.medicalCoreId = wd.medicalCoreId;

                        currentHour = currentHour.plusMinutes(appointmentDuration);
                    }
                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                ArrayList<Appointment> appointments = new ArrayList<>();
                appointments.addAll(mRealm.where(Appointment.class)
                        .equalTo("isDirty", 1)
                        .findAll()
                        .sort("startTime", Sort.ASCENDING));
                mOnPaintAgenda.onAppointmentsLoaded(appointments);
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Logger.e(TAG, "retrieveScheduleByDoctor.onError", error.getCause());
            }
        });
    }

    private void retrieveDoctorSchedule(long doctorId, long medicalCoreId) {
        GetDoctorScheduleByMCRequest request = new GetDoctorScheduleByMCRequest();
        request.setSessionId(mCurrentUser.sessionId);
        request.setMedicalCoreId(medicalCoreId);
        request.setDoctorId(doctorId);
        request.setDate(VUTDDateTimeUtils.nowLocal());
        mGetDoctorScheduleByMCHandler.getSchedule(request);
        mProgress.show();
    }

    /**
     * Return the range of days
     *
     * @return the amount of days specify according with the doctor's work days
     */
    private List<DaySelectorAdapter.DaySelector> retrieveRangeDays(long doctorId, int workdaysCount) {
        ArrayList<DaySelectorAdapter.DaySelector> result = new ArrayList<>();
        DateTime dateTime = new DateTime();
        for (int i = 0; i < workdaysCount; i++) {
            int dayOfWeek = dateTime.getDayOfWeek();
            int monthOfYear = dateTime.getMonthOfYear();
            RealmResults<Workday> doctorDays = mRealm
                    .where(Workday.class)
                    .equalTo("doctorId", doctorId)
                    .equalTo("day", dayOfWeek)
                    .findAll()
                    .sort("startTime", Sort.ASCENDING);
            DaySelectorAdapter.DaySelector daySelector = new DaySelectorAdapter.DaySelector();
            daySelector.setDayNumber(dateTime.getDayOfMonth());
            daySelector.setDate(dateTime);
            daySelector.setWeekday(dayOfWeek);
            daySelector.setDayName(VUTDDateTimeUtils.getWeekday(dayOfWeek, mContext));
            daySelector.setMonth(VUTDDateTimeUtils.getMonth(monthOfYear, mContext));

            //If the doctor doesn't work that day, then just skip  to the next day
            if (!doctorDays.isEmpty()) {
                result.add(daySelector);
            }
            dateTime = dateTime.plusDays(1);
        }
        return result;
    }

    /**
     * Every time that the UI close or has removed this method has to be called
     */
    public void onViewClose() {
        if (mCurrentUser.isValid()) {
            switch (mCurrentUser.roleId) {
                case DOCTOR:
                    break;
                case PATIENT:
                    mRealm.beginTransaction();
                    mRealm.delete(Workday.class);
                    mRealm.delete(Appointment.class);
                    mRealm.commitTransaction();
                    break;
            }
        }
    }

    /************************************************************************
     * Agenda Listeners
     ************************************************************************/

    /**
     * According to the current user the option will display different.
     * For a Doctor and a patient is to view the profile and photo.
     * For a MC user is to switch between doctors.
     */
    public void onDoctorClick() {
        switch (mCurrentUser.roleId) {
            case DOCTOR:
                //TODO: Show doctor profile
                break;

            case PATIENT:
                //TODO: Show doctor profile
                break;
        }
    }

    /**
     * Display the main option of an appointment according with the user role and appointment state.
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    public void onAppointmentSelected(Appointment appointment, DaySelectorAdapter.DaySelector daySelector) {

        int currentUser = mCurrentUser.roleId;
        //Fabric Answers
        VutdAnswers.logEvent(
                VutdAnswers.EVENT.APPOINTMENT_SELECTED,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(currentUser)),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

        switch (currentUser) {
            case DOCTOR:
                displayDoctorMainOption(appointment, daySelector);
                break;

            case PATIENT:
                displayPatientMainOption(appointment, daySelector);
                break;
        }

    }


    /**
     * Display the main option for a doctor
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    private void displayDoctorMainOption(Appointment appointment, DaySelectorAdapter.DaySelector daySelector) {
        switch ((int) appointment.bookTypeId) {
            case Appointment.BOOKED:
                //Cancel appointment.
                cancelAppointment(appointment);
                break;

            case Appointment.BUSY:
                //Mark as free.
                releaseBookTime(appointment, daySelector);
                break;

            case Appointment.FREE:
                if (mMultiSelectionMode) {
                    mRealm.beginTransaction();
                    if (appointment.isValid()) {
                        if (appointment.isSelected == 1) {
                            appointment.id = -1;
                            appointment.date = null;
                            appointment.isSelected = 0;
                        } else {
                            appointment.id = -100;// This value is to recognize selected appointments, and avoid to be deleted in showAppointment method
                            appointment.date = VUTDDateTimeUtils.convertToVUTDDateFormat(daySelector.getDate());
                            appointment.isSelected = appointment.isSelected == 1 ? 0 : 1;
                        }
                    }
                    mRealm.commitTransaction();
                    mOnPaintAgenda.onReloadAgenda();
                    if (mRealm.where(Appointment.class)
                            .equalTo("id", -100)
                            .count() == 0) {
                        mOnPaintAgenda.onAnyAppointmentSelected();
                    }
                } else {
                    //Mark as busy.
                    bookTimeAsBusy(appointment, daySelector);
                }
                break;

            case Appointment.REST_TIME:
                break;
        }
    }

    /**
     * Display the main option for a Patient
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */

    private void displayPatientMainOption(Appointment appointment, DaySelectorAdapter.DaySelector daySelector) {
        switch ((int) appointment.bookTypeId) {
            case Appointment.BOOKED:
                subscribeAppointment(appointment, daySelector);
                break;

            case Appointment.BUSY:
                subscribeAppointment(appointment, daySelector);
                break;

            case Appointment.FREE:
                createAppointment(appointment, daySelector);
                break;

            case Appointment.REST_TIME:
                break;
        }
    }

    /**
     * Display more options according the user role and the appointment state
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    public void onMoreOptionsSelected(Appointment appointment,
                                      DaySelectorAdapter.DaySelector daySelector,
                                      MenuItem menuItem, int position) {

        String optionSelected = "default";
        switch (menuItem.getItemId()) {
            case R.id.menu_create_appointment:
                optionSelected = "create_appointment";
                createAppointment(appointment, daySelector);
                break;
            case R.id.menu_mark_as_busy:
                optionSelected = "mark_as_busy";
                bookTimeAsBusy(appointment, daySelector);
                break;
            case R.id.menu_release_time:
                optionSelected = "release_time";
                releaseBookTime(appointment, daySelector);
                break;
            case R.id.menu_call_patient:
                optionSelected = "call_patient";
                mAppointmentListener.onCallPatient(appointment.getPatientPhone());
                break;
            case R.id.menu_cancel_appointment:
                optionSelected = "cancel_appointment";
                cancelAppointment(appointment);
                break;
            case R.id.menu_request_confirmation:
                optionSelected = "request_confirmation";
                requestPatientConfirmation(appointment, daySelector, position);
                break;
        }

        //Fabric Answers
        VutdAnswers.logEvent(
                VutdAnswers.EVENT.MORE_OPTIONS,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.MENU_OPTION, optionSelected),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));
    }

    @SuppressWarnings("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDoubleCheck(DoubleCheck doubleCheck) {
        Logger.d(TAG, "onDoubleCheck() " + doubleCheck.appointmentId);
        mOnPaintAgenda.onReloadAgenda();
    }

    private void onMultiSelectionStateChange() {
        RealmResults<Appointment> appointments = mRealm.where(Appointment.class).findAll();
        mRealm.beginTransaction();
        for (Appointment appointment : appointments) {
            if (mMultiSelectionMode) {
                //Fabric Answers
                VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE);

                appointment.selectionMode = 1;
            } else {
                appointment.selectionMode = 0;
            }
        }
        //If the user change its mind then just clear selected data
        if (!mMultiSelectionMode) {
            RealmResults<Appointment> appointmentsSelected = appointments.where().equalTo("id", -100).findAll();
            for (Appointment appointment : appointmentsSelected) {
                appointment.id = -1;
                appointment.isSelected = 0;
                appointment.date = null;
            }
        }
        mRealm.commitTransaction();
        mOnPaintAgenda.onReloadAgenda();
    }

    /************************************************************************
     * Agenda Transactions
     ************************************************************************/

    /**
     * This will call the web service in order to create a new appointment
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    private void createAppointment(final Appointment appointment, final DaySelectorAdapter.DaySelector daySelector) {
        CreateAppointmentDialog dialog = mCurrentUser.roleId == PATIENT ?
                new CreateAppointmentDialog(mContext, mCurrentUser.name + " " + mCurrentUser.lastName, mCurrentUser.phone, false) :
                new CreateAppointmentDialog(mContext);

        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        dialog.setTitle(startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_create_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_create_appointment, startTime));

        dialog.setOnDecisionClick(new CreateAppointmentDialog.OnDecisionClick() {
            @Override
            public void onConfirmClick(String patientName, String phone) {
                mProgress.show();
                mRealm.beginTransaction();
                Appointment tmp = mRealm.copyToRealm(appointment);
                tmp.id = -99;
                tmp.status = "0";
                tmp.userId = mCurrentUser.reference;
                tmp.bookTypeId = Appointment.BOOKED;
                tmp.patientName = patientName;
                tmp.patientPhone = phone;
                tmp.isDirty = 1;
                tmp.date = VUTDDateTimeUtils.convertToVUTDDateFormat(daySelector.getDate());
                mRealm.commitTransaction();

                CreateAppointmentRequest request = new CreateAppointmentRequest();
                request.setSessionId(mCurrentUser.sessionId);
                request.setStartTime(tmp.getStartTime());
                request.setPatientPhone(tmp.getPatientPhone());
                request.setPatientName(tmp.getPatientName());
                request.setBookDate(tmp.getDate());
                request.setBookType(tmp.getBookTypeId());
                request.setDoctorId(tmp.getDoctorId());
                request.setMedicalCoreId(tmp.getMedicalCoreId());
                mCreateAppointmentHandler.createAppointment(request);
            }

            @Override
            public void onCancelClick() {
                //TODO: Do something in a future
            }
        });
        dialog.show();
    }

    /**
     * This will create a new subscription to the appointment selected
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    private void subscribeAppointment(final Appointment appointment, final DaySelectorAdapter.DaySelector daySelector) {
        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        String message = startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_subscribe_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_subscribe_appointment, startTime);
        mSimpleDialog.showYesNoDismissDialog(
                true,
                message,
                mContext.getString(R.string.agenda_doctor_main_option_subscribe_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //Fabric Answers
                        VutdAnswers.logEvent(
                                VutdAnswers.EVENT.SUBSCRIBE_APPOINTMENT,
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));


                        mProgress.show();
                        SubscribeAppointmentRequest request = new SubscribeAppointmentRequest();
                        request.setAppointmentId(appointment.id);
                        request.setSessionId(mCurrentUser.sessionId);
                        mSubscribeAppointmentHandler.subscribeAppointment(request);
                    }
                });
    }

    /**
     * This will delete the appointment and set as free the specific time
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    private void releaseBookTime(final Appointment appointment, final DaySelectorAdapter.DaySelector daySelector) {
        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        String message = startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_release_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_release_appointment, startTime);
        mSimpleDialog.showYesNoDismissDialog(
                true,
                message,
                mContext.getString(R.string.agenda_doctor_main_option_release_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgress.show();

                        //Fabric Answers
                        VutdAnswers.logEvent(
                                VutdAnswers.EVENT.RELEASE_TIME,
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

                        mRealm.beginTransaction();
                        Appointment tmp = mRealm.copyToRealm(appointment);
                        tmp.bookTypeId = Appointment.DELETED;
                        mRealm.commitTransaction();

                        DeleteAppointmentRequest request = new DeleteAppointmentRequest();
                        request.setAppointmentId(appointment.getId());
                        request.setSessionId(mCurrentUser.sessionId);
                        request.setReasonId(1);//TODO: change reason id
                        mDeleteAppointmentHandler.deleteAppointment(request);
                    }
                }
        );
    }

    /**
     * This create a new appointment with the status as Busy
     *
     * @param appointment The appointment selected
     * @param daySelector The current day when teh appointment was selected
     */
    private void bookTimeAsBusy(final Appointment appointment, final DaySelectorAdapter.DaySelector daySelector) {
//TODO: String format? to give a better appearance. Title or message
        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        String message = startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_book_as_busy_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_book_as_busy_appointment, startTime);
        mSimpleDialog.showYesNoDismissDialog(
                true,
                message,
                mContext.getString(R.string.agenda_doctor_main_option_busy_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgress.show();

                        mRealm.beginTransaction();
                        Appointment tmp = mRealm.copyToRealm(appointment);
                        tmp.id = -99;
                        tmp.status = "2";
                        tmp.userId = mCurrentUser.reference;
                        tmp.bookTypeId = Appointment.BUSY;
                        tmp.patientName = mCurrentUser.name;
                        tmp.patientPhone = mCurrentUser.phone;
                        tmp.isDirty = 1;
                        tmp.date = VUTDDateTimeUtils.convertToVUTDDateFormat(daySelector.getDate());
                        mRealm.commitTransaction();

                        CreateAppointmentRequest request = new CreateAppointmentRequest();
                        request.setSessionId(mCurrentUser.sessionId);
                        request.setStartTime(tmp.getStartTime());
                        request.setPatientPhone(tmp.getPatientPhone());
                        request.setPatientName(tmp.getPatientName());
                        request.setBookDate(tmp.getDate());
                        request.setBookType(tmp.getBookTypeId());
                        request.setDoctorId(tmp.getDoctorId());
                        request.setMedicalCoreId(tmp.getMedicalCoreId());
                        mCreateAppointmentHandler.createAppointment(request);
                    }
                }
        );
    }

    /**
     * This will delete the appointment from the doctor schedule
     *
     * @param appointment The appointment selected
     */
    private void cancelAppointment(final Appointment appointment) {
        //TODO: String format? to give a better appearance. Title or message
        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        String message = startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_cancel_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_cancel_appointment, startTime);
        mSimpleDialog.showYesNoDismissDialog(
                true,
                message,
                mContext.getString(R.string.agenda_doctor_main_option_cancel_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgress.show();

                        //Fabric Answers
                        VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_CANCELLED,
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

                        mRealm.beginTransaction();
                        Appointment tmp = mRealm.copyToRealm(appointment);
                        tmp.bookTypeId = Appointment.DELETED;
                        mRealm.commitTransaction();

                        DeleteAppointmentRequest request = new DeleteAppointmentRequest();
                        request.setAppointmentId(appointment.getId());
                        request.setSessionId(mCurrentUser.sessionId);
                        request.setReasonId(1);//TODO: change reason id
                        mDeleteAppointmentHandler.deleteAppointment(request);
                    }
                });
    }

    private void requestPatientConfirmation(final Appointment appointment, final DaySelectorAdapter.DaySelector daySelector, int postion) {
        String startTime = VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.getStartTime());
        String message = startTime.contains("01:00") ?
                mContext.getString(R.string.agenda_request_confirmation_appointment_at_1, startTime) :
                mContext.getString(R.string.agenda_request_confirmation_appointment, startTime);
        mSimpleDialog.showYesNoDismissDialog(
                true,
                message,
                mContext.getString(R.string.agenda_doctor_main_option_cancel_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        //Fabric Answers
                        VutdAnswers.logEvent(VutdAnswers.EVENT.REQUEST_CONFIRMATION,
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

                        mRealm.beginTransaction();
                        if (appointment.isValid()) {
                            appointment.isConfirmationSend = -99;
                        }
                        mRealm.commitTransaction();

                        mOnPaintAgenda.onReloadAgenda();

                        SendConfirmationRequest request = new SendConfirmationRequest();
                        request.setSessionId(mCurrentUser.sessionId);
                        request.setAppointmentId(appointment.id);
                        mSendConfirmationRequestHandler.sendConfirmationRequest(request);
                    }
                });
    }

    /************************************************************************
     * Callbacks                                                            *
     ************************************************************************/

    private IHandlerResult<Rule, BaseResponse> mDeleteAppointmentCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            mProgress.showBadResult();
            for (Rule rule : errors) {
                Toast.makeText(mContext, rule.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }

            //Back appointment to its original state (BOOKED)
            mRealm.beginTransaction();
            Appointment appointment = mRealm.where(Appointment.class)
                    .equalTo("bookTypeId", Appointment.DELETED)
                    .findFirst();
            appointment.bookTypeId = Appointment.BOOKED;
            mRealm.commitTransaction();

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_DELETED,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));
        }

        @Override
        public void onOkResult(BaseResponse result) {
            //TODO: This method is called even when it fails… BUG!!
            mProgress.showSuccessfulResult();
            mProgress.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    DateTime currentDateTime = null;
                    Appointment appointment = mRealm.where(Appointment.class)
                            .equalTo("bookTypeId", Appointment.DELETED)
                            .findFirst();
                    if (appointment != null) {
                        mRealm.beginTransaction();
                        currentDateTime = VUTDDateTimeUtils
                                .parseForPattern(appointment.getDate(), VUTDDateTimeUtils.VUTD_DATE_FORMAT);
                        appointment.clear();
                        mRealm.commitTransaction();
                    }

                    if (currentDateTime != null) {
                        showAppointments(appointment.getDoctorId(), currentDateTime);
                    }

                    //Fabric Answers
                    VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_DELETED,
                            new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));

                }
            });
        }
    };

    private IHandlerResult<Rule, CreateAppointmentResponse> mCreateAppointmentCallback
            = new IHandlerResult<Rule, CreateAppointmentResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            for (Rule rule : errors) {
                Toast.makeText(mContext, rule.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }
            mProgress.showBadResult();

            //Here we retrieve the only dirty object
            Appointment appointment = mRealm.where(Appointment.class)
                    .equalTo("id", -99)
                    .equalTo("isDirty", 1)
                    .findFirst();

            if (appointment != null) {
                //Fabric Answers
                VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_CREATION,
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.APPOINTMENT_TYPE, String.valueOf(appointment.getBookTypeId())),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

                mRealm.beginTransaction();
                appointment.clear();
                mRealm.commitTransaction();
            }
        }

        @Override
        public void onOkResult(final CreateAppointmentResponse result) {
            mProgress.showSuccessfulResult();
            // this listener is to make a smooth animation
            mProgress.setOnDismissListener(
                    new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialogInterface) {
                            //Here we retrieve the only dirty object
                            Appointment appointment = mRealm.where(Appointment.class)
                                    .equalTo("id", -99)
                                    .equalTo("isDirty", 1)
                                    .findFirst();
                            if (appointment != null) {
                                mRealm.beginTransaction();
                                appointment.id = result.getAppointmentCreated();
                                appointment.isDirty = 0;
                                mRealm.commitTransaction();

                                //Fabric Answers
                                VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_CREATION,
                                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.APPOINTMENT_TYPE, String.valueOf(appointment.getBookTypeId())),
                                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"),
                                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));

                                if (mCurrentUser.roleId == PATIENT) {
                                    mAppointmentListener.onAppointmentCreatedByPatient(appointment.getId());
                                } else {
                                    showAppointments(appointment.getDoctorId(), VUTDDateTimeUtils.parseForPattern(
                                            appointment.getDate(), VUTDDateTimeUtils.VUTD_DATE_FORMAT));
                                }
                            }
                        }
                    }
            );
        }
    };

    private IHandlerResult<Rule, BaseResponse> mSubscribeAppointmentCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            mProgress.showBadResult();
            for (Rule rule : errors) {
                Toast.makeText(mContext, rule.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.SUBSCRIBE_APPOINTMENT,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));
        }

        @Override
        public void onOkResult(BaseResponse result) {
            mProgress.showSuccessfulResult();
            mAppointmentListener.onAppointmentCreatedByPatient(99);

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.SUBSCRIBE_APPOINTMENT,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));
        }
    };

    private IHandlerResult<Rule, BaseResponse> mSendConfirmationRequestCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            for (Rule error : errors){
                Toast.makeText(mContext, error.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }
            Appointment appointment = mRealm.where(Appointment.class).equalTo("isConfirmationSend", -99)
                    .findFirst();
            mRealm.beginTransaction();
            appointment.isConfirmationSend = 0;
            mRealm.commitTransaction();

            mOnPaintAgenda.onReloadAgenda();

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.REQUEST_CONFIRMATION,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));
        }

        @Override
        public void onOkResult(BaseResponse result) {
            Appointment appointment = mRealm.where(Appointment.class).equalTo("isConfirmationSend", -99)
                    .findFirst();
            mRealm.beginTransaction();
            appointment.isConfirmationSend = 1;
            mRealm.commitTransaction();

            mOnPaintAgenda.onReloadAgenda();

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.REQUEST_CONFIRMATION,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, String.valueOf(appointment.getDoctorId())),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, appointment.getStartTime()));
        }
    };

    private IHandlerResult<Rule, GetDoctorScheduleByMCResponse> mGetDoctorScheduleByMCCallback
            = new IHandlerResult<Rule, GetDoctorScheduleByMCResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            Logger.d(TAG, "mGetDoctorScheduleByMCCallback.onBadResult()");
            mProgress.showBadResult();
        }

        @Override
        public void onOkResult(final GetDoctorScheduleByMCResponse result) {
            Logger.d(TAG, "mGetDoctorScheduleByMCCallback.onOkResult()");
            mProgress.dismiss();
            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    for (WorkdayCommon common : result.getDays()) {
                        Workday workday = realm.createObject(Workday.class);
                        workday.transform(common);
                        workday.doctorId = mDoctorId;
                        workday.medicalCoreId = mMedicalCoreId;
                    }

                    for (AppointmentCommon common : result.getAppointmentCommons()) {
                        Appointment appointment = realm.createObject(Appointment.class);
                        appointment.id = common.getId();
                        appointment.doctorId = mDoctorId;
                        appointment.medicalCoreId = mMedicalCoreId;
                        appointment.bookTypeId = common.getBookTypeId();
                        appointment.startTime = common.getStartTime();
                        appointment.date = common.getDate();
                    }

                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Logger.d(TAG, "GetDoctorScheduleByMCCallback.onSuccess()");
                    List<DaySelectorAdapter.DaySelector> workDays = retrieveRangeDays(mDoctorId, 8);
                    mOnPaintAgenda.onDaysLoaded(workDays);
                    showAppointments(mDoctorId, workDays.get(0).getDate());
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Logger.e(TAG, "GetDoctorScheduleByMCCallback.onError()", error.getCause());
                }
            });
        }
    };

    /************************************************************************
     * Interfaces                                                           *
     ************************************************************************/

    public interface IPaintAgenda {
        void onDaysLoaded(List<DaySelectorAdapter.DaySelector> days);

        void onAppointmentsLoaded(List<Appointment> appointments);

        void onDoctorLoaded(Doctor doctor);

        void onReloadAgenda();

        void onAnyAppointmentSelected();
    }

    public interface IAppointmentListener {
        void onCallPatient(String number);

        void onAppointmentCreatedByPatient(long lastEntryId);
    }
}
