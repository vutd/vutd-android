package io.vutd.agenda.controller.fragment;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import io.realm.Realm;
import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.activity.EntryActivity_;
import io.vutd.agenda.controller.mediator.PatientAppointmentMediator;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.adapter.BookedAppointmentAdapter;
import io.vutd.agenda.view.adapter.BookedDaySelectorAdapter;

/**
 * Created by andresmariscal on 04/09/16.
 */
@EFragment(R.layout.fragment_patient_appointmet)
public class PatientAppointmentsFragment extends AbstractFragment
        implements PatientAppointmentMediator.IBookedAppointment,
        BookedDaySelectorAdapter.IBookedDaySelector,
        BookedAppointmentAdapter.OnBookedAppointmentSelected ,
        PatientAppointmentMediator.IBookedAppointmentListener{
    public static final String TAG = "PatientAppointmentsFragment";

    @FragmentArg
    long lastEntryId;

    @ViewById(R.id.booked_appointments)
    RecyclerView mBookedAppointments;

    @ViewById(R.id.empty_list_container)
    View mEmptyListContainer;

    @ViewById(R.id.agenda_settings)
    View TEMPORAL_LOGUT;

    @Bean
    PatientAppointmentMediator mMediator;

    @Bean
    BookedDaySelectorAdapter mDaySelectorAdapter;

    @Bean
    BookedAppointmentAdapter mBookedAppointmentAdapter;

    @Override
    public String getClassTag() {
        return TAG;
    }

    @Override
    public void afterViews() {
        super.afterViews();
        mBookedAppointments.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL, false));
        mBookedAppointments.setAdapter(mBookedAppointmentAdapter);

        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //Temporal Logout
        final PopupMenu popupMenu = new PopupMenu(getActivity(), TEMPORAL_LOGUT, Gravity.END, 0, R.style.AppointmentOptions);
        popupMenu.inflate(R.menu.delete_tmp_log_out);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                mRealm.executeTransactionAsync(
                        new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.deleteAll();
                            }
                        },
                        new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {

                                //Fabric Answers
                                VutdAnswers.logEvent(VutdAnswers.EVENT.LOGOUT);

                                EntryActivity_.intent(getActivity())
                                        .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                        .start();
                            }
                        },
                        new Realm.Transaction.OnError() {
                            @Override
                            public void onError(Throwable error) {
                                Logger.e(TAG, "executeTransactionAsync onError: Logout", error);
                            }
                        });
                return true;
            }
        });
        TEMPORAL_LOGUT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupMenu.show();
            }
        });
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

        mMediator.init();
    }

    /************************************************************************
     * Initializer
     ************************************************************************/

    @Override
    public void afterInjects() {
        super.afterInjects();
        mDaySelectorAdapter.setOnBookedDaySelector(this);
        mBookedAppointmentAdapter.setHorizontalRecyclerAdapter(mDaySelectorAdapter);
        mBookedAppointmentAdapter.setOnBookedAppointmentSelected(this);

        mMediator.setContext(getActivity());
        mMediator.setRealm(mRealm);
        mMediator.setLastEntryId(lastEntryId);
        mMediator.setOnBookedAppointment(this);
        mMediator.setOnBookedAppointmentListener(this);
    }

    private void showEmptyListIcon(boolean show){
        mBookedAppointments.setVisibility(show ? View.GONE : View.VISIBLE);
        mEmptyListContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    /************************************************************************
     * Listeners
     ************************************************************************/

    @Override
    public void onBookedAppointmentsSuccess(ArrayList<BookedDaySelectorAdapter.BookedDaySelector> appointments) {
        if(!appointments.isEmpty()){
            mDaySelectorAdapter.setDaySelectors(appointments);
            mDaySelectorAdapter.notifyDataSetChanged();
            mBookedAppointmentAdapter.setAppointments(appointments.get(0).getAppointments());
            mBookedAppointmentAdapter.notifyDataSetChanged();
            showEmptyListIcon(false);
        } else {
            mBookedAppointmentAdapter.clear();
            mDaySelectorAdapter.clear();
            showEmptyListIcon(true);
        }
    }

    @Override
    public void onBookedAppointmentsFail() {
        //TODO: improve this behavior
    }

    @Override
    public void reloadBookedAppointments() {
        mBookedAppointmentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(ArrayList<BookedAppointmentAdapter.Booked> appointments) {
        mBookedAppointmentAdapter.setAppointments(appointments);
        mBookedAppointmentAdapter.notifyDataSetChanged();
    }

    @Override
    public void onMenuItemSelected(MenuItem menuItem, BookedAppointmentAdapter.Booked booked) {
        mMediator.onMenuItemSelected(menuItem, booked);
    }

    @Override
    public void onPhoneSelected(BookedAppointmentAdapter.Booked booked) {
        mMediator.onPhoneNumberClick(booked);
    }

    @Override
    public void onCallDoctor(String number) {
        AbstractFragmentPermissionsDispatcher.dialPhoneNumberWithCheck(this, number);
    }
}
