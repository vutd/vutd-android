package io.vutd.agenda.controller.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.RealmResults;
import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.rest.common.ConcurrentCommon;
import io.vutd.agenda.rest.handler.CreateConcurrentAppointmentHandler;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.request.CreateConcurrentAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.view.adapter.ConcurrentAppointmentAdapter;

/**
 * Created by andresmariscal on 23/10/16.
 */
@EActivity(R.layout.activity_concurrent_appointment)
public class ConcurrentAppointmentActivity extends AbstractFormActivity<BaseResponse> {

    @ViewById(R.id.concurrent_tool_bar)
    Toolbar mToolBar;

    @ViewById(R.id.concurrent_name)
    EditText mName;

    @ViewById(R.id.concurrent_phone)
    EditText mPhone;

    @ViewById(R.id.concurrent_summary)
    RecyclerView mSummary;

    @Bean
    ConcurrentAppointmentAdapter mAdapter;

    @Bean
    CreateConcurrentAppointmentHandler mHandler;

    @Override
    public void afterInjects() {
        super.afterInjects();
        mHandler.setHandlerResult(this);
    }

    @Override
    public void afterViews() {
        super.afterViews();
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mSummary.setLayoutManager(llm);

        RealmResults<Appointment> appointments = mRealm.where(Appointment.class)
                .equalTo("id", -100)
                .findAll();
        ArrayList<Appointment> appointmentsView = new ArrayList<>();
        appointmentsView.addAll(appointments);
        mAdapter.setAppointments(appointmentsView);

        mSummary.setAdapter(mAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.add(0, 1, 1, R.string.dialog_create_appointment_book_button);
        menuItem.setIcon(R.drawable.ic_check_dark_disabled);
        menuItem.setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == 1){

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CONFIRMATION, "confirmed"));

            mProgress.show();
            CreateConcurrentAppointmentRequest request = new CreateConcurrentAppointmentRequest();
            request.setSessionId(mRealm.where(CurrentUser.class).findFirst().sessionId);
            request.setBookType(1);
            request.setPatientName(mName.getText().toString());
            request.setPatientPhone(mPhone.getText().toString());

            ArrayList<ConcurrentCommon> concurrentCommons = new ArrayList<>();
            for(Appointment appointment : mAdapter.getAppointments()){
                //This is a lazy way to get the doctor id and mc id
                request.setDoctorId(appointment.doctorId);
                request.setMedicalCoreId(appointment.medicalCoreId);
                ConcurrentCommon concurrentCommon = new ConcurrentCommon();
                concurrentCommon.setBookDate(appointment.getDate());
                concurrentCommon.setStartTime(appointment.getStartTime());
                concurrentCommons.add(concurrentCommon);
            }
            request.setConcurrent(concurrentCommons);
            mHandler.createConcurrentAppointment(request);
        } else {

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CONFIRMATION, "not_confirmed"));

            onBackPressed();
        }

        return true;
    }

    @Override
    public Map<EditText, List<Rule>> attachRules() {
        HashMap<EditText, List<Rule>> rules = new HashMap<>();

        ArrayList<Rule> nameRules = new ArrayList<>();
        nameRules.add(new Rule(Rule.NAME));
        rules.put(mName, nameRules);

        ArrayList<Rule> phoneRules = new ArrayList<>();
        phoneRules.add(new Rule(Rule.PHONE));
        rules.put(mPhone, phoneRules);

        return rules;
    }

    @Override
    public void okResult(BaseResponse result) {
        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));

        mProgress.showSuccessfulResult();
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void badResult() {
        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));

        mProgress.showBadResult();
    }
}
