package io.vutd.agenda.controller.activity;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnMenuTabClickListener;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.fragment.IDataLoaded;
import io.vutd.agenda.controller.mediator.MainMediator;
import io.vutd.agenda.view.widget.DilatingDotsProgressBar;

@EActivity
public class MainActivity extends AbstractActivity implements
        OnMenuTabClickListener,
        MainMediator.IBottomNavigationResource,
        IDataLoaded {

    public BottomBar mBottomBar;

    @Bean
    public MainMediator mMediator;

    @ViewById(R.id.progress)
    DilatingDotsProgressBar mProgress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //It is require to set the content view here in order to make BottomBar works
        setContentView(R.layout.activity_main);
        mBottomBar = BottomBar.attach(this, savedInstanceState);
        mBottomBar.noTopOffset();
        mBottomBar.noNavBarGoodness();
        mBottomBar.setOnMenuTabClickListener(this);
        mMediator.init();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mBottomBar.onSaveInstanceState(outState);
    }

    @Override
    public void afterViews() {
        super.afterViews();
        mProgress.showNow();
        mProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void afterInjects(){
        super.afterInjects();
        mMediator.setRealm(mRealm);
        mMediator.setContext(this);
        mMediator.setOnBottomNavigationResource(this);

    }

    @Override
    public void onMenuTabSelected(@IdRes int menuItemId) {
        mMediator.showView(menuItemId);
    }

    @Override
    public void onMenuTabReSelected(@IdRes int menuItemId) {
        mMediator.onMenuTabReSelected(menuItemId);
    }

    @Override
    public void resourceByUserRole(int menuXml) {
        mBottomBar.setItems(menuXml);
        mBottomBar.setActiveTabColor(ContextCompat.getColor(this, R.color.colorAccent));
    }

    @Override
    public void onDataLoaded() {
        mProgress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onDataLoading() {
        mProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void onBackPressed() {
        if(mMediator.onBackPressed()){
            finish();
        } else {
            mBottomBar.selectTabAtPosition(0, true);
        }
    }
}
