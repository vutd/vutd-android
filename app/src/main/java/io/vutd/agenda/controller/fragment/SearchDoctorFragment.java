package io.vutd.agenda.controller.fragment;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.mediator.SearchMediator;
import io.vutd.agenda.realm.viewmodel.SearchDoctorHistory;
import io.vutd.agenda.utils.Animations;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.adapter.SearchAdapter;

/**
 * Created by andresmariscal on 29/08/16.
 */
@EFragment(R.layout.fragment_search_doctor)
public class SearchDoctorFragment extends AbstractFragment
        implements SearchMediator.ISearchHistory,
        SearchMediator.ISearchResult,
        SearchAdapter.OnSearchItemCLickListener,
        SearchAdapter.OnEmptyListener{

    public static final String TAG = "SearchDoctorFragment";

    @ViewById(R.id.search_edit_text)
    EditText mSearchEditText;

    @ViewById(R.id.clear_search_button)
    Button mClearSearch;

    @ViewById(R.id.icon_container)
    View mIconContainer;

    @ViewById(R.id.icon_holder)
    ImageView mIconHolder;

    @ViewById(R.id.search_message_title)
    TextView mMessageTitle;

    @ViewById(R.id.search_message_description)
    TextView mMessageDescription;

    @ViewById(R.id.recycler_history_search)
    RecyclerView mHistorySearch;

    @ViewById(R.id.search_try_again)
    Button mTryAgainButton;

    @Bean
    SearchAdapter mSearchAdapter;

    @Bean
    SearchMediator mMediator;

    @Override
    public String getClassTag() {
        return TAG;
    }

    @Override
    public void afterViews() {
        super.afterViews();
        mSearchEditText.addTextChangedListener(mSearchTextWatcher);
        mSearchEditText.setOnEditorActionListener(mOnSearch);

        LinearLayoutManager llm = new LinearLayoutManager(getActivity(),  LinearLayoutManager.VERTICAL, false);
        mHistorySearch.setLayoutManager(llm);

        mSearchAdapter.setOnSearchItemCLickListener(this);

        mMediator.init();
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mMediator.setContext(getActivity());
        mMediator.setRealm(mRealm);

        //Listeners
        mMediator.setOnSearchHistoryListener(this);
        mMediator.setOnSearchResultListener(this);

        mSearchAdapter.setOnEmptyListener(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mMediator.disconnectAsyncTasks();
        dismissLoading();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMediator.disconnectAsyncTasks();
        dismissLoading();
    }

    @Override
    public void onDetach() {
        mMediator.disconnectAsyncTasks();
        dismissLoading();
        super.onDetach();
    }

    /************************************************************************
     * Listeners
     ************************************************************************/

    @Click({R.id.icon_container, R.id.search_tool_bar})
    public void onSearchClick(){
        mSearchEditText.requestFocus();
        mSearchEditText.setFocusableInTouchMode(true);

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(mSearchEditText, InputMethodManager.SHOW_FORCED);

    }

    @Click(R.id.search_try_again)
    public void onTryAgainClick(){
        if (mSearchEditText.getText().toString().trim().length() >= 6) {
            Logger.d(TAG, "Search for: " + mSearchEditText.getText().toString());
            mTryAgainButton.setVisibility(View.GONE);
            removeAllViews();
            onDataLoading();
            mMediator.searchDoctorByPhone(mSearchEditText.getText().toString());
        } else {
            mSearchEditText.setError(getString(R.string.search_doctor_by_phone_invalid_phone));
        }
    }

    private TextView.OnEditorActionListener mOnSearch = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH && textView.getText().toString().trim().length() >= 6) {
                Logger.d(TAG, "Search for: " + textView.getText().toString());
                removeAllViews();
                onDataLoading();
                mMediator.searchDoctorByPhone(mSearchEditText.getText().toString());
            } else {
                mSearchEditText.setError(getString(R.string.search_doctor_by_phone_invalid_phone));
            }
            InputMethodManager imm = (InputMethodManager)getActivity().getBaseContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mSearchEditText.getWindowToken(), 0);
            return true;
        }
    };

    private TextWatcher mSearchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            boolean isCleanable = count > 0;
            mClearSearch.setVisibility(isCleanable ? View.VISIBLE : View.INVISIBLE);
            mClearSearch.setEnabled(isCleanable);
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            boolean isCleanable = count > 0;
            mClearSearch.setVisibility(isCleanable ? View.VISIBLE : View.INVISIBLE);
            mClearSearch.setEnabled(isCleanable);
        }

        @Override
        public void afterTextChanged(Editable s) {
            boolean isCleanable = s != null && s.length() > 0;
            mClearSearch.setVisibility(isCleanable ? View.VISIBLE : View.INVISIBLE);
            mClearSearch.setEnabled(isCleanable);
            if(!isCleanable) mMediator.clearSearch();
        }
    };

    @Click(R.id.clear_search_button)
    public void clearSearch() {
        mSearchEditText.setText(null);
        mSearchAdapter.clear();
        mMediator.clearSearch();
    }

    @Override
    public void onItemClick(SearchDoctorHistory item) {
        mMediator.onDoctorSelected(item);
    }

    @Override
    public void onDeleteClick(SearchDoctorHistory item, int position) {
        mMediator.deleteHistoryItem(item, position);
    }

    /************************************************************************
     * Draw screen
     ************************************************************************/

    @Override
    public void onEmptyHistory() {
        Logger.d(TAG,"onEmptyHistory");
        showIconHolder(true);
        mIconHolder.setImageResource(R.drawable.find_by_phone_card);
        mMessageTitle.setText(R.string.search_doctor_by_phone_instructions);
        mMessageDescription.setText(R.string.search_doctor_by_phone_instructions_message);
    }

    @Override
    public void onHistoryLoaded(ArrayList<SearchDoctorHistory> doctors) {
        Logger.d(TAG,"onHistoryLoaded");

        mSearchAdapter.setDoctors(doctors);
        mSearchAdapter.setHeaderTitle(getActivity().getString(R.string.search_doctor_by_phone_history_header));
        mHistorySearch.setAdapter(mSearchAdapter);
        mSearchAdapter.notifyDataSetChanged();
        showIconHolder(false);
    }

    @Override
    public void onRecordDeleted(int position, int totalRows) {
        Logger.d(TAG,"onRecordDeleted");
        mSearchAdapter.deleteAtPosition(position);
    }

    @Override
    public void onSearchFailed() {
        Logger.d(TAG, "onSearchFailed");
        onDataLoaded();

        showIconHolder(true);
        mIconHolder.setImageResource(R.drawable.ic_fail_100dp);
        mMessageTitle.setText(R.string.search_doctor_by_phone_retrofit_fail);
        mMessageDescription.setText(R.string.search_doctor_by_phone_retrofit_fail_message);
        mTryAgainButton.setVisibility(View.VISIBLE);

        mIconContainer.startAnimation(Animations.VUTDfadeInAnimation(null));

        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.SEARCH_BY_PHONE,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));
    }

    @Override
    public void onNoResultsFound() {
        Logger.d(TAG, "onNoResultsFound");
        onDataLoaded();

        showIconHolder(true);
        mIconHolder.setImageResource(R.drawable.not_found);
        mMessageTitle.setText(R.string.search_doctor_by_phone_no_result_found);
        mMessageDescription.setText(R.string.search_doctor_by_phone_no_result_found_message);

        mIconContainer.startAnimation(Animations.VUTDfadeInAnimation(null));

        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.SEARCH_BY_PHONE,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.SEARCH_RESULT, "not_found"),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER, mMediator.getCurrentUserName()),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PHONE, mSearchEditText.getText().toString()));
    }

    @Override
    public void onResults(ArrayList<SearchDoctorHistory> doctors) {
        Logger.d(TAG,"onResults doctors.size " + doctors.size());
        onDataLoaded();

        mSearchAdapter.setDoctors(doctors);
        mSearchAdapter.setHeaderTitle(getActivity().getString(R.string.search_doctor_by_phone_result_title, mSearchEditText.getText().toString()));
        mHistorySearch.setAdapter(mSearchAdapter);
        mSearchAdapter.notifyDataSetChanged();
        showIconHolder(false);

        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.SEARCH_BY_PHONE,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.SEARCH_RESULT, "found"),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER, mMediator.getCurrentUserName()),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PHONE, mSearchEditText.getText().toString()));
    }

    private void showIconHolder(boolean show){
        mHistorySearch.setVisibility(show ? View.GONE : View.VISIBLE);
        mIconContainer.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private void removeAllViews(){
        mHistorySearch.setVisibility(View.GONE);
        mIconContainer.setVisibility(View.GONE);
    }

    @Override
    public void onEmptyAdapter() {
        Logger.d(TAG, "onEmptyAdapter");
        showIconHolder(true);
    }
}
