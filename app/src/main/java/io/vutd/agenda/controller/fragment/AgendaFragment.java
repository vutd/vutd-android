package io.vutd.agenda.controller.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.FragmentArg;
import org.androidannotations.annotations.ViewById;

import java.util.List;

import io.realm.Realm;
import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.activity.ConcurrentAppointmentActivity;
import io.vutd.agenda.controller.activity.ConcurrentAppointmentActivity_;
import io.vutd.agenda.controller.activity.EntryActivity_;
import io.vutd.agenda.controller.activity.MainActivity;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.controller.mediator.AgendaMediator;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.adapter.AgendaDoctorAdapter;
import io.vutd.agenda.view.adapter.AgendaPatientAdapter;
import io.vutd.agenda.view.adapter.DaySelectorAdapter;
import io.vutd.agenda.view.adapter.OnAppointmentClickListener;

/**
 * Created by andresmariscal on 09/07/16.
 */
@EFragment(R.layout.fragment_agenda)
public class AgendaFragment extends AbstractFragment
        implements AgendaMediator.IPaintAgenda,
        DaySelectorAdapter.IDaySelector,
        OnAppointmentClickListener,
        AgendaMediator.IAppointmentListener {

    public static final String TAG = "AgendaFragment";
    private final int REQUEST_CODE = 199;

    private ActionMode mActionMode;

    @ViewById(R.id.tool_bar_agenda)
    Toolbar toolbar;

    @ViewById(R.id.agenda_doctor_profile_photo)
    SimpleDraweeView mProfilePhoto;

    @ViewById(R.id.agenda_doctor_name)
    TextView mDoctorName;

    @ViewById(R.id.agenda_doctor_speciality)
    TextView mDoctorSpeciality;

    @ViewById(R.id.agenda_day_selector_recycler_view)
    RecyclerView mDaysSelector;

    @ViewById(R.id.agenda_appointments_recycler_view)
    RecyclerView mAppointments;

    @Bean
    AgendaMediator mMediator;

    @Bean
    DaySelectorAdapter mDaySelectorAdapter;

    @Bean
    AgendaDoctorAdapter mAgendaDoctorAdapter;

    @Bean
    AgendaPatientAdapter mAgendaPatientAdapter;

    @FragmentArg
    long mDoctorId;

    @FragmentArg
    long mMedicalCoreId;

    private DaySelectorAdapter.DaySelector mCurrentSelectedDay;

    @Override
    public String getClassTag() {
        return TAG;
    }

    @Click({R.id.agenda_doctor_name, R.id.agenda_doctor_profile_photo})
    public void onDoctorClick() {
        mMediator.onDoctorClick();
    }

    /************************************************************************
     * Life cycle
     ************************************************************************/

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        mMediator.onStart();
    }

    @Override
    public void afterViews() {
        super.afterViews();
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        LinearLayoutManager llmDaysSelector = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mDaysSelector.setLayoutManager(llmDaysSelector);
        LinearLayoutManager llmAppointments = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        mAppointments.setLayoutManager(llmAppointments);

        //Wait til views are injected to assign the adapter
        mDaysSelector.setAdapter(mDaySelectorAdapter);
        mDaySelectorAdapter.setOnDaySelected(this);

        mMediator.setAppointmentListener(this);
        mMediator.setIPaintAgenda(this);

        mMediator.init();

        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //Temporal
        if (mMediator.isPatient()) {
            mAppointments.setAdapter(mAgendaPatientAdapter);
            mAgendaPatientAdapter.setOnAppointmentClickListener(this);
        } else {
            //TODO: Temporal util more menus are added
            ((MainActivity) getActivity()).mBottomBar.hide();
            mAppointments.setAdapter(mAgendaDoctorAdapter);
            mAgendaDoctorAdapter.setOnAppointmentSelected(this);
        }
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mMediator.setContext(getActivity());
        mMediator.setRealm(mRealm);

        //this values are valid only when the current user is a patient
        mMediator.setDoctorId(mDoctorId);
        mMediator.setMedicalCoreId(mMedicalCoreId);
    }

    @Override
    public void onStop() {
        super.onStop();
        mMediator.onStop();
    }

    @Override
    public void onDetach() {
        mMediator.onViewClose();
        super.onDetach();
    }

    /************************************************************************
     * Draw View
     ************************************************************************/

    @Override
    public void onDaysLoaded(List<DaySelectorAdapter.DaySelector> days) {
        if (!days.isEmpty()) {
            //Highlight the first day selector
            days.get(0).setSelected(true);
            mCurrentSelectedDay = days.get(0);
        }

        mDaySelectorAdapter.setDays(days);
        mDaySelectorAdapter.notifyDataSetChanged();
    }

    @Override
    public void onAppointmentsLoaded(List<Appointment> appointments) {
        if (mMediator.isPatient()) {
            mAgendaPatientAdapter.setAppointments(appointments);
        } else {
            mAgendaDoctorAdapter.setAppointments(appointments);
        }
        Animation animation = new AlphaAnimation(1, 0);
        animation.setDuration(100);
        animation.setInterpolator(new LinearInterpolator());
        animation.setRepeatCount(Animation.RESTART);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                //mOnDataLoaded.onDataLoaded();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                if (mMediator.isPatient()) {
                    mAgendaPatientAdapter.notifyDataSetChanged();
                } else {
                    mAgendaDoctorAdapter.notifyDataSetChanged();
                }
            }
        });
        mAppointments.startAnimation(animation);
    }

    @Override
    public void onDoctorLoaded(Doctor doctor) {
        mDoctorId = doctor.id;
        mDoctorName.setText(doctor.name);
        mDoctorSpeciality.setText(doctor.speciality);
        mProfilePhoto.setImageURI(Uri.parse(doctor.profilePhoto));
    }

    @Override
    public void onReloadAgenda() {
        if (mMediator.isPatient()) {
            mAgendaPatientAdapter.notifyDataSetChanged();
        } else {
            mAgendaDoctorAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onAnyAppointmentSelected() {
        mActionMode.finish();
    }

    /************************************************************************
     * Listeners
     ************************************************************************/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (!mMediator.isPatient()) {
            inflater.inflate(R.menu.delete_tmp_log_out, menu);//TODO: change
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mRealm.executeTransactionAsync(
                new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        realm.deleteAll();
                    }
                },
                new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {

                        //Fabric Answers
                        VutdAnswers.logEvent(VutdAnswers.EVENT.LOGOUT);

                        EntryActivity_.intent(getActivity())
                                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                .start();
                    }
                },
                new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        Logger.e(TAG, "executeTransactionAsync onError: Logout", error);
                    }
                });
        return true;
    }

    @Override
    public void onDaySelected(DaySelectorAdapter.DaySelector daySelector) {
        mCurrentSelectedDay = daySelector;
        mMediator.showAppointments(mDoctorId, daySelector.getDate());
    }

    @Override
    public void onAppointmentClick(Appointment appointment, int position) {
        mMediator.onAppointmentSelected(appointment, mCurrentSelectedDay);
    }

    @Override
    public void onLongClickListener(Appointment appointment, int position) {
        if (!mMediator.isPatient()) {
            mActionMode = ((AppCompatActivity) getActivity()).startSupportActionMode(mActionModeCallback);//TODO: check for improvements
            mMediator.setMultiSelectionMode(true);
            mMediator.onAppointmentSelected(appointment, mCurrentSelectedDay);
        }
    }

    @Override
    public void onMoreOptionsClick(Appointment appointment, MenuItem menuItem, int position) {
        mMediator.onMoreOptionsSelected(appointment, mCurrentSelectedDay, menuItem, position);
    }

    @Override
    public void onCallPatient(String number) {
        AbstractFragmentPermissionsDispatcher.dialPhoneNumberWithCheck(this, number);
    }

    @Override
    public void onAppointmentCreatedByPatient(long lastEntryId) {
        MainActivity mainActivity = (MainActivity) getActivity();
        mainActivity.mMediator.replaceFragment(PatientAppointmentsFragment_
                .builder()
                .lastEntryId(lastEntryId)
                .build());
    }

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.add(R.string.menu_continue);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PREVIEW, "continue"));

            startActivityForResult(ConcurrentAppointmentActivity_.intent(getActivity()).get(), REQUEST_CODE);
            return false;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.MULTI_SELECTION_MODE,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PREVIEW, "dismiss"));

            mMediator.setMultiSelectionMode(false);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            SplashActivity_.intent(getActivity())
                    .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                    .start();
        }
    }
}