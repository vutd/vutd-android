package io.vutd.agenda.controller.activity;

import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.Toast;

import org.androidannotations.annotations.EBean;

import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;

/**
 * Created by andresmariscal on 04/07/16.
 */

/**
 * This class must be extended for every activity that contains a form that calls a wab service
 * through a Handler class
 * @param <R> for the response from the Handler class that extends from <p>BaseResponse</p>
 */
@EBean
public abstract class AbstractFormActivity<R> extends AbstractActivity implements IHandlerResult<Rule, R> {

    public abstract Map<EditText, List<Rule>> attachRules();
    public abstract void okResult(R result);
    public abstract void badResult();

    @Override
    public void onBadResult(Set<Rule> errors) {
        badResult();
        //Show error in EditText
        for(Map.Entry<EditText, List<Rule>> ruleMap : attachRules().entrySet()){
            for(Rule rule : ruleMap.getValue()){
                if(errors.contains(rule)){
                    ruleMap.getKey().requestFocus();
                    ruleMap.getKey().setError(getString(rule.getErrorStringId()));
                    errors.remove(rule);
                }
            }
        }

        //Show other error types
        for(Rule rule : errors){
            Toast.makeText(this,getString(rule.getErrorStringId()),Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onOkResult(R result) {
        okResult(result);
    }
}
