package io.vutd.agenda.controller.activity;

import android.content.Intent;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.calendardatepicker.CalendarDatePickerDialogFragment;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.FacebookSingInController;
import io.vutd.agenda.rest.handler.Rule;

/**
 * Created by andresmariscal on 20/08/16.
 */
@EActivity(R.layout.activity_register)
public class RegisterActivity extends AbstractActivity
        implements CalendarDatePickerDialogFragment.OnDateSetListener,
FacebookSingInController.OnFbUserSignedInListener{

    @ViewById(R.id.tool_bar_title)
    TextView mToolBarTitle;

    @ViewById(R.id.register_name)
    EditText mName;

    @ViewById(R.id.register_last_name)
    EditText mLastName;

    @ViewById(R.id.radio_group_gender)
    RadioGroup mGender;

    @ViewById(R.id.register_birthday)
    EditText mBirthday;

    @Bean
    FacebookSingInController mFacebookSingInController;

    @Override
    public void afterViews() {
        super.afterViews();
        mToolBarTitle.setText(R.string.register_title);
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mFacebookSingInController.setOnFbUserSignedInListener(this);
    }

    @Click(R.id.register_fb_button)
    public void onFacebookClick(){
        mFacebookSingInController.signIn();
    }

    @Click(R.id.navigation_back)
    public void onBackPressNavigation(){
        onBackPressed();
    }

    @Click(R.id.register_birthday)
    public void onBirthdayClick(){
        CalendarDatePickerDialogFragment cdp = new CalendarDatePickerDialogFragment()
                .setOnDateSetListener(this);
        cdp.show(getSupportFragmentManager(), "RegisterActivity");
    }

    @Click(R.id.register_continue_button)
    public void onContinueClick(){
        Set<Rule> errors = validateInputs();
        if(errors.isEmpty()){
            PhoneVerificationActivity_.intent(this)
                    .mBirthday(mBirthday.getText().toString())
                    .mName(mName.getText().toString())
                    .mLastName(mLastName.getText().toString())
                    .mGender(mGender.getCheckedRadioButtonId() == R.id.radio_button_male ? 0 : 1)
                    .start();
        } else {
            for(Map.Entry<EditText, List<Rule>> ruleMap : attachRules().entrySet()){
                for(Rule rule : ruleMap.getValue()){
                    if(errors.contains(rule)){
                        ruleMap.getKey().requestFocus();
                        ruleMap.getKey().setError(getString(rule.getErrorStringId()));
                        errors.remove(rule);
                    }
                }
            }

            //Show other error types
            for(Rule rule : errors){
                Toast.makeText(this,getString(rule.getErrorStringId()),Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookSingInController.onActivityResult(requestCode, resultCode, data);
    }

    private Map<EditText, List<Rule>> attachRules(){
        HashMap<EditText, List<Rule>> rules = new HashMap<>();
        ArrayList<Rule> nameRules = new ArrayList<>();
        nameRules.add(new Rule(Rule.NAME));
        rules.put(mName, nameRules);

        ArrayList<Rule> lastNameRules = new ArrayList<>();
        lastNameRules.add(new Rule(Rule.LAST_NAME));
        rules.put(mLastName, lastNameRules);

        ArrayList<Rule> birthdayRules = new ArrayList<>();
        birthdayRules.add(new Rule(Rule.BIRTHDAY));
        rules.put(mBirthday, birthdayRules);
        return rules;
    }

    private Set<Rule> validateInputs(){
        HashSet<Rule> errors = new HashSet<>();
        if(mName.getText().toString().trim().isEmpty()){
            errors.add(new Rule(Rule.NAME));
        }
        if(mLastName.getText().toString().trim().isEmpty()){
            errors.add(new Rule(Rule.LAST_NAME));
        }
        if(mBirthday.getText().toString().trim().isEmpty()){
            errors.add(new Rule(Rule.BIRTHDAY));
        }
        if(mGender.getCheckedRadioButtonId() == -1){
            errors.add(new Rule(Rule.GENDER));
        }
        return errors;
    }

    @Override
    public void onDateSet(CalendarDatePickerDialogFragment dialog, int year, int monthOfYear, int dayOfMonth) {
        mBirthday.setText(getString(R.string.date_format, year, monthOfYear + 1, dayOfMonth));
    }

    @Override
    public void onFbUserSignedIn(FacebookSingInController.FbUser fbUser) {
        if(fbUser.isAllPermissionGranted) {
            //String birthday = DateTime.parse(fbUser.birthday, DateTimeFormat.forPattern("MM/dd/yyyy")).toString("yyyy-MM-dd");
            PhoneVerificationActivity_.intent(this)
                    .mBirthday("")
                    .mName(fbUser.name)
                    .mLastName(fbUser.lastName)
                    .mGender(fbUser.gender)
                    .mFbId(fbUser.facebookId)
                    .mEmail(fbUser.email)
                    .start();
        } else {

            //Fabric Answers
            VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.FACEBOOK, false,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PERMISSION_DENIED, "birthday"));

            Toast.makeText(this, "We need to know your birthday to continue", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFbUserSignedInFailed(Exception exception) {
        //TODO: get Exception

        //Fabric Answers
        VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.FACEBOOK, false,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FACEBOOK_SIGN_UP_FAIL, "failed"));
    }
}
