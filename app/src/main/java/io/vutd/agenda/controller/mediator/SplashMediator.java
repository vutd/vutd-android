package io.vutd.agenda.controller.mediator;

import android.widget.Toast;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.Set;

import io.vutd.agenda.rest.handler.Rule;

/**
 * Created by andresmariscal on 08/07/16.
 */
@EBean
public class SplashMediator extends AbstractMediator implements SyncMediator.ISyncListener{

    public static final String TAG = "SplashMediator";
    private ILoginListener mOnLoginListener;

    @Bean
    SyncMediator mSyncMediator;

    public void setOnLoginListener(ILoginListener onLoginListener) {
        this.mOnLoginListener = onLoginListener;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected void defineFlow() {
        if(isUserSignedIn){
            mSyncMediator.setContext(mContext);
            mSyncMediator.setRealm(mRealm);
            mSyncMediator.setOnSyncListener(this);
            mSyncMediator.init();
        } else {
            mOnLoginListener.onSessionExpired();
        }
    }

    /************************************************************************
     *                            Callbacks                                 *
     ************************************************************************/

    @Override
    public void onSyncCompleted() {
        mOnLoginListener.onSessionActive();
    }

    @Override
    public void onSyncFailed(Set<Rule> errors) {
        for(Rule rule : errors){
            Toast.makeText(mContext, mContext.getString(rule.getErrorStringId()), Toast.LENGTH_SHORT).show();
        }
        mOnLoginListener.onSessionActive();
    }

    /************************************************************************
     *                            Interfaces                                *
     ************************************************************************/

    public interface ILoginListener{
        void onSessionActive();
        void onSessionExpired();
    }
}
