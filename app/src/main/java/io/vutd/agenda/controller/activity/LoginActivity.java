package io.vutd.agenda.controller.activity;

import android.content.Intent;
import android.os.Build;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.FacebookSingInController;
import io.vutd.agenda.controller.mediator.LoginMediator;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.handler.UserLoginHandler;
import io.vutd.agenda.rest.request.UserLoginRequest;
import io.vutd.agenda.rest.response.UserLoginResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.Utils;
import io.vutd.agenda.view.widget.DilatingDotsProgressBar;

/**
 * Created by andresmariscal on 25/08/16.
 */
@EActivity(R.layout.activity_login)
public class LoginActivity extends AbstractFormActivity<UserLoginResponse>
        implements FacebookSingInController.OnFbUserSignedInListener,
        LoginMediator.IDataDownload {

    public static final String TAG = "LoginActivity";

    @ViewById(R.id.tool_bar_title)
    TextView mToolBarTitle;

    @ViewById(R.id.login_fb_button)
    Button mFacebookButton;

    @ViewById(R.id.navigation_back)
    View mToolBar;

    @ViewById(R.id.login_email)
    EditText mEmail;

    @ViewById(R.id.login_password)
    EditText mPassword;

    @ViewById(R.id.login_button)
    Button mLoginButton;

    @ViewById(R.id.login_forgot_password)
    View mForgotPassword;

    @ViewById(R.id.progress)
    DilatingDotsProgressBar mProgress;

    @Bean
    FacebookSingInController mFacebookSingInController;

    @Bean
    UserLoginHandler mHandler;

    @Bean
    LoginMediator mMediator;

    private boolean mIsFacebookLogin = false;

    @Override
    public void afterViews() {
        super.afterViews();
        mToolBarTitle.setText(R.string.doctor_login_login);
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mHandler.setHandlerResult(this);
        mMediator.setContext(this);
        mMediator.setRealm(mRealm);
        mMediator.setOnDataDownload(this);
        mFacebookSingInController.setOnFbUserSignedInListener(this);
    }

    @Override
    public Map<EditText, List<Rule>> attachRules() {
        HashMap<EditText, List<Rule>> mRules = new HashMap<>();

        //Email rules
        ArrayList<Rule> emailRules = new ArrayList<>();
        emailRules.add(new Rule(Rule.EMAIL));
        emailRules.add(new Rule(Rule.INVALID_EMAIL));
        mRules.put(mEmail, emailRules);

        //Password Rules
        ArrayList<Rule> passwordRule = new ArrayList<>();
        passwordRule.add(new Rule(Rule.PASSWORD));
        passwordRule.add(new Rule(Rule.WRONG_PASSWORD));
        mRules.put(mPassword, passwordRule);
        return mRules;
    }

    @Override
    public void okResult(UserLoginResponse result) {
        mRealm.beginTransaction();
        mRealm.delete(CurrentUser.class);
        CurrentUser currentUser = mRealm.createObject(CurrentUser.class);
        currentUser.transform(result);
        mRealm.commitTransaction();

        //Download all the required data according to the current user
        mMediator.init();

        if (mIsFacebookLogin) {
            //Fabric Answers
            VutdAnswers.logLoginEvent(VutdAnswers.SESSION.FACEBOOK, true,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(result.getRoleId())));
        } else {
            //Fabric Answers
            VutdAnswers.logLoginEvent(VutdAnswers.SESSION.EMAIL, true,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(result.getRoleId())));
        }

    }

    @Override
    public void badResult() {
        loading(true);
        if (mIsFacebookLogin) {
            //Fabric Answers
            VutdAnswers.logLoginEvent(VutdAnswers.SESSION.FACEBOOK, false,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, "patient"));
        } else {
            //Fabric Answers
            VutdAnswers.logLoginEvent(VutdAnswers.SESSION.EMAIL, false,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, "patient"));
        }
    }

    @Click(R.id.login_button)
    public void onLoginRequest() {
        loading(false);
        mIsFacebookLogin = false;
        UserLoginRequest request = new UserLoginRequest();
        request.setPassword(mPassword.getText().toString());
        request.setEmail(mEmail.getText().toString());
        request.setDeviceOs("Android " + Build.VERSION.SDK_INT);
        request.setDeviceName(Utils.getDeviceName());
        request.setDeviceModel(Build.MODEL);
        request.setPushNotificationToken(FirebaseInstanceId.getInstance().getToken());
        mHandler.login(request);
    }

    @Click(R.id.login_fb_button)
    public void onFacebookClick() {
        loading(false);
        mIsFacebookLogin = true;
        mFacebookSingInController.signIn();
    }


    @Click(R.id.navigation_back)
    public void onBackPressNavigation() {
        onBackPressed();
    }

    private void loading(boolean isResponse) {
        mLoginButton.setVisibility(isResponse ? View.VISIBLE : View.INVISIBLE);
        mLoginButton.setEnabled(isResponse);
        mFacebookButton.setEnabled(isResponse);
        mToolBar.setEnabled(isResponse);
        mEmail.setEnabled(isResponse);
        mPassword.setEnabled(isResponse);
        mForgotPassword.setEnabled(isResponse);
        if (isResponse) mProgress.hideNow();
        else mProgress.showNow();
    }

    @Override
    public void onFbUserSignedIn(FacebookSingInController.FbUser fbUser) {
        loading(false);
        if (fbUser.isAllPermissionGranted) {
            UserLoginRequest request = new UserLoginRequest();
            request.setFacebookId(fbUser.facebookId);
            request.setEmail(fbUser.email);
            request.setDeviceOs("Android " + Build.VERSION.SDK_INT);
            request.setDeviceName(Utils.getDeviceName());
            request.setDeviceModel(Build.MODEL);
            request.setPushNotificationToken(FirebaseInstanceId.getInstance().getToken());
            mHandler.login(request);
        } else {
            loading(true);
            //Fabric Answers
            VutdAnswers.logLoginEvent(VutdAnswers.SESSION.FACEBOOK, false,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PERMISSION_DENIED, "birthday"));

            Toast.makeText(this, R.string.login_facebook_permission_denied, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFbUserSignedInFailed(Exception exception) {
        Logger.d(TAG, "onFbUserSignedInFailed");
        loading(true);

        //Fabric Answers
        VutdAnswers.logSignUpEvent(VutdAnswers.SESSION.FACEBOOK, false,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FACEBOOK_SIGN_UP_FAIL, "failed"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mFacebookSingInController.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onDataDownloaded() {
        MainActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .start();
    }

    @Override
    public void onDataDownloadFail(Set<Rule> errors) {
        Logger.d(TAG, "onDataDownloadFail");
        loading(true);
        for (Rule rule : errors) {
            Toast.makeText(this, rule.getErrorStringId(), Toast.LENGTH_SHORT).show();
        }
    }
}
