package io.vutd.agenda.controller.mediator;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.MenuItem;
import android.widget.Toast;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.activity.MainActivity;
import io.vutd.agenda.realm.viewmodel.BookedAppointment;
import io.vutd.agenda.rest.common.AppointmentBookedCommon;
import io.vutd.agenda.rest.handler.ConfirmAppointmentHandler;
import io.vutd.agenda.rest.handler.DeleteAppointmentHandler;
import io.vutd.agenda.rest.handler.GetAppointmentsBookedHandler;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.request.ConfirmAppointmentRequest;
import io.vutd.agenda.rest.request.DeleteAppointmentRequest;
import io.vutd.agenda.rest.request.GetAppointmentsBookedRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.rest.response.GetAppointmentsBookedResponse;
import io.vutd.agenda.utils.GoogleStaticMap;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.VUTDDateTimeUtils;
import io.vutd.agenda.view.adapter.BookedAppointmentAdapter;
import io.vutd.agenda.view.adapter.BookedDaySelectorAdapter;
import io.vutd.agenda.view.dialog.OperationResultProgressDialog;

/**
 * Created by andresmariscal on 08/09/16.
 */
@EBean
public class PatientAppointmentMediator extends AbstractMediator {
    public static final String TAG = "PatientAppointmentMediator";
    private OperationResultProgressDialog mProgress;

    @Bean
    GetAppointmentsBookedHandler mGetAppointmentsBookedHandler;

    @Bean
    DeleteAppointmentHandler mDeleteAppointmentHandler;

    @Bean
    ConfirmAppointmentHandler mConfirmAppointmentHandler;

    private long mLastEntryId;
    private IBookedAppointment mOnBookedAppointment;
    private IBookedAppointmentListener mOnBookedAppointmentListener;

    public void setLastEntryId(long lastEntryId) {
        this.mLastEntryId = lastEntryId;
    }

    public void setOnBookedAppointment(IBookedAppointment mOnBookedAppointment) {
        this.mOnBookedAppointment = mOnBookedAppointment;
    }

    public void setOnBookedAppointmentListener(IBookedAppointmentListener onBookedAppointmentListener) {
        this.mOnBookedAppointmentListener = onBookedAppointmentListener;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mGetAppointmentsBookedHandler.setHandlerResult(mGetAppointmentsBookedCallback);
        mDeleteAppointmentHandler.setHandlerResult(mDeleteAppointmentCallback);
        mConfirmAppointmentHandler.setHandlerResult(mConfirmAppointmentCallback);
    }

    /************************************************************************
     * Initializer
     ************************************************************************/

    @Override
    protected void defineFlow() {
        mProgress = new OperationResultProgressDialog(mContext);
        if (mLastEntryId != 0) {//TODO: logger
            GetAppointmentsBookedRequest request = new GetAppointmentsBookedRequest();
            request.setSessionId(mCurrentUser.sessionId);
            request.setDate(VUTDDateTimeUtils.nowLocal());
            mGetAppointmentsBookedHandler.getAppointments(request);
            mProgress.show();
        } else {
            mOnBookedAppointment.onBookedAppointmentsSuccess(retrieveBookedAppointments());
        }
    }

    /************************************************************************
     * Listeners
     ************************************************************************/

    public void onMenuItemSelected(MenuItem menuItem, BookedAppointmentAdapter.Booked booked) {
        switch (menuItem.getItemId()) {
            case R.id.menu_map:
                GoogleStaticMap.showMap(booked.getAppointment().lat, booked.getAppointment().lon, booked.getAppointment().mcName, mContext);
                break;

            case R.id.menu_call_patient:
                callPhoneNumber(booked);
                break;

            case R.id.menu_cancel_appointment:
                cancelAppointment(booked);
                break;

            case R.id.menu_confirm_appointment:
                confirmAppointment(booked);
                break;
        }
    }

    public void onPhoneNumberClick(BookedAppointmentAdapter.Booked booked){
        callPhoneNumber(booked);
    }

    private void callPhoneNumber(BookedAppointmentAdapter.Booked booked){
        final String[] phones = booked.getAppointment().mcPhones.split(",");
        if (phones.length == 0) {
            //TODO: Toast. no phone registered
        } else if (phones.length == 1) {
            if (mOnBookedAppointmentListener != null)
                mOnBookedAppointmentListener.onCallDoctor(phones[0].replace(" ", ""));
        } else if (phones.length > 1) {
            AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
            builder.setTitle(R.string.item_booked_appointment_dialog_phones_title)
                    .setItems(phones, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            if (mOnBookedAppointmentListener != null)
                                mOnBookedAppointmentListener.onCallDoctor(phones[which].replace(" ", ""));
                        }
                    });
            builder.create().show();
        }

        //Fabric Answer
        VutdAnswers.logEvent(VutdAnswers.EVENT.CALL_DOCTOR_MC,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, booked.getAppointment().startTime));
    }

    /************************************************************************
     * Agenda Transactions
     ************************************************************************/

    private ArrayList<BookedDaySelectorAdapter.BookedDaySelector> retrieveBookedAppointments() {
        ArrayList<BookedDaySelectorAdapter.BookedDaySelector> appointmentsByDay = new ArrayList<>();

        RealmResults<BookedAppointment> bookedAppointments =
                mRealm.where(BookedAppointment.class).findAllSorted("date", Sort.ASCENDING);

        boolean defaultSelection = true;

        //Get dates
        RealmResults<BookedAppointment> totalDays = bookedAppointments.where().distinct("date");
        for(BookedAppointment bookedAppointment : totalDays) {
            ArrayList<BookedAppointmentAdapter.Booked> appointments = new ArrayList<>();
            DateTime dateTime = VUTDDateTimeUtils.parseForPattern(bookedAppointment.date, VUTDDateTimeUtils.VUTD_DATE_FORMAT);

            BookedDaySelectorAdapter.BookedDaySelector daySelector = new BookedDaySelectorAdapter.BookedDaySelector();
            daySelector.setShortMonth(VUTDDateTimeUtils.getMonthShort(dateTime.getMonthOfYear(), mContext));
            daySelector.setDayName(VUTDDateTimeUtils.getWeekday(dateTime.getDayOfWeek(), mContext));
            daySelector.setDayNumber(String.valueOf(dateTime.getDayOfMonth()));
            daySelector.setSelected(defaultSelection);
            defaultSelection = false;

            RealmResults<BookedAppointment> appointmentsByDate = bookedAppointments
                    .where()
                    .equalTo("date", bookedAppointment.date)
                    .findAllSorted("startTime", Sort.ASCENDING);
            for(BookedAppointment appointmentByDate : appointmentsByDate){
                BookedAppointmentAdapter.Booked booked = new BookedAppointmentAdapter.Booked();
                booked.setAppointment(appointmentByDate);
                appointments.add(booked);
            }
            daySelector.setAppointments(appointments);
            appointmentsByDay.add(daySelector);
        }

        return appointmentsByDay;
    }

    /**
     * This will delete the appointment from the doctor schedule
     *
     * @param booked The booked appointment selected
     */
    private void cancelAppointment(final BookedAppointmentAdapter.Booked booked) {
        mSimpleDialog.showYesNoDismissDialog(
                true,
                mContext.getString(R.string.agenda_doctor_main_option_cancel_title),
                mContext.getString(R.string.agenda_doctor_main_option_cancel_message),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        mProgress.show();

                        //Fabric Answers
                        VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_CANCELLED,
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER_ROLE, String.valueOf(mCurrentUser.roleId)),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_NAME, String.valueOf(booked.getAppointment().doctorName)),
                                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.TIME, booked.getAppointment().startTime));

                        mRealm.beginTransaction();
                        booked.getAppointment().bookTypeId = BookedAppointment.DELETED;
                        mRealm.commitTransaction();

                        DeleteAppointmentRequest request = new DeleteAppointmentRequest();
                        request.setAppointmentId(booked.getAppointment().id);
                        request.setSessionId(mCurrentUser.sessionId);
                        request.setReasonId(1);//TODO: change reason id
                        mDeleteAppointmentHandler.deleteAppointment(request);

                    }
                });
    }

    private void confirmAppointment(final BookedAppointmentAdapter.Booked booked) {
        mProgress.show();
        mRealm.beginTransaction();
        booked.getAppointment().status = "pending";
        mRealm.commitTransaction();

        ConfirmAppointmentRequest request = new ConfirmAppointmentRequest();
        request.setSessionId(mCurrentUser.sessionId);
        request.setAppointmentId(booked.getAppointment().id);
        request.setBookDate(VUTDDateTimeUtils.convertToVUTDDateFormat(new DateTime()));// current time
        mConfirmAppointmentHandler.requestConfirmation(request);
    }

    /************************************************************************
     * Callbacks
     ************************************************************************/

    private IHandlerResult<Rule, BaseResponse> mDeleteAppointmentCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            mProgress.showBadResult();
            for(Rule rule : errors){
                Toast.makeText(mContext, rule.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }

            mRealm.beginTransaction();
            BookedAppointment appointment = mRealm.where(BookedAppointment.class)
                    .equalTo("bookTypeId", BookedAppointment.DELETED )
                    .findFirst();
            appointment.bookTypeId = BookedAppointment.BOOKED;
            mRealm.commitTransaction();

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_DELETED,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));

        }

        @Override
        public void onOkResult(BaseResponse result) {
            mProgress.showSuccessfulResult();
            mProgress.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialogInterface) {
                    RealmResults<BookedAppointment> debug = mRealm.where(BookedAppointment.class)
                            .findAll();
                    for(BookedAppointment ap : debug){
                        Logger.e(TAG, ap.toString());
                    }

                    mRealm.beginTransaction();
                    BookedAppointment appointment = mRealm.where(BookedAppointment.class)
                            .equalTo("bookTypeId", BookedAppointment.DELETED )
                            .findFirst();
                    if(appointment != null) appointment.deleteFromRealm();
                    mRealm.commitTransaction();

                    mOnBookedAppointment.onBookedAppointmentsSuccess(retrieveBookedAppointments());

                    //Fabric Answers
                    VutdAnswers.logEvent(VutdAnswers.EVENT.APPOINTMENT_DELETED,
                            new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));
                }
            });
        }
    };

    private IHandlerResult<Rule, GetAppointmentsBookedResponse> mGetAppointmentsBookedCallback
            = new IHandlerResult<Rule, GetAppointmentsBookedResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            ((MainActivity) mContext).mBottomBar.selectTabAtPosition(1, true);
            mProgress.showBadResult();
        }

        @Override
        public void onOkResult(final GetAppointmentsBookedResponse result) {
            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(BookedAppointment.class);
                    for (AppointmentBookedCommon common : result.getAppointmentBookedCommons()) {
                        BookedAppointment booked = realm.createObject(BookedAppointment.class);
                        booked.transform(common);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Logger.d(TAG, "appointmentsBooked.Realm.onSuccess");
                    mProgress.dismiss();
                    mOnBookedAppointment.onBookedAppointmentsSuccess(retrieveBookedAppointments());
                    ((MainActivity) mContext).mBottomBar.selectTabAtPosition(1, true);
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Logger.d(TAG, "appointmentsBooked.Realm.onError", error);
                    mProgress.showBadResult();
                    Set<Rule> errors = new HashSet<>();
                    errors.add(new Rule(Rule.LOCAL_EXCEPTION));
                    ((MainActivity) mContext).mBottomBar.selectTabAtPosition(1, true);
                    mOnBookedAppointment.onBookedAppointmentsFail();
                }
            });
        }
    };

    private IHandlerResult<Rule, BaseResponse> mConfirmAppointmentCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            mProgress.showBadResult();
            for(Rule error : errors) {
                Toast.makeText(mContext, error.getErrorStringId(), Toast.LENGTH_SHORT).show();
            }

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.CONFIRM_APPOINTMENT,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "fail"));
        }

        @Override
        public void onOkResult(BaseResponse result) {
            mProgress.showSuccessfulResult();
            mRealm.beginTransaction();
            BookedAppointment booked = mRealm.where(BookedAppointment.class)
                    .equalTo("status", "pending")
                    .findFirst();
            booked.status = "1";
            mRealm.commitTransaction();
            mOnBookedAppointment.reloadBookedAppointments();

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.CONFIRM_APPOINTMENT,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.CALLBACK, "success"));
        }
    };

    /************************************************************************
     * Interfaces
     ************************************************************************/

    public interface IBookedAppointment {
        void onBookedAppointmentsSuccess(ArrayList<BookedDaySelectorAdapter.BookedDaySelector> appointments);

        void onBookedAppointmentsFail();

        void reloadBookedAppointments();
    }

    public interface IBookedAppointmentListener {
        void onCallDoctor(String number);
    }
}
