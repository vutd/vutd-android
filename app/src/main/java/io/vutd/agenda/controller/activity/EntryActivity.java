package io.vutd.agenda.controller.activity;

import android.widget.ImageView;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;


import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.utils.Animations;

/**
 * Created by andresmariscal on 03/07/16.
 */
@Fullscreen
@EActivity(R.layout.activity_entry)
public class EntryActivity extends AbstractActivity {

    @ViewById(R.id.background_color)
    ImageView mBackgroundColor;

    @Override
    public void afterViews() {
        super.afterViews();
        Animations.colorTransition(this, mBackgroundColor, 2000, -1);
    }

    @Click(R.id.login_doctor_account)
    public void onDoctorAccount(){
        DoctorLoginActivity_.intent(this).start();
    }

    @Click(R.id.login_register_button)
    public void onRegisterClick(){
        RegisterActivity_.intent(this).start();
    }

    @Click(R.id.login_login_button)
    public void onLoginClick(){
        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.LOGOUT);

        LoginActivity_.intent(this).start();
    }

}
