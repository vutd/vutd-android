package io.vutd.agenda.controller.mediator;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.annotation.IdRes;

import org.androidannotations.annotations.EBean;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.MainActivity;
import io.vutd.agenda.controller.fragment.AbstractFragment;
import io.vutd.agenda.controller.fragment.AgendaFragment_;
import io.vutd.agenda.controller.fragment.MoreFragment_;
import io.vutd.agenda.controller.fragment.PatientAppointmentsFragment_;
import io.vutd.agenda.controller.fragment.SearchDoctorFragment;
import io.vutd.agenda.controller.fragment.SearchDoctorFragment_;
import io.vutd.agenda.controller.fragment.TmpFragment_;

/**
 * Created by andresmariscal on 08/07/16.
 */
@EBean
public class MainMediator extends AbstractMediator {

    public static final String TAG = "MainMediator";
    private IBottomNavigationResource mOnBottomNavigationResource;
    private String mDefaultFragment;
    private boolean isDefault = true;

    public MainMediator() {
    }

    public void setOnBottomNavigationResource(IBottomNavigationResource onBottomNavigationResource) {
        this.mOnBottomNavigationResource = onBottomNavigationResource;
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected void defineFlow() {
        switch (mCurrentUser.roleId) {
            case DOCTOR:
                mDefaultFragment = AgendaFragment_.class.getName();
                mOnBottomNavigationResource.resourceByUserRole(R.menu.bottom_navigation_doctor);
                //Set default view
                showView(R.id.bottomBarAgenda);

                break;
            case PATIENT:
                mDefaultFragment = SearchDoctorFragment_.class.getName();
                mOnBottomNavigationResource.resourceByUserRole(R.menu.bottom_navigation_patient);
                //Set default view
                showView(R.id.bottomBarSearch);
                break;
        }
    }

    public void showView(@IdRes int itemId){
        AbstractFragment fragment = null;
        switch (itemId){
            case R.id.bottomBarAgenda:
                fragment = AgendaFragment_.builder().build();
                break;

            /*case R.id.bottomBarCancellations:
                fragment = TmpFragment_.builder().build();
                break;

            case R.id.bottomBarProfile:
                fragment = TmpFragment_.builder().build();
                break;

            case R.id.bottomBarMore:
                fragment = MoreFragment_.builder().build();
                break;

           case R.id.bottomBarDoctors:
                fragment = TmpFragment_.builder().build();
                break;

            case R.id.bottomBarMaps:
                fragment = TmpFragment_.builder().build();
                break;*/

            case R.id.bottomBarAppointments:
                fragment = PatientAppointmentsFragment_.builder().build();
                break;

            case R.id.bottomBarSearch:
                fragment = SearchDoctorFragment_.builder().build();
                break;

            default:
                fragment = TmpFragment_.builder().build();
                break;
        }
        replaceFragment(fragment);
    }

    public void onMenuTabReSelected(@IdRes int menuItemId) {
        switch (menuItemId){
            case R.id.bottomBarSearch:
                SearchDoctorFragment fragment = SearchDoctorFragment_.builder().build();
                replaceFragment(fragment);
                break;
        }
    }

    public void replaceFragment (Fragment fragment){
        String backStateName = fragment.getClass().getName();
        isDefault = mDefaultFragment.equals(backStateName);

        FragmentManager manager = ((Activity) mContext).getFragmentManager();
        if(!popBackStack(backStateName, manager)){
            FragmentTransaction ft = manager.beginTransaction();
            ft.addToBackStack(backStateName);
            ft.replace(R.id.fragment_container, fragment);
            ft.commit();
        }
    }

    private boolean popBackStack(String backStateName, FragmentManager manager){
        return manager.popBackStackImmediate (backStateName, 0);
    }

    public boolean onBackPressed(){
        if(!isDefault){
            popBackStack(mDefaultFragment, ((Activity) mContext).getFragmentManager());
        }
        return isDefault;
    }

    public interface IBottomNavigationResource {
        void resourceByUserRole(int menuXml);
    }
}
