package io.vutd.agenda.controller.mediator;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.HashSet;
import java.util.Set;

import io.realm.Realm;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.realm.model.MedicalCore;
import io.vutd.agenda.realm.model.Workday;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.realm.viewmodel.BookedAppointment;
import io.vutd.agenda.rest.common.AppointmentBookedCommon;
import io.vutd.agenda.rest.common.AppointmentCommon;
import io.vutd.agenda.rest.common.MedicalCoreCommon;
import io.vutd.agenda.rest.common.WorkdayCommon;
import io.vutd.agenda.rest.handler.GetAppointmentsBookedHandler;
import io.vutd.agenda.rest.handler.GetMedicalCoresByDoctorHandler;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.request.GetAppointmentsBookedRequest;
import io.vutd.agenda.rest.request.GetMedicalCoreByDoctorRequest;
import io.vutd.agenda.rest.response.GetAppointmentsBookedResponse;
import io.vutd.agenda.rest.response.GetMedicalCoreByDoctorResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 31/07/16.
 */
@EBean
public class SyncMediator extends AbstractMediator {

    public static final String TAG = "SyncMediator";
    private ISyncListener mOnSyncListener;

    @Bean
    GetMedicalCoresByDoctorHandler mGetMedicalCoresByDoctorHandler;

    @Bean
    GetAppointmentsBookedHandler mGetAppointmentsBookedHandler;

    public void setOnSyncListener(ISyncListener onSyncListener) {
        this.mOnSyncListener = onSyncListener;
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mGetMedicalCoresByDoctorHandler.setHandlerResult(mGetMedicalCoresByDoctorCallback);
        mGetAppointmentsBookedHandler.setHandlerResult(mGetAppointmentsBookedCallback);
    }

    @Override
    public String getTag() {
        return TAG;
    }

    /************************************************************************
     * Initializer
     ************************************************************************/

    @Override
    protected void defineFlow() {
        switch (mCurrentUser.roleId) {
            case DOCTOR:
                GetMedicalCoreByDoctorRequest getMedicalCoreByDoctorRequest = new GetMedicalCoreByDoctorRequest();
                getMedicalCoreByDoctorRequest.setSessionId(mCurrentUser.sessionId);
                getMedicalCoreByDoctorRequest.setDate(VUTDDateTimeUtils.nowLocal());
                mGetMedicalCoresByDoctorHandler.getMedicalCores(getMedicalCoreByDoctorRequest);
                break;

            case PATIENT:
                GetAppointmentsBookedRequest request = new GetAppointmentsBookedRequest();
                request.setSessionId(mCurrentUser.sessionId);
                request.setDate(VUTDDateTimeUtils.nowLocal());
                mGetAppointmentsBookedHandler.getAppointments(request);
                break;

        }
    }

    /************************************************************************
     * Callbacks
     ************************************************************************/

    private IHandlerResult<Rule, GetMedicalCoreByDoctorResponse> mGetMedicalCoresByDoctorCallback
            = new IHandlerResult<Rule, GetMedicalCoreByDoctorResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            mOnSyncListener.onSyncFailed(errors);
        }

        @Override
        public void onOkResult(final GetMedicalCoreByDoctorResponse result) {
            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(Doctor.class);
                    realm.delete(MedicalCore.class);
                    realm.delete(Workday.class);
                    realm.delete(Appointment.class);
                    Doctor doctor = realm.createObject(Doctor.class);
                    doctor.transform(result.getDoctorReference());
                    for (MedicalCoreCommon medicalCore : result.getMedicalCores()) {
                        MedicalCore mc = realm.createObject(MedicalCore.class);
                        mc.transform(medicalCore);

                        for (WorkdayCommon commonDay : medicalCore.getDays()) {
                            Workday day = realm.createObject(Workday.class);
                            day.transform(commonDay);

                            day.doctorId = doctor.id;
                            day.medicalCoreId = mc.id;

                        }
                        for (AppointmentCommon appointmentCommon : medicalCore.getAppointments()) {
                            Appointment appointment = realm.createObject(Appointment.class);
                            appointment.transform(appointmentCommon);
                            appointment.medicalCoreId = mc.id;
                            appointment.doctorId = doctor.id;
                        }
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Logger.d(TAG, "executeTransactionAsync.onSuccess(): Download doctor data");
                    mOnSyncListener.onSyncCompleted();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Logger.e(TAG, "executeTransactionAsync onError: Download medical cores", error);
                    Set<Rule> errors = new HashSet();
                    errors.add(new Rule(Rule.LOCAL_EXCEPTION));
                    mOnSyncListener.onSyncFailed(errors);
                }
            });

        }
    };

    private IHandlerResult<Rule, GetAppointmentsBookedResponse> mGetAppointmentsBookedCallback
            = new IHandlerResult<Rule, GetAppointmentsBookedResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            Logger.d(TAG, "GetAppointmentsBookedCallback onBadResult");
            mOnSyncListener.onSyncFailed(errors);
        }

        @Override
        public void onOkResult(final GetAppointmentsBookedResponse result) {
            mRealm.executeTransactionAsync(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.delete(BookedAppointment.class);
                    for (AppointmentBookedCommon common : result.getAppointmentBookedCommons()) {
                        BookedAppointment booked = realm.createObject(BookedAppointment.class);
                        booked.transform(common);
                    }
                }
            }, new Realm.Transaction.OnSuccess() {
                @Override
                public void onSuccess() {
                    Logger.d(TAG, "appointmentsBooked.Realm.onSuccess");
                    mOnSyncListener.onSyncCompleted();
                }
            }, new Realm.Transaction.OnError() {
                @Override
                public void onError(Throwable error) {
                    Logger.d(TAG, "appointmentsBooked.Realm.onError");
                    Set<Rule> errors = new HashSet<>();
                    errors.add(new Rule(Rule.LOCAL_EXCEPTION));
                    mOnSyncListener.onSyncFailed(errors);
                }
            });
        }
    };

    /************************************************************************
     * Interfaces
     ************************************************************************/

    public interface ISyncListener {
        void onSyncCompleted();

        void onSyncFailed(Set<Rule> errors);
    }
}
