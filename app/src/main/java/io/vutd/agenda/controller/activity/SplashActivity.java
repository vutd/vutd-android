package io.vutd.agenda.controller.activity;

import android.app.ActivityOptions;
import android.content.Intent;
import android.view.View;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.Fullscreen;
import org.androidannotations.annotations.ViewById;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.mediator.SplashMediator;
import io.vutd.agenda.view.widget.DilatingDotsProgressBar;


/**
 * Created by andresmariscal on 03/07/16.
 */
@Fullscreen
@EActivity(R.layout.activity_splash)
public class SplashActivity extends AbstractActivity implements SplashMediator.ILoginListener {

    @ViewById(R.id.splash_logo)
    View mLogo;

    @ViewById(R.id.splash_progress)
    DilatingDotsProgressBar mProgressBar;

    @Bean
    SplashMediator mSplashMediator;

    @AfterViews
    public void afterViews(){
        mProgressBar.showNow();
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mSplashMediator.setContext(this);
        mSplashMediator.setRealm(mRealm);
        mSplashMediator.setOnLoginListener(this);
        mSplashMediator.init();
    }

    @Override
    public void onSessionActive() {
        MainActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .start();
    }

    @Override
    public void onSessionExpired() {
        ActivityOptions options = ActivityOptions.makeCustomAnimation(SplashActivity.this,R.anim.fade_in, R.anim.fade_in);
        EntryActivity_.intent(SplashActivity.this).withOptions(options.toBundle()).start();
        finish();
    }
}
