package io.vutd.agenda.controller.mediator;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.HashSet;
import java.util.Set;

import io.realm.Realm;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.realm.model.MedicalCore;
import io.vutd.agenda.realm.model.Workday;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.realm.viewmodel.BookedAppointment;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 13/07/16.
 */
@EBean
public class LoginMediator extends AbstractMediator implements SyncMediator.ISyncListener{
    public static final String TAG = "LoginMediator";
    private IDataDownload mOnDataDownload;

    @Bean
    SyncMediator mSyncMediator;

    public void setOnDataDownload(IDataDownload mOnDataDownload) {
        this.mOnDataDownload = mOnDataDownload;
    }

    @Override
    public void afterInjects(){
        super.afterInjects();
        mSyncMediator.setOnSyncListener(this);
    }

    @Override
    public String getTag() {
        return TAG;
    }

    @Override
    protected void defineFlow() {
        mSyncMediator.setContext(mContext);
        mSyncMediator.setRealm(mRealm);
        //Clear data base
        mRealm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                //Ensure to keep the current user info
                realm.delete(Doctor.class);
                realm.delete(MedicalCore.class);
                realm.delete(Workday.class);
                realm.delete(Appointment.class);
                realm.delete(BookedAppointment.class);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                //start downloading all the dependencies
                mSyncMediator.init();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(Throwable error) {
                Logger.e(TAG, "clearDatabase.onError", error.getCause());
                HashSet<Rule> errors = new HashSet<>();
                errors.add(new Rule(Rule.LOCAL_EXCEPTION));
                mOnDataDownload.onDataDownloadFail(errors);
            }
        });

    }

    /************************************************************************
     *                            Listeners                                 *
     ************************************************************************/

    @Override
    public void onSyncCompleted() {
        mOnDataDownload.onDataDownloaded();
    }

    @Override
    public void onSyncFailed(Set<Rule> errors) {
        mOnDataDownload.onDataDownloadFail(errors);
    }

    /************************************************************************
     *                            Interfaces                                *
     ************************************************************************/

    public interface IDataDownload {
        void onDataDownloaded();
        void onDataDownloadFail(Set<Rule> errors);
    }
}
