package io.vutd.agenda.controller.fragment;

import android.Manifest;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;

import io.realm.Realm;
import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.MainActivity;
import io.vutd.agenda.utils.Logger;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;

/**
 * Created by andresmariscal on 09/07/16.
 */
@EFragment
@RuntimePermissions
public abstract class AbstractFragment extends Fragment {
    protected Realm mRealm;

    public abstract String getClassTag();

    @AfterInject
    public void afterInjects(){
        Logger.d(getClassTag(), "afterInjects");
        mRealm = Realm.getDefaultInstance();
    }

    @AfterViews
    public void afterViews(){
        Logger.d(getClassTag(), "afterViews");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    public void dismissLoading(){
        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).onDataLoaded();
        }
    }

    public void onDataLoaded(){
        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).onDataLoaded();
        }
    }

    public void onDataLoading(){
        if(getActivity() instanceof MainActivity){
            ((MainActivity) getActivity()).onDataLoading();
        }
    }

    @NeedsPermission(Manifest.permission.CALL_PHONE)
    public void dialPhoneNumber(String number){
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + number));
        startActivity(callIntent);
    }

    @OnShowRationale(Manifest.permission.CALL_PHONE)
    public void onCallPhoneRationale(final PermissionRequest request){
        new AlertDialog.Builder(getActivity())
                .setMessage(R.string.call_phone_rationale)
                .setPositiveButton(R.string.button_allow, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.proceed();
                    }
                })
                .setNegativeButton(R.string.button_deny, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        request.cancel();
                    }
                })
                .show();
    }

    @OnPermissionDenied(Manifest.permission.CALL_PHONE  )
    public void onCallPhonePermissionDenied(){
        Toast.makeText(getActivity(), R.string.permission_phone_call_denied, Toast.LENGTH_SHORT).show();
    }

    @OnNeverAskAgain(Manifest.permission.CALL_PHONE)
    void showNeverAskForCallPhone() {
        Toast.makeText(getActivity(), R.string.permission_phone_call_never_ask, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        AbstractFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }
}
