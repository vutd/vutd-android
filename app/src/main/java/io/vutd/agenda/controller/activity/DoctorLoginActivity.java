package io.vutd.agenda.controller.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.LoginEvent;
import com.google.firebase.iid.FirebaseInstanceId;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.mediator.LoginMediator;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.handler.UserLoginHandler;
import io.vutd.agenda.rest.request.UserLoginRequest;
import io.vutd.agenda.rest.response.UserLoginResponse;
import io.vutd.agenda.utils.Utils;
import io.vutd.agenda.view.widget.DilatingDotsProgressBar;

/**
 * Created by andresmariscal on 03/07/16.
 */
@EActivity(R.layout.activity_doctor_login)
public class DoctorLoginActivity extends AbstractFormActivity<UserLoginResponse> implements LoginMediator.IDataDownload {

    @ViewById(R.id.tool_bar_title)
    TextView mToolBarTitle;

    @ViewById(R.id.doctor_login_email)
    EditText mEmail;

    @ViewById(R.id.doctor_login_password)
    EditText mPassword;

    @ViewById(R.id.doctor_login_toolbar)
    View mToolBar;

    @ViewById(R.id.login_banner)
    View mBanner;

    @ViewById(R.id.login_login_button)
    View mLoginButton;

    @ViewById(R.id.login_forgot_password)
    View mForgotPassword;

    @ViewById(R.id.progress)
    DilatingDotsProgressBar mProgress;

    @Bean
    UserLoginHandler mHandler;

    @Bean
    LoginMediator mMediator;

    @Override
    public void afterInjects() {
        super.afterInjects();
        mHandler.setHandlerResult(this);
        mMediator.setContext(this);
        mMediator.setRealm(mRealm);
        mMediator.setOnDataDownload(this);
    }

    @AfterViews
    public void afterViews() {
        attachRules();
        mToolBarTitle.setText(R.string.doctor_login_login);
    }

    public Map attachRules() {
        HashMap<EditText, List<Rule>> mRules = new HashMap<>();

        //Email rules
        ArrayList<Rule> emailRules = new ArrayList<>();
        emailRules.add(new Rule(Rule.EMAIL));
        emailRules.add(new Rule(Rule.INVALID_EMAIL));
        mRules.put(mEmail, emailRules);

        //Password Rules
        ArrayList<Rule> passwordRule = new ArrayList<>();
        passwordRule.add(new Rule(Rule.PASSWORD));
        passwordRule.add(new Rule(Rule.WRONG_PASSWORD));
        mRules.put(mPassword, passwordRule);
        return mRules;
    }

    @Override
    public void okResult(UserLoginResponse result) {
        //Download all the required data according to the current user
        if (result.getRoleId() == 3) {
            mRealm.beginTransaction();
            mRealm.delete(CurrentUser.class);
            CurrentUser currentUser = mRealm.createObject(CurrentUser.class);
            currentUser.transform(result);
            mRealm.commitTransaction();
            mMediator.init();
            //Fabric Answers
            Answers.getInstance().logLogin(new LoginEvent()
                    .putMethod("email_password")
                    .putCustomAttribute("user_role", result.getRoleId())
                    .putSuccess(true));
        } else if(result.getRoleId() == 4) {
            Toast.makeText(this, "Invalid user, Use desktop app instead", Toast.LENGTH_SHORT).show();
            loading(true);
        } else {
            Toast.makeText(this, "Invalid user", Toast.LENGTH_SHORT).show();
            loading(true);
        }


    }

    @Override
    public void badResult() {
        loading(true);

        //Fabric Answers
        Answers.getInstance().logLogin(new LoginEvent()
                .putMethod("email_password")
                .putCustomAttribute("user_role", "doctor_mc")
                .putSuccess(false));
    }

    @Click(R.id.login_login_button)
    public void onLoginRequest() {
        loading(false);
        UserLoginRequest request = new UserLoginRequest();
        request.setPassword(mPassword.getText().toString());
        request.setEmail(mEmail.getText().toString());
        request.setDeviceOs("Android " + Build.VERSION.SDK_INT);
        request.setDeviceName(Utils.getDeviceName());
        request.setDeviceModel(Build.MODEL);
        request.setPushNotificationToken(FirebaseInstanceId.getInstance().getToken());
        mHandler.login(request);
    }

    @Click(R.id.navigation_back)
    public void onBackPressNavigation() {
        onBackPressed();
    }

    private void loading(boolean isResponse) {
        mLoginButton.setVisibility(isResponse ? View.VISIBLE : View.INVISIBLE);
        mLoginButton.setEnabled(isResponse);
        mToolBar.setEnabled(isResponse);
        mEmail.setEnabled(isResponse);
        mPassword.setEnabled(isResponse);
        mForgotPassword.setEnabled(isResponse);
        if (isResponse) mProgress.hideNow();
        else mProgress.showNow();
    }

    @Override
    public void onDataDownloaded() {
        loading(true);
        MainActivity_.intent(this)
                .flags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK)
                .start();
    }

    @Override
    public void onDataDownloadFail(Set<Rule> errors) {
        onBadResult(errors);
    }
}
