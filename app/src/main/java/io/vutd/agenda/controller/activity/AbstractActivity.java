package io.vutd.agenda.controller.activity;

import android.support.v7.app.AppCompatActivity;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import io.realm.Realm;
import io.vutd.agenda.view.dialog.OperationResultProgressDialog;

/**
 * Created by andresmariscal on 06/07/16.
 */
@EActivity
public abstract class AbstractActivity extends AppCompatActivity {

    protected Realm mRealm;
    protected OperationResultProgressDialog mProgress;

    @AfterInject
    public void afterInjects() {
        mRealm = Realm.getDefaultInstance();
        mProgress = new OperationResultProgressDialog(this);
    }

    @AfterViews
    public void afterViews(){

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mRealm.close();
        if(mProgress.isShowing()){
            mProgress.dismiss();
        }
    }
}
