package io.vutd.agenda.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 20/08/16.
 */
@EBean
public class FacebookSingInController {

    public static final String TAG = "FacebookSingInController";
    private OnFbUserSignedInListener mOnFbUserSignedInListener;
    private List<String> mPermissions = new ArrayList<>(Arrays.asList(
            "email",
            "user_friends",
            //"user_likes",//TODO: search for the doctors pages
            //"user_birthday", // TODO: Temporal removed to avoid facebook submission review
            "public_profile"));

    @RootContext
    Activity mActivity;

    private CallbackManager mFacebookCallbackManager;

    public void setOnFbUserSignedInListener(OnFbUserSignedInListener mOnFbUserSignedInListener) {
        this.mOnFbUserSignedInListener = mOnFbUserSignedInListener;
    }

    /**
     * Starts the facebook sign in process
     */
    public void signIn()
    {
        Logger.d(TAG , "signIn");
        LoginManager.getInstance().logInWithReadPermissions(mActivity, mPermissions);
    }

    @AfterInject
    public void afterInjects(){
        mFacebookCallbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(mFacebookCallbackManager, mFacebookCallback);
    }

    private final FacebookCallback<LoginResult> mFacebookCallback =
            new FacebookCallback<LoginResult>()
            {
                @Override
                public void onSuccess(LoginResult loginResult)
                {
                    Logger.d(TAG, "Success");
                    getUserInformation();
                }

                @Override
                public void onCancel()
                {
                    Logger.d(TAG, "FacebookCallback.onCancel");
                    onFbUserSignedInFailed(null);
                }

                @Override
                public void onError(FacebookException exception)
                {
                    Logger.e(TAG, "FacebookCallback.onError", exception);
                    onFbUserSignedInFailed(exception);
                }
            };

    public void onActivityResult(final int requestCode, final int resultCode, final Intent data)
    {
        Logger.d(TAG, "onActivityResult");
        mFacebookCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void getUserInformation()
    {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(),
                new GraphRequest.GraphJSONObjectCallback()
                {
                    @Override
                    public void onCompleted(JSONObject json, GraphResponse response)
                    {
                        Logger.w(TAG, "GraphRequest.getUserInformation - " +
                                "json: " + json + "," +
                                "error: " + response.getError());
                        if (response.getError() != null)
                        {
                            onFbUserSignedInFailed(response.getError().getException());
                        }
                        else
                        {
                            FbUser fbUser = new FbUser();
                            fbUser.isAllPermissionGranted = json.has("email");
                            fbUser.facebookId = json.optString("id");
                            fbUser.name = json.optString("first_name");
                            fbUser.lastName = json.optString("last_name");
                            fbUser.email = json.optString("email");
                            //fbUser.birthday = json.optString("birthday");
                            fbUser.gender = json.optString("gender").trim().toLowerCase().equals("male")
                                    ? FbUser.GENDER_MALE
                                    : FbUser.GENDER_FEMALE;

                            onFbUserSignedIn(fbUser);
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,first_name,last_name,email,gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void onFbUserSignedIn(FbUser fbUser)
    {
        if (mOnFbUserSignedInListener != null)
        {
            mOnFbUserSignedInListener.onFbUserSignedIn(fbUser);
        }
    }

    private void onFbUserSignedInFailed(Exception exception)
    {
        if (mOnFbUserSignedInListener != null)
        {
            mOnFbUserSignedInListener.onFbUserSignedInFailed(exception);
        }
    }

    public class FbUser
    {
        public static final int GENDER_MALE = 0;
        public static final int GENDER_FEMALE = 1;

        public String facebookId;
        public String name;
        public String lastName;
        public String email;
        public String picture;
        //public String birthday;
        public int gender;
        public boolean isAllPermissionGranted;
    }

    public interface OnFbUserSignedInListener
    {
        void onFbUserSignedIn(FbUser fbUser);

        void onFbUserSignedInFailed(Exception exception);
    }
}
