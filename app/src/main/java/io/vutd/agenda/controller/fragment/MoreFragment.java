package io.vutd.agenda.controller.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.androidannotations.annotations.EFragment;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 09/07/16.
 */
@EFragment(R.layout.fragment_more)
public class MoreFragment extends AbstractFragment {

    public static final String TAG = "MoreFragment";

    @Override
    public String getClassTag() {
        return TAG;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                onDataLoaded();
            }
        }, 1500);
        return null;
    }
}
