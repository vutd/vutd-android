package io.vutd.agenda.controller.mediator;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmResults;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.controller.activity.MainActivity;
import io.vutd.agenda.controller.fragment.AgendaFragment_;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.realm.viewmodel.SearchDoctorHistory;
import io.vutd.agenda.rest.common.DoctorProfileCommon;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.handler.SearchDoctorByPhoneHandler;
import io.vutd.agenda.rest.request.SearchDoctorByPhoneRequest;
import io.vutd.agenda.rest.response.SearchDoctorsByPhoneResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.adapter.SearchAdapter;

/**
 * Created by andresmariscal on 31/08/16.
 */
@EBean
public class SearchMediator extends AbstractMediator {

    public static final String TAG = "SearchMediator";

    @Bean
    SearchDoctorByPhoneHandler mSearchDoctorByPhoneHandler;

    private ISearchHistory mOnSearchHistoryListener;
    private ISearchResult mOnSearchResultListener;

    public void setOnSearchHistoryListener(ISearchHistory mOnSearchHistoryListener) {
        this.mOnSearchHistoryListener = mOnSearchHistoryListener;
    }

    public void setOnSearchResultListener(ISearchResult mOnSearchResult) {
        this.mOnSearchResultListener = mOnSearchResult;
    }

    @Override
    public void afterInjects() {
        super.afterInjects();
        mSearchDoctorByPhoneHandler.setHandlerResult(mOnSearchDoctorByPhoneCallback);
    }

    @Override
    public String getTag() {
        return TAG;
    }

    /************************************************************************
     * Initializer
     ************************************************************************/

    @Override
    protected void defineFlow() {
        showHistory();
    }

    /************************************************************************
     * Search functions
     ************************************************************************/

    private void showDoctorAgenda(long doctorId, long medicalCoreId) {
        ((MainActivity) mContext).mMediator.replaceFragment(
                AgendaFragment_
                        .builder()
                        .mDoctorId(doctorId)
                        .mMedicalCoreId(medicalCoreId)
                        .build());
    }

    public void searchDoctorByPhone(String phone) {
        SearchDoctorByPhoneRequest request = new SearchDoctorByPhoneRequest();
        request.setPhone(phone);
        request.setSessionId(mCurrentUser.sessionId);
        mSearchDoctorByPhoneHandler.searchDoctors(request);
    }

    public void clearSearch() {
        clearDirtyObjects();
        showHistory();
    }

    private void clearDirtyObjects(){
        RealmResults<SearchDoctorHistory> temporalResults = mRealm.where(SearchDoctorHistory.class)
                .findAll();
        mRealm.beginTransaction();
        temporalResults
                .where()
                .equalTo("isSearchRecord", 1)
                .equalTo("frequency", -1)
                .findAll()
                .deleteAllFromRealm();
        for(SearchDoctorHistory history : temporalResults) {
            if(history.isValid()) {
                history.isSearchRecord = 0;
            }
        }
        mRealm.commitTransaction();
    }

    public void deleteHistoryItem(SearchDoctorHistory item, int position) {
        mRealm.beginTransaction();
        //Fabric Answers
        VutdAnswers.logEvent(VutdAnswers.EVENT.DOCTOR_DELETED_FROM_HISTORY,
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, item.doctor.id + ""),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.MEDICAL_CORE_ID, item.medicalCoreId + ""),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER, getCurrentUserName()),
                new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FREQUENCY, item.frequency + ""));
        item.deleteFromRealm();
        mRealm.commitTransaction();
        mOnSearchHistoryListener.onRecordDeleted(position, (int) mRealm.where(SearchDoctorHistory.class).count());
    }

    private void showHistory() {
        RealmResults<SearchDoctorHistory> history = mRealm.where(SearchDoctorHistory.class).findAll();
        SearchDoctorHistory.debugTable(history);
        if (history.isEmpty()) {
            mOnSearchHistoryListener.onEmptyHistory();
        } else {
            ArrayList<SearchDoctorHistory> historyResult = new ArrayList<>();
            historyResult.addAll(history);
            mOnSearchHistoryListener.onHistoryLoaded(historyResult);
        }
    }

    /************************************************************************
     * Listeners
     ************************************************************************/

    public void onDoctorSelected(SearchDoctorHistory item) {
        mRealm.beginTransaction();
        if (item.frequency >= 1) {
            item.frequency++;

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.DOCTOR_AGENDA_SELECTED,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, item.doctor.id + ""),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.MEDICAL_CORE_ID, item.medicalCoreId + ""),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER, getCurrentUserName()),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FREQUENCY, "" + item.frequency));
        } else {
            item.frequency = 1;

            //Fabric Answers
            VutdAnswers.logEvent(VutdAnswers.EVENT.DOCTOR_AGENDA_SELECTED,
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.DOCTOR_ID, item.doctor.id + ""),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.MEDICAL_CORE_ID, item.medicalCoreId + ""),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.USER, getCurrentUserName()),
                    new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.FREQUENCY, "first_time"));

        }
        mRealm.commitTransaction();
        clearDirtyObjects();
        showDoctorAgenda(item.doctor.id, item.medicalCoreId);
    }

    /************************************************************************
     * Callbacks
     ************************************************************************/

    private IHandlerResult<Rule, SearchDoctorsByPhoneResponse> mOnSearchDoctorByPhoneCallback
            = new IHandlerResult<Rule, SearchDoctorsByPhoneResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            if (isAsyncActive) {
                clearDirtyObjects();
                if (errors.contains(new Rule(Rule.INVALID_USER))) {
                    mOnSearchResultListener.onNoResultsFound();
                } else {
                    mOnSearchResultListener.onSearchFailed();
                }
            }
        }

        @Override
        public void onOkResult(final SearchDoctorsByPhoneResponse result) {
            if (result.getDoctorCommons().isEmpty()) {
                mOnSearchResultListener.onNoResultsFound();
            } else {
                mRealm.executeTransactionAsync(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (DoctorProfileCommon common : result.getDoctorCommons()) {
                            SearchDoctorHistory history = realm.where(SearchDoctorHistory.class)
                                    .equalTo("doctor.id", common.getDoctorId())
                                    .findFirst();
                            if (history == null) {
                                history = realm.createObject(SearchDoctorHistory.class);
                                Doctor doctor = realm.createObject(Doctor.class);
                                history.frequency = -1;
                                doctor.id = common.getDoctorId();
                                doctor.name = common.getName() + " " + common.getLastName();
                                doctor.profilePhoto = common.getProfilePhoto() == null ? "" : common.getProfilePhoto();
                                doctor.speciality = common.getSpecialities().get(0).getName();
                                doctor.gender = common.getGender();
                                history.doctor = doctor;
                                history.medicalCoreId = common.getMedicalCoreId();
                                history.mcAddress = common.getMcAddress();
                            }
                            history.isSearchRecord = 1;
                        }
                    }
                }, new Realm.Transaction.OnSuccess() {
                    @Override
                    public void onSuccess() {
                        RealmResults<SearchDoctorHistory> history = mRealm.where(SearchDoctorHistory.class).findAll();
                        ArrayList<SearchDoctorHistory> historyResult = new ArrayList<>();
                        historyResult.addAll(history);
                        if (isAsyncActive) {
                            mOnSearchResultListener.onResults(historyResult);
                        }
                    }
                }, new Realm.Transaction.OnError() {
                    @Override
                    public void onError(Throwable error) {
                        Logger.e(TAG, "searchDoctorsByPhone.Realm.onError", error.getCause());
                        if (isAsyncActive) {
                            mOnSearchResultListener.onSearchFailed();
                        }
                    }
                });
            }
        }
    };


    /************************************************************************
     * Interfaces
     ************************************************************************/

    public interface ISearchHistory {
        void onEmptyHistory();

        void onHistoryLoaded(ArrayList<SearchDoctorHistory> doctors);

        void onRecordDeleted(int position, int totalRows);
    }

    public interface ISearchResult {
        void onSearchFailed();

        void onNoResultsFound();

        void onResults(ArrayList<SearchDoctorHistory> doctors);
    }
}
