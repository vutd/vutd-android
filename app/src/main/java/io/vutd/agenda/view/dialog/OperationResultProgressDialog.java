package io.vutd.agenda.view.dialog;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import io.vutd.agenda.R;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.view.widget.DilatingDotsProgressBar;


/**
 * Created by andresmariscal on 25/07/16.
 */
public class OperationResultProgressDialog extends AbstractDialog {

    public static final String TAG = "OperationResultProgressDialog";
    private DilatingDotsProgressBar mProgressBar;
    private ImageView mResult;

    public OperationResultProgressDialog(Context context) {
        super(context, R.style.VUTDDialog);
    }

    public OperationResultProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected OperationResultProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_operation_result);

        mProgressBar = (DilatingDotsProgressBar) findViewById(R.id.dialog_progress);
        mResult = (ImageView) findViewById(R.id.show_result);

        mProgressBar.showNow();

        mResult.setVisibility(View.GONE);
        setCancelable(false);
    }

    public void showBadResult(){
        Logger.d(TAG, "showBadResult");
        mProgressBar.setVisibility(View.GONE);
        mResult.setVisibility(View.VISIBLE);
        mResult.setImageResource(R.drawable.ic_clear_black_24dp);//TODO: Change icon
        animateResult(mResult);
    }

    public void showSuccessfulResult(){
        Logger.d(TAG, "showSuccessfulResult");
        mProgressBar.setVisibility(View.GONE);
        mResult.setVisibility(View.VISIBLE);
        mResult.setImageResource(R.drawable.ic_done_black_48dp);//TODO: Change icon
        animateResult(mResult);
    }

    private void animateResult(View view){
        Animation animation = new AlphaAnimation(0, 1);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        reset();
                        dismiss();
                    }
                }, 750);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {}
        });
        view.startAnimation(animation);
    }

    private void reset(){
        mProgressBar.setVisibility(View.VISIBLE);
        mResult.setVisibility(View.GONE);
    }
}
