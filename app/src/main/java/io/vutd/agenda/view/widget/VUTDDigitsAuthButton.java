package io.vutd.agenda.view.widget;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.digits.sdk.android.DigitsAuthButton;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 20/08/16.
 */
public class VUTDDigitsAuthButton extends DigitsAuthButton {
    public VUTDDigitsAuthButton(Context c) {
        super(c);
        init();
    }
    public VUTDDigitsAuthButton(Context c, AttributeSet attrs) {
        super(c, attrs);
        init();
    }

    public VUTDDigitsAuthButton(Context c, AttributeSet attrs, int defStyle) {
        super(c, attrs, defStyle);
        init();
    }
    private void init() {
        if (isInEditMode()) {
            return;
        }

        setBackgroundColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));

        // Modifying the text here..
        setText(getResources().getString(R.string.dgts__create_account));

        setTextColor(ContextCompat.getColor(getContext(), R.color.white));
    }
}
