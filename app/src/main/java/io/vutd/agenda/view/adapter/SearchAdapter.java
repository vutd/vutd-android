package io.vutd.agenda.view.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

import io.vutd.agenda.R;
import io.vutd.agenda.realm.viewmodel.SearchDoctorHistory;

/**
 * Created by andresmariscal on 01/09/16.
 */
@EBean
public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<SearchDoctorHistory> mDoctors = new ArrayList<>();
    private String mHeaderTitle;
    private OnSearchItemCLickListener mOnSearchItemCLickListener;
    private OnEmptyListener mOnEmptyListener;

    public SearchAdapter() {
    }

    public void setDoctors(ArrayList<SearchDoctorHistory> mDoctors) {
        this.mDoctors = mDoctors;
    }

    public void setHeaderTitle(String mHeaderTitle) {
        this.mHeaderTitle = mHeaderTitle;
    }

    public void setOnSearchItemCLickListener(OnSearchItemCLickListener onSearchItemCLickListener) {
        this.mOnSearchItemCLickListener = onSearchItemCLickListener;
    }

    public void setOnEmptyListener(OnEmptyListener onEmptyListener) {
        this.mOnEmptyListener = onEmptyListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new SearchDoctorHeaderVH(
                        LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.item_search_doctor_header, parent, false));

            case TYPE_ITEM:
                return new SearchDoctorItemVH(
                        LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.item_search_doctor_item, parent, false));

            default:
                throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    public void deleteAtPosition(int position){
        mDoctors.remove(position - 1);
        if(!mDoctors.isEmpty()) {
            notifyItemRangeRemoved(position, mDoctors.size());
        } else {
            notifyDataSetChanged();
            if(mOnEmptyListener != null) mOnEmptyListener.onEmptyAdapter();
        }
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private SearchDoctorHistory getItem(int position) {
        return mDoctors.get(position - 1);
    }

    public void clear() {
        if (mDoctors != null) {
            mDoctors.clear();
            notifyDataSetChanged();
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof SearchDoctorItemVH) {
            SearchDoctorHistory doctor = getItem(position);
            SearchDoctorItemVH itemVH = (SearchDoctorItemVH) holder;

            if (doctor.isSearchRecord == 1) {
                itemVH.delete.setVisibility(View.GONE);
            }
                itemVH.speciality.setText(doctor.doctor.speciality);
                itemVH.profilePhoto.setImageURI(Uri.parse(doctor.doctor.profilePhoto));
                itemVH.doctorName.setText(doctor.doctor.name);
                itemVH.address.setText(doctor.mcAddress);

        } else if (holder instanceof SearchDoctorHeaderVH) {
            SearchDoctorHeaderVH headerVH = (SearchDoctorHeaderVH) holder;
            headerVH.title.setText(mHeaderTitle);
        }
    }

    @Override
    public int getItemCount() {
        return mDoctors.size() + 1;
    }

    public class SearchDoctorHeaderVH extends RecyclerView.ViewHolder {
        TextView title;


        public SearchDoctorHeaderVH(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    public class SearchDoctorItemVH extends RecyclerView.ViewHolder implements View.OnClickListener {

        SimpleDraweeView profilePhoto;
        TextView doctorName;
        TextView speciality;
        TextView address;
        View delete;

        public SearchDoctorItemVH(View itemView) {
            super(itemView);

            profilePhoto = (SimpleDraweeView) itemView.findViewById(R.id.profile_photo);
            doctorName = (TextView) itemView.findViewById(R.id.doctor_name);
            speciality = (TextView) itemView.findViewById(R.id.speciality);
            address = (TextView) itemView.findViewById(R.id.address);
            delete = itemView.findViewById(R.id.delete_row);
            itemView.setOnClickListener(this);
            delete.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnSearchItemCLickListener != null) {
                if(view.getId() == R.id.delete_row){
                    mOnSearchItemCLickListener.onDeleteClick(getItem(getAdapterPosition()), getAdapterPosition());
                } else {
                    mOnSearchItemCLickListener.onItemClick(getItem(getAdapterPosition()));
                }
            }
        }
    }

    public interface OnEmptyListener{
        void onEmptyAdapter();
    }

    public interface OnSearchItemCLickListener {
        void onItemClick(SearchDoctorHistory item);
        void onDeleteClick(SearchDoctorHistory item, int position);
    }
}
