package io.vutd.agenda.view.dialog;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import org.androidannotations.annotations.EBean;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 20/07/16.
 */
@EBean
public class SimpleDialog {

    private Context mContext;
    protected AlertDialog.Builder mBuilder = null;
    protected AlertDialog mAlert = null;

    public SimpleDialog() {
    }

    public void init(Context context){
        mContext = context;
        mBuilder = new AlertDialog.Builder(context);
    }

    public void setMessage(int messageResId)
    {
        setMessage(mContext.getString(messageResId));
    }

    public void setMessage(String message)
    {
        mBuilder.setMessage(message);
    }

    public void setTitle(int titleResId)
    {
        setTitle(mContext.getString(titleResId));
    }

    public void setTitle(String title)
    {
        mBuilder.setTitle(title);
    }

    public void setYesButton(DialogInterface.OnClickListener listener){
        setPositiveButton(mContext.getString(R.string.dialog_button_yes), listener);
    }

    public void setOkButton(DialogInterface.OnClickListener listener){
        setPositiveButton(mContext.getString(R.string.dialog_button_ok), listener);
    }

    public void setPositiveButton(String caption, DialogInterface.OnClickListener listener){
        mBuilder.setPositiveButton(caption, listener);
    }

    public void setCancelButton(DialogInterface.OnClickListener listener){
        setNegativeButton(mContext.getString(R.string.dialog_button_cancel), listener);
    }

    public void setNegativeButton(DialogInterface.OnClickListener listener){
        setNegativeButton(mContext.getString(R.string.dialog_button_no), listener);
    }

    public void setNegativeButton(String caption, DialogInterface.OnClickListener listener){
        mBuilder.setNegativeButton(caption, listener);
    }

    public void setCancelable(boolean cancelable)
    {
        mBuilder.setCancelable(cancelable);
    }

    public void show()
    {
        mAlert = mBuilder.create();
        mAlert.show();
    }

    public void dismiss(){
        if (mAlert != null && mAlert.isShowing()){
            mAlert.dismiss();
        }
    }

    public AlertDialog getAlertDialog()
    {
        return mAlert;
    }

    public void showDialog(Context context, boolean cancelable, int messageResId, DialogInterface.OnClickListener listener){
        showDialog(cancelable, context.getString(messageResId), listener);
    }

    public void showDialog(boolean cancelable, String message, DialogInterface.OnClickListener listener){
        this.setMessage(message);
        this.setCancelable(cancelable);
        this.setOkButton(listener);
        this.show();
    }

    public void showYesNoDismissDialog(boolean cancelable, String title, String message,
                                       DialogInterface.OnClickListener yesListener){
        final SimpleDialog dialog = this;
        this.setTitle(title);
        this.setMessage(message);
        this.setCancelable(cancelable);
        this.setPositiveButton(mContext.getString(R.string.dialog_button_yes), yesListener);
        this.setNegativeButton(mContext.getString(R.string.dialog_button_no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialog.dismiss();
            }
        });
        this.show();
    }

    public void showYesNoDialog(boolean cancelable, String title, String message,
                                               DialogInterface.OnClickListener yesListener,
                                               DialogInterface.OnClickListener noListener){
        this.setTitle(title);
        this.setMessage(message);
        this.setCancelable(cancelable);
        this.setPositiveButton(mContext.getString(R.string.dialog_button_yes), yesListener);
        this.setNegativeButton(mContext.getString(R.string.dialog_button_no), noListener);
        this.show();
    }

    public void showOkDialog(boolean cancelable, String title, String message,
                                            DialogInterface.OnClickListener okListener){
        this.setTitle(title);
        this.setMessage(message);
        this.setCancelable(cancelable);
        this.setOkButton(okListener);
        this.show();
    }

    public void showOkCancelDialog(boolean cancelable, String title, String message,
                                                  DialogInterface.OnClickListener okListener,
                                                  DialogInterface.OnClickListener cancelListener){
        this.setTitle(title);
        this.setMessage(message);
        this.setCancelable(cancelable);
        this.setOkButton(okListener);
        this.setCancelButton(cancelListener);
        this.show();
    }
}
