package io.vutd.agenda.view.dialog;

import android.app.Dialog;
import android.content.Context;

/**
 * Created by andresmariscal on 23/07/16.
 */
public abstract class AbstractDialog extends Dialog{

    protected abstract String getTag();

    public AbstractDialog(Context context) {
        super(context);
    }

    public AbstractDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected AbstractDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }
}
