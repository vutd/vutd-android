package io.vutd.agenda.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;
import org.joda.time.DateTime;

import java.util.ArrayList;

import io.vutd.agenda.R;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 23/10/16.
 */
@EBean
public class ConcurrentAppointmentAdapter extends RecyclerView.Adapter<ConcurrentAppointmentAdapter.ViewHolder> {

    private ArrayList<Appointment> mAppointments = new ArrayList<>();

    public void setAppointments(ArrayList<Appointment> appointments) {
        this.mAppointments = appointments;
    }

    public ArrayList<Appointment> getAppointments() {
        return mAppointments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_concurrent_description, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Appointment appointment = mAppointments.get(position);
        DateTime dateTime = VUTDDateTimeUtils.parseForPattern(appointment.date, VUTDDateTimeUtils.VUTD_DATE_FORMAT);
        String dayOfWeek = VUTDDateTimeUtils.getWeekday(dateTime.getDayOfWeek(), holder.itemView.getContext());
        String monthOfYear = VUTDDateTimeUtils.getMonth(dateTime.getMonthOfYear(), holder.itemView.getContext());
        String dayOfMonth = dateTime.getDayOfMonth() + "";
        holder.mStartTime.setText(appointment.startTime);
        holder.mDay.setText(holder.itemView.getContext().getString(
                R.string.concurrent_date_format, dayOfWeek, monthOfYear, dayOfMonth)
        );
    }

    @Override
    public int getItemCount() {
        return mAppointments.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        TextView mStartTime;
        TextView mDay;

        ViewHolder(View itemView) {
            super(itemView);
            mStartTime = (TextView) itemView.findViewById(R.id.item_concurrent_start_time);
            mDay = (TextView) itemView.findViewById(R.id.item_concurrent_day);
        }
    }
}
