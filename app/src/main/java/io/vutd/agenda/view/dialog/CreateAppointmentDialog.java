package io.vutd.agenda.view.dialog;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.R;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 23/07/16.
 */
public class CreateAppointmentDialog extends AbstractDialog {

    public static final String TAG = "CreateAppointmentDialog";

    private TextView mTitle;
    private EditText mPatientName;
    private EditText mPatientPhone;
    private Button mCancel;
    private Button mBook;
    private boolean mEditable;

    private String mName;
    private String mPhone;

    private String mTitleHeader = "";

    private OnDecisionClick mOnDecisionClick;

    public CreateAppointmentDialog(Context context) {
        super(context, R.style.VUTDDialog);
    }

    public CreateAppointmentDialog(Context context, String name, String phone, boolean editable){
        super(context);
        this.mName = name;
        this.mPhone = phone;
        this.mEditable = editable;
    }

    public CreateAppointmentDialog(Context context, int themeResId) {
        super(context, themeResId);
    }

    protected CreateAppointmentDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setTitle(String mTitle) {
        this.mTitleHeader = mTitle;
    }

    public void setOnDecisionClick(OnDecisionClick mOnDecisionClick) {
        this.mOnDecisionClick = mOnDecisionClick;
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Logger.d(TAG, "onCreate");
        setContentView(R.layout.dialog_create_appointment);
        mTitle = (TextView) findViewById(R.id.dialog_create_appointment_title);
        mPatientName = (EditText) findViewById(R.id.dialog_create_appointment_patient_name);
        mPatientPhone = (EditText) findViewById(R.id.dialog_create_appointment_patient_phone);
        mCancel = (Button) findViewById(R.id.dialog_create_appointment_cancel);
        mBook = (Button) findViewById(R.id.dialog_create_appointment_book);

        mTitle.setText(mTitleHeader);
        mCancel.setOnClickListener(mOnClickListener);
        mBook.setOnClickListener(mOnClickListener);

        if(mName != null && mPhone != null){
            mPatientPhone.setText(mPhone);
            mPatientName.setText(mName);

            mPatientPhone.setFocusable(mEditable);
        }
    }

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.dialog_create_appointment_cancel){
                Logger.d(TAG, "onCancelClick");
                dismiss();
                mOnDecisionClick.onCancelClick();
            } else if(view.getId() == R.id.dialog_create_appointment_book){
                Logger.d(TAG, "onConfirmClick");
                if(validateInputs(mPatientName.getText().toString().trim(), mPatientPhone.getText().toString().trim())){
                    mOnDecisionClick.onConfirmClick(mPatientName.getText().toString(), mPatientPhone.getText().toString());
                    dismiss();
                }
            }
        }
    };

    private boolean validateInputs(String patientName, String patientPhone){
        boolean isValid = true;
        Set<Rule> errors = new HashSet<>();
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.PATIENT_NAME), patientName);
        fieldsRequired.put(new Rule(Rule.PATIENT_PHONE), patientPhone);

        for(Map.Entry<Rule, String> validator: fieldsRequired.entrySet()){
            if(validator.getValue() == null || validator.getValue().trim().isEmpty()){
                errors.add(validator.getKey());
            }
        }

        HashMap<EditText, List<Rule>> mRules = new HashMap<>();

        ArrayList<Rule> patientNameRule = new ArrayList<>();
        patientNameRule.add(new Rule(Rule.PATIENT_NAME));
        mRules.put(mPatientName, patientNameRule);

        ArrayList<Rule> patientPhoneRule = new ArrayList<>();
        patientPhoneRule.add(new Rule(Rule.PATIENT_PHONE));
        mRules.put(mPatientPhone, patientPhoneRule);

        for(Map.Entry<EditText, List<Rule>> ruleMap : mRules.entrySet()){
            for(Rule rule : ruleMap.getValue()){
                if(errors.contains(rule)){
                    ruleMap.getKey().requestFocus();
                    ruleMap.getKey().setError(getContext().getString(rule.getErrorStringId()));
                    errors.remove(rule);
                    isValid = false;
                }
            }
        }

        return isValid;
    }

    public interface OnDecisionClick {
        void onConfirmClick(String patientName, String phone);
        void onCancelClick();
    }
}
