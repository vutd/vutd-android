package io.vutd.agenda.view.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import org.androidannotations.annotations.EBean;
import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 09/07/16.
 */
@EBean
public class DaySelectorAdapter extends RecyclerView.Adapter<DaySelectorAdapter.ViewHolder>{

    private List<DaySelector> mDays = new ArrayList<>();
    private IDaySelector mOnDaySelected;
    private int mLastPositionSelected = -1;

    public DaySelectorAdapter() {
    }

    public void setDays(List<DaySelector> mDays) {
        this.mDays = mDays;
    }

    public void setOnDaySelected(IDaySelector mOnDaySelected) {
        this.mOnDaySelected = mOnDaySelected;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_day_selector, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //Assign Month name if it is first
        holder.mMonthName.setVisibility(View.GONE);
        holder.mDivider.setVisibility(View.GONE);
        holder.mSelector.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(),R.color.white));
        holder.mDayNumber.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));

        if(mDays.get(position).getDayNumber() == 1){
            holder.mMonthName.setVisibility(View.VISIBLE);
            holder.mMonthName.setText(mDays.get(position).getMonth());
            holder.mDivider.setVisibility(View.VISIBLE);
        }

        holder.mDayNumber.setText(mDays.get(position).getDayNumber() + "");
        holder.mDayName.setText(mDays.get(position).getDayName());

        //Define behavior selection
        if(mDays.get(position).isSelected()){
            holder.mSelector.setBackgroundColor(ContextCompat.getColor(holder.itemView.getContext(),R.color.cyan300_alpha));
            holder.mDayNumber.setTextColor(ContextCompat.getColor(holder.itemView.getContext(), R.color.colorAccent));
            mLastPositionSelected = position;
            //This call back refresh the current selection in the agenda fragment
            mOnDaySelected.onDaySelected(mDays.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return mDays.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView mDayNumber;
        private TextView mDayName;
        private CardView mSelector;
        private View mDivider;
        private TextView mMonthName;

        public ViewHolder(View itemView) {
            super(itemView);
            mDayName = (TextView) itemView.findViewById(R.id.day_name);
            mDayNumber = (TextView) itemView.findViewById(R.id.day_number);
            mSelector = (CardView) itemView.findViewById(R.id.day_selector);
            mDivider = itemView.findViewById(R.id.day_selector_moth_divider);
            mMonthName = (TextView) itemView.findViewById(R.id.day_selector_month_name);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mDays.get(mLastPositionSelected).setSelected(false);
            mDays.get(getAdapterPosition()).setSelected(true);
            notifyDataSetChanged();
        }
    }

    public static class DaySelector {
        private boolean selected;
        private int dayNumber;
        private String month;
        private String dayName;
        private DateTime date;
        private int weekday;


        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }

        public int getDayNumber() {
            return dayNumber;
        }

        public void setDayNumber(int dayNumber) {
            this.dayNumber = dayNumber;
        }

        public String getDayName() {
            return dayName;
        }

        public void setDayName(String dayName) {
            this.dayName = dayName;
        }

        public DateTime getDate() {
            return date;
        }

        public void setDate(DateTime date) {
            this.date = date;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public int getWeekday() {
            return weekday;
        }

        public void setWeekday(int weekday) {
            this.weekday = weekday;
        }
    }

    public interface IDaySelector{
        void onDaySelected(DaySelector daySelector);
    }
}
