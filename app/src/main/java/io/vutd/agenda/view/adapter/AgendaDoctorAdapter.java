package io.vutd.agenda.view.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.R;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 09/10/16.
 */
@EBean
public class AgendaDoctorAdapter extends RecyclerView.Adapter<AgendaDoctorAdapter.ViewHolder> {

    private List<Appointment> mAppointments = new ArrayList<>();
    private OnAppointmentClickListener mOnAppointmentClickListener;

    public AgendaDoctorAdapter() {
    }

    public void setOnAppointmentSelected(OnAppointmentClickListener mOnAppointmentClickListener) {
        this.mOnAppointmentClickListener = mOnAppointmentClickListener;
    }

    public void setAppointments(List<Appointment> mAppointments) {
        this.mAppointments = mAppointments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_appointment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        Appointment appointment = mAppointments.get(position);
        if (position == 0) {
            holder.timeHeader.setPadding(
                    0,
                    (int) holder.itemView.getContext().getResources().getDimension(R.dimen.agenda_screen_agenda_appointment_first_padding_top),
                    0,
                    0
            );
        }
        holder.timeHeader.setText(VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.startTime));
        holder.clearView();

        switch ((int)appointment.bookTypeId) {
            case Appointment.FREE:
                holder.statusMessage.setVisibility(View.VISIBLE);

                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_free));
                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.teal));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.appointmentIcon.setImageResource(R.drawable.free_appointment_36dp);
                holder.itemView.setOnLongClickListener(holder);

                if(appointment.selectionMode == 0) {
                    holder.moreOptions.setVisibility(View.VISIBLE);
                    holder.popupMenu = new PopupMenu(holder.itemView.getContext(), holder.moreOptions, Gravity.END, 0, R.style.AppointmentOptions);
                    holder.popupMenu.inflate(R.menu.menu_doctor_free);
                    holder.popupMenu.setOnMenuItemClickListener(holder);
                    holder.moreOptions.setOnClickListener(holder);
                } else {
                    holder.selectedIcon.setVisibility(View.VISIBLE);
                    int resourceImage = appointment.isSelected == 1 ? R.drawable.ic_check_dark_disabled : R.drawable.circle_divider;
                    holder.selectedIcon.setImageResource(resourceImage);
                }

                break;
            case Appointment.BOOKED:
                //Set Visibilities
                holder.patientName.setVisibility(View.VISIBLE);
                holder.status.setVisibility(View.VISIBLE);
                holder.patientPhone.setVisibility(View.VISIBLE);
                holder.phoneTile.setVisibility(View.VISIBLE);

                //Fields Values
                holder.patientPhone.setText(appointment.patientPhone);
                holder.patientName.setText(appointment.patientName);

                //Set Icon
                holder.appointmentIcon.setImageResource(R.drawable.appointment_booked_36dp);

                //Set Colors
                holder.patientName.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.white));
                switch (Integer.parseInt(appointment.status)) {
                    case 0:
                        holder.highlightStatusOut.setCardBackgroundColor(
                                ContextCompat.getColor(holder.itemView.getContext(), R.color.orange_500));
                        holder.status.setTextColor(
                                ContextCompat.getColor(holder.itemView.getContext(), R.color.orange_500));
                        holder.status.setText(holder.itemView.getContext().getString(R.string.item_appointment_pending));
                        break;
                    case 1:
                        holder.highlightStatusOut.setCardBackgroundColor(
                                ContextCompat.getColor(holder.itemView.getContext(), R.color.green_500));
                        holder.status.setTextColor(
                                ContextCompat.getColor(holder.itemView.getContext(), R.color.green_500));
                        holder.status.setText(holder.itemView.getContext().getString(R.string.item_appointment_confirm));
                        break;
                }

                if(appointment.subscribers != 0) {
                    holder.subscriberIcon.setVisibility(View.VISIBLE);
                    holder.subscriberCount.setVisibility(View.VISIBLE);

                    holder.subscriberCount.setText(appointment.subscribers + "");
                }

                if(appointment.isConfirmationSend == -99) {
                    holder.checkStatus.setVisibility(View.VISIBLE);
                    holder.checkStatus.setImageResource(R.drawable.ic_wait_24);
                }

                if(appointment.isConfirmationSend == 1) {
                    holder.checkStatus.setVisibility(View.VISIBLE);
                    holder.checkStatus.setImageResource(R.drawable.ic_check_24);
                }

                if(appointment.isConfirmationSend == 1 && appointment.isConfirmationDelivered == 1){
                    holder.checkStatus.setVisibility(View.VISIBLE);
                    holder.checkStatus.setImageResource(R.drawable.ic_double_check_24);
                }

                //Set Menu
                holder.moreOptions.setVisibility(View.VISIBLE);
                holder.popupMenu = new PopupMenu(holder.itemView.getContext(),
                        holder.moreOptions, Gravity.END, 0, R.style.AppointmentOptions);
                holder.popupMenu.inflate(R.menu.menu_doctor_booked);
                holder.popupMenu.setOnMenuItemClickListener(holder);
                holder.moreOptions.setOnClickListener(holder);
                break;

            case Appointment.REST_TIME:
                holder.statusMessage.setVisibility(View.VISIBLE);

                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_rest));
                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.appointmentIcon.setImageResource(R.drawable.rest_appointment_36dp);
                holder.moreOptions.setEnabled(false);
                break;
            case Appointment.BUSY:
                holder.statusMessage.setVisibility(View.VISIBLE);

                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_busy));
                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.red_200));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.appointmentIcon.setImageResource(R.drawable.busy_appointment_36dp);

                holder.moreOptions.setVisibility(View.VISIBLE);
                holder.popupMenu = new PopupMenu(holder.itemView.getContext(), holder.moreOptions, Gravity.END, 0, R.style.AppointmentOptions);
                holder.popupMenu.inflate(R.menu.menu_doctor_busy);
                holder.popupMenu.setOnMenuItemClickListener(holder);
                holder.moreOptions.setOnClickListener(holder);

                break;
        }
    }

    @Override
    public int getItemCount() {
        return mAppointments.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, PopupMenu.OnMenuItemClickListener, View.OnLongClickListener {

        TextView timeHeader;
        CardView highlightStatus;
        CardView highlightStatusOut;
        View moreOptions;
        TextView status;
        TextView patientName;
        TextView patientPhone;
        TextView statusMessage;
        TextView phoneTile;
        ImageView appointmentIcon;
        ImageView checkStatus;
        ImageView selectedIcon;
        View subscriberIcon;
        TextView subscriberCount;
        PopupMenu popupMenu;
        int position = -1;

        public ViewHolder(View itemView) {
            super(itemView);
            timeHeader = (TextView) itemView.findViewById(R.id.appointment_header_time);
            highlightStatus = (CardView) itemView.findViewById(R.id.appointment_highlight_status);
            highlightStatusOut = (CardView) itemView.findViewById(R.id.appointment_highlight_status_out);
            moreOptions = itemView.findViewById(R.id.appointment_more_options);
            status = (TextView) itemView.findViewById(R.id.appointment_status);
            patientName = (TextView) itemView.findViewById(R.id.appointment_patient_name);
            patientPhone = (TextView) itemView.findViewById(R.id.appointment_patient_phone);
            statusMessage = (TextView) itemView.findViewById(R.id.appointment_status_message);
            phoneTile = (TextView) itemView.findViewById(R.id.appointment_phone_title);
            appointmentIcon = (ImageView) itemView.findViewById(R.id.appointment_icon);
            checkStatus = (ImageView) itemView.findViewById(R.id.appointment_check_status);
            subscriberIcon = itemView.findViewById(R.id.appointment_subscribers_icon);
            subscriberCount = (TextView) itemView.findViewById(R.id.appointment_subscribers_count);
            selectedIcon = (ImageView) itemView.findViewById(R.id.appointment_select);
            itemView.setOnClickListener(this);
        }

        void clearView() {
            //Set all visibility gone
            status.setVisibility(View.GONE);
            patientPhone.setVisibility(View.GONE);
            patientName.setVisibility(View.GONE);
            statusMessage.setVisibility(View.GONE);
            checkStatus.setVisibility(View.GONE);
            subscriberCount.setVisibility(View.GONE);
            subscriberIcon.setVisibility(View.GONE);
            selectedIcon.setVisibility(View.GONE);
            phoneTile.setVisibility(View.GONE);
            status.setTextColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            statusMessage.setTextColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            highlightStatus.setCardBackgroundColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            highlightStatusOut.setCardBackgroundColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            popupMenu = null;
            moreOptions.setVisibility(View.GONE);
        }

        @Override
        public void onClick(View view) {
            position = getAdapterPosition();
            if (view.getId() == R.id.appointment_more_options) {
                popupMenu.show();
            } else {
                mOnAppointmentClickListener.onAppointmentClick(mAppointments.get(position), position);
            }
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            mOnAppointmentClickListener.onMoreOptionsClick(mAppointments.get(position), item, position);
            return true;
        }

        @Override
        public boolean onLongClick(View view) {
            position = getAdapterPosition();
            mOnAppointmentClickListener.onLongClickListener(mAppointments.get(position), position);
            return true;
        }
    }
}
