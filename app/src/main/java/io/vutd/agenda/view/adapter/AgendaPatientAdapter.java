package io.vutd.agenda.view.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.R;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 16/10/16.
 */
@EBean
public class AgendaPatientAdapter extends RecyclerView.Adapter<AgendaPatientAdapter.ViewHolder> {

    private List<Appointment> mAppointments = new ArrayList<>();
    private OnAppointmentClickListener mOnAppointmentClickListener;

    public void setOnAppointmentClickListener(OnAppointmentClickListener mOnAppointmentClickListener) {
        this.mOnAppointmentClickListener = mOnAppointmentClickListener;
    }

    public void setAppointments(List<Appointment> mAppointments) {
        this.mAppointments = mAppointments;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_patient_appointment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Appointment appointment = mAppointments.get(position);
        holder.setIsRecyclable(false);
        holder.timeHeader.setText(VUTDDateTimeUtils.changeMilitaryToAmPm(appointment.startTime));
        holder.clearView();
        switch ((int) appointment.bookTypeId) {
            case Appointment.FREE:
                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_free));

                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.teal));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.appointmentIcon.setImageResource(R.drawable.free_appointment_36dp);
                break;

            case Appointment.REST_TIME:
                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_rest));

                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.gray300));
                holder.appointmentIcon.setImageResource(R.drawable.rest_appointment_36dp);
                break;

            default:
                holder.statusMessage.setText(holder.itemView.getContext().getString(R.string.item_appointment_booked));

                holder.statusMessage.setTextColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));
                holder.highlightStatus.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), android.R.color.background_light));
                holder.highlightStatusOut.setCardBackgroundColor(
                        ContextCompat.getColor(holder.itemView.getContext(), R.color.colorPrimaryDark));
                holder.appointmentIcon.setImageResource(R.drawable.appointment_booked_36dp);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mAppointments.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView timeHeader;
        CardView highlightStatus;
        CardView highlightStatusOut;
        TextView statusMessage;
        ImageView appointmentIcon;
        int position = -1;

        public ViewHolder(View itemView) {
            super(itemView);
            timeHeader = (TextView) itemView.findViewById(R.id.appointment_header_time);
            highlightStatus = (CardView) itemView.findViewById(R.id.appointment_highlight_status);
            highlightStatusOut = (CardView) itemView.findViewById(R.id.appointment_highlight_status_out);
            statusMessage = (TextView) itemView.findViewById(R.id.appointment_status_message);
            appointmentIcon = (ImageView) itemView.findViewById(R.id.appointment_icon);
            itemView.setOnClickListener(this);
        }

        void clearView() {
            //Set all visibility gone
            statusMessage.setTextColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            highlightStatus.setCardBackgroundColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
            highlightStatusOut.setCardBackgroundColor(
                    ContextCompat.getColor(itemView.getContext(), R.color.white));
        }

        @Override
        public void onClick(View view) {
            position = getAdapterPosition();
            mOnAppointmentClickListener.onAppointmentClick(mAppointments.get(position), position);
        }
    }
}
