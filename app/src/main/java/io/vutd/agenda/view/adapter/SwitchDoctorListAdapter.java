package io.vutd.agenda.view.adapter;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;


import io.realm.RealmResults;
import io.vutd.agenda.R;
import io.vutd.agenda.realm.model.Doctor;

/**
 * Created by andresmariscal on 30/07/16.
 */
public class SwitchDoctorListAdapter extends RecyclerView.Adapter<SwitchDoctorListAdapter.ViewHolder> {

    private RealmResults<Doctor> mDoctors;
    private OnItemClickListener mOnItemClickListener;

    public SwitchDoctorListAdapter() {
    }

    public RealmResults<Doctor>  getDoctors() {
        return mDoctors;
    }

    public void setDoctors(RealmResults<Doctor>  doctors) {
        this.mDoctors = doctors;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.mOnItemClickListener = onItemClickListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_switch_doctor, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.profilePhoto.setImageURI(Uri.parse(mDoctors.get(position).profilePhoto));
        holder.doctorName.setText(mDoctors.get(position).name);//TODO: add Dr. or Dra.
    }

    @Override
    public int getItemCount() {
        return mDoctors != null ? mDoctors.size() : 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView doctorName;
        SimpleDraweeView profilePhoto;

        public ViewHolder(View itemView) {
            super(itemView);
            doctorName = (TextView) itemView.findViewById(R.id.item_switch_doctor_name);
            profilePhoto = (SimpleDraweeView) itemView.findViewById(R.id.item_switch_doctor_photo_profile);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            mOnItemClickListener.onItemClick(mDoctors.get(getAdapterPosition()));
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Doctor doctorReference);
    }
}
