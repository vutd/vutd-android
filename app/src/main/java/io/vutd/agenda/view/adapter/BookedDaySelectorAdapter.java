package io.vutd.agenda.view.adapter;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

import io.vutd.agenda.R;
import io.vutd.agenda.utils.Logger;


/**
 * Created by andresmariscal on 10/09/16.
 */
@EBean
public class BookedDaySelectorAdapter extends RecyclerView.Adapter<BookedDaySelectorAdapter.DaySelectorVH> {

    private ArrayList<BookedDaySelector> mDaySelectors = new ArrayList<>();
    private IBookedDaySelector mOnBookedDaySelector;
    private int mLastPositionSelected = -1;

    public void setDaySelectors(ArrayList<BookedDaySelector> mDaySelectors) {
        //Dummy fill to get a better view
        if(mDaySelectors.size() < 5) {
            BookedDaySelector dummy = new BookedDaySelector();
            dummy.setSelected(false);
            dummy.setAppointments(null);
            dummy.setShortMonth("");
            dummy.setDayName("");
            dummy.setDayNumber("");
            for(int i = mDaySelectors.size() ; i < 5 ; i++) {
                mDaySelectors.add(dummy);
            }
        }
        this.mDaySelectors = mDaySelectors;
    }

    public void setOnBookedDaySelector(IBookedDaySelector mOnBookedDaySelector) {
        this.mOnBookedDaySelector = mOnBookedDaySelector;
    }

    @Override
    public DaySelectorVH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_booked_day_selector, parent, false);
        return new DaySelectorVH(view);
    }

    @Override
    public void onBindViewHolder(DaySelectorVH holder, int position) {
        BookedDaySelector day = mDaySelectors.get(position);
        // Initial Values
        holder.selection.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.circle_booked_day_selector_background));
        holder.empty.setVisibility(View.INVISIBLE);

        if(day.isSelected()){
            holder.selection.setBackground(ContextCompat.getDrawable(holder.itemView.getContext(), R.drawable.circle_booked_day_selector_selected));
            mLastPositionSelected = position;
        }

        holder.dayName.setText(day.getDayName());
        holder.dayNumber.setText(day.getDayNumber());
        holder.month.setText(day.getShortMonth());

        if(day.getAppointments() == null) {
            holder.dayContainer.setVisibility(View.INVISIBLE);
            holder.empty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return mDaySelectors.size();
    }

    public void clear(){
        mDaySelectors.clear();
        notifyDataSetChanged();
    }

    public class DaySelectorVH extends RecyclerView.ViewHolder implements View.OnClickListener{
        View empty;
        View selection;
        View dayContainer;
        TextView dayName;
        TextView dayNumber;
        TextView month;

        public DaySelectorVH(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            empty = itemView.findViewById(R.id.empty);
            dayContainer = itemView.findViewById(R.id.day_container);
            selection = itemView.findViewById(R.id.selection);
            dayName = (TextView) itemView.findViewById(R.id.day_name);
            dayNumber = (TextView) itemView.findViewById(R.id.day_number);
            month = (TextView) itemView.findViewById(R.id.month);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if(mDaySelectors.get(position).getAppointments() != null) {
                mDaySelectors.get(mLastPositionSelected).setSelected(false);
                mDaySelectors.get(position).setSelected(true);
                notifyDataSetChanged();
                if (mOnBookedDaySelector != null)
                    mOnBookedDaySelector.onItemClick(mDaySelectors.get(position).getAppointments());
            }
        }
    }

    public static class BookedDaySelector {
        boolean mSelected;
        String mDayName;
        String mDayNumber;
        String mShortMonth;
        ArrayList<BookedAppointmentAdapter.Booked> mAppointments = new ArrayList<>();

        public boolean isSelected() {
            return mSelected;
        }

        public void setSelected(boolean selected) {
            this.mSelected = selected;
        }

        public String getDayName() {
            return mDayName;
        }

        public void setDayName(String dayName) {
            this.mDayName = dayName;
        }

        public String getDayNumber() {
            return mDayNumber;
        }

        public void setDayNumber(String dayNumber) {
            this.mDayNumber = dayNumber;
        }

        public String getShortMonth() {
            return mShortMonth;
        }

        public void setShortMonth(String shortMonth) {
            this.mShortMonth = shortMonth;
        }

        public ArrayList<BookedAppointmentAdapter.Booked> getAppointments() {
            return mAppointments;
        }

        public void setAppointments(ArrayList<BookedAppointmentAdapter.Booked> appointments) {
            this.mAppointments = appointments;
        }
    }

    public interface IBookedDaySelector{
        void onItemClick(ArrayList<BookedAppointmentAdapter.Booked> appointments);
    }
}
