package io.vutd.agenda.view.adapter;

import android.view.MenuItem;

import io.vutd.agenda.realm.viewmodel.Appointment;

/**
 * Created by andresmariscal on 16/10/16.
 */

public interface OnAppointmentClickListener {
    void onAppointmentClick(Appointment appointment, int position);
    void onLongClickListener(Appointment appointment, int position);
    void onMoreOptionsClick(Appointment appointment, MenuItem menuItem, int position);
}
