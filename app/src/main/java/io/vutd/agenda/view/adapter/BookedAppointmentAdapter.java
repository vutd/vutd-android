package io.vutd.agenda.view.adapter;

import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;

import io.vutd.agenda.R;
import io.vutd.agenda.app.VutdAnswers;
import io.vutd.agenda.realm.viewmodel.BookedAppointment;
import io.vutd.agenda.utils.GoogleStaticMap;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 10/09/16.
 */
@EBean
public class BookedAppointmentAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private ArrayList<Booked> mAppointments = new ArrayList<>();
    private BookedDaySelectorAdapter mHorizontalRecyclerAdapter;
    private OnBookedAppointmentSelected mOnBookedAppointmentSelected;

    public void setAppointments(ArrayList<Booked> mAppointments) {
        this.mAppointments = mAppointments;
    }

    public void setHorizontalRecyclerAdapter(BookedDaySelectorAdapter mHorizontalRecyclerAdapter) {
        this.mHorizontalRecyclerAdapter = mHorizontalRecyclerAdapter;
    }

    public void setOnBookedAppointmentSelected(OnBookedAppointmentSelected mOnBookedAppointmentSelected) {
        this.mOnBookedAppointmentSelected = mOnBookedAppointmentSelected;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_HEADER:
                return new DayRecyclerHeaderVH(
                        LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.item_horizontal_recycler, parent, false));

            case TYPE_ITEM:
                return new BookedAppointmentVH(
                        LayoutInflater
                                .from(parent.getContext())
                                .inflate(R.layout.item_booked_appointment, parent, false));

            default:
                throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionHeader(position))
            return TYPE_HEADER;

        return TYPE_ITEM;
    }

    public void clear(){
        mAppointments.clear();
        notifyDataSetChanged();
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private Booked getItem(int position) {
        return mAppointments.get(position - 1);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BookedAppointmentVH) {
            Booked booked = getItem(position);
            BookedAppointmentVH bookedAppointmentVH = (BookedAppointmentVH) holder;

            //Clear view
            bookedAppointmentVH.confirmed.setVisibility(View.GONE);
            bookedAppointmentVH.confirmed.setTextColor(
                    ContextCompat.getColor(bookedAppointmentVH.itemView.getContext(), R.color.colorPrimaryDark));

            bookedAppointmentVH.profilePhoto.setImageURI(Uri.parse(booked.getAppointment().doctorPhoto));
            bookedAppointmentVH.doctorName.setText(booked.getAppointment().doctorName);
            bookedAppointmentVH.speciality.setText(booked.getAppointment().doctorSpeciality);
            bookedAppointmentVH.startTime.setText(VUTDDateTimeUtils.changeMilitaryToAmPm (booked.getAppointment().startTime));
            bookedAppointmentVH.address1.setText(booked.getAppointment().address1);
            bookedAppointmentVH.extNum.setText(booked.getAppointment().extNumber);
            bookedAppointmentVH.intNum.setText(booked.getAppointment().intNumber);
            bookedAppointmentVH.province.setText(booked.getAppointment().province);
            bookedAppointmentVH.zipcode.setText(holder.itemView.getContext().getString(R.string.item_booked_appointment_zipcode, booked.getAppointment().zipcode));
            bookedAppointmentVH.city.setText(booked.getAppointment().city + ",");
            bookedAppointmentVH.state.setText(booked.getAppointment().state);
            bookedAppointmentVH.mainPhone.setText(booked.getAppointment().mcPhones);
            bookedAppointmentVH.map.setImageURI(Uri.parse(booked.getMap()));
            switch (booked.getAppointment().appointmentType){
                case 1:
                    if(booked.getAppointment().status.equals("1")) {
                        bookedAppointmentVH.confirmed.setVisibility(View.VISIBLE);
                        bookedAppointmentVH.confirmed.setText(R.string.item_booked_confirm_appointment);
                        bookedAppointmentVH.confirmed.setTextColor(
                                ContextCompat.getColor(bookedAppointmentVH.itemView.getContext(), R.color.green_500));
                    } else if(booked.getAppointment().status.equals("2")) {
                        bookedAppointmentVH.confirmed.setVisibility(View.VISIBLE);
                        bookedAppointmentVH.confirmed.setText(R.string.item_booked_doc_has_request_confirmation);
                        bookedAppointmentVH.confirmed.setTextColor(
                                ContextCompat.getColor(bookedAppointmentVH.itemView.getContext(), R.color.orange_500));
                    }

                    bookedAppointmentVH.appointmentType.setText(bookedAppointmentVH.itemView.getContext().getString(R.string.item_booked_appointment_reservation));
                    bookedAppointmentVH.appointmentType.setTextColor(ContextCompat.getColor(bookedAppointmentVH.itemView.getContext(), R.color.green_500));
                    bookedAppointmentVH.popupMenu = new PopupMenu(bookedAppointmentVH.itemView.getContext(), bookedAppointmentVH.moreOptions, Gravity.END, 0, R.style.AppointmentOptions);
                    bookedAppointmentVH.popupMenu.inflate(R.menu.menu_patient_booked_appointment);
                    bookedAppointmentVH.popupMenu.setOnMenuItemClickListener(bookedAppointmentVH);
                    break;
                case 2:
                    bookedAppointmentVH.moreOptions.setVisibility(View.GONE);//TODO: temporal
                    bookedAppointmentVH.appointmentType.setText(bookedAppointmentVH.itemView.getContext().getString(R.string.item_booked_appointment_subscription));
                    bookedAppointmentVH.appointmentType.setTextColor(ContextCompat.getColor(bookedAppointmentVH.itemView.getContext(), R.color.orange_500));
                    break;
            }

        } else if (holder instanceof DayRecyclerHeaderVH) {
            DayRecyclerHeaderVH dayRecyclerHeaderVH = (DayRecyclerHeaderVH) holder;
            dayRecyclerHeaderVH.mDays.setLayoutManager(new LinearLayoutManager(dayRecyclerHeaderVH.itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
            dayRecyclerHeaderVH.mDays.setAdapter(mHorizontalRecyclerAdapter);
        }

    }

    @Override
    public int getItemCount() {
        return mAppointments.size() + 1;
    }

    public class DayRecyclerHeaderVH extends RecyclerView.ViewHolder {
        RecyclerView mDays;

        public DayRecyclerHeaderVH(View itemView) {
            super(itemView);
            mDays = (RecyclerView) itemView.findViewById(R.id.horizontal_recycler);
        }
    }

    public class BookedAppointmentVH extends RecyclerView.ViewHolder
            implements PopupMenu.OnMenuItemClickListener,
            View.OnClickListener {
        PopupMenu popupMenu;
        View moreOptions;
        SimpleDraweeView profilePhoto;
        TextView doctorName;
        TextView speciality;
        TextView startTime;
        TextView address1;
        TextView extNum;
        TextView intNum;
        TextView province;
        TextView zipcode;
        TextView city;
        TextView state;
        TextView mainPhone;
        TextView appointmentType;
        TextView confirmed;
        SimpleDraweeView map;
        int position;

        public BookedAppointmentVH(View itemView) {
            super(itemView);
            moreOptions = itemView.findViewById(R.id.more_options);
            profilePhoto = (SimpleDraweeView) itemView.findViewById(R.id.profile_photo);
            doctorName = (TextView) itemView.findViewById(R.id.doctor_name);
            speciality = (TextView) itemView.findViewById(R.id.speciality);
            startTime = (TextView) itemView.findViewById(R.id.start_time);
            address1 = (TextView) itemView.findViewById(R.id.address1);
            extNum = (TextView) itemView.findViewById(R.id.ext_num);
            intNum = (TextView) itemView.findViewById(R.id.int_num);
            province = (TextView) itemView.findViewById(R.id.province);
            zipcode = (TextView) itemView.findViewById(R.id.zipcode);
            city = (TextView) itemView.findViewById(R.id.city);
            state = (TextView) itemView.findViewById(R.id.state);
            mainPhone = (TextView) itemView.findViewById(R.id.main_phone);
            map = (SimpleDraweeView) itemView.findViewById(R.id.map);
            appointmentType = (TextView) itemView.findViewById(R.id.appointment_type);
            confirmed = (TextView) itemView.findViewById(R.id.appointment_confirmed);
            moreOptions.setOnClickListener(this);
            map.setOnClickListener(this);
            mainPhone.setOnClickListener(this);
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            position = getAdapterPosition();
            if(mOnBookedAppointmentSelected != null) {
                mOnBookedAppointmentSelected.onMenuItemSelected(item, getItem(position));
                return true;
            }
            return false;
        }

        @Override
        public void onClick(View view) {
            if(view.getId() == R.id.map){
                Booked booked = getItem(getAdapterPosition());
                GoogleStaticMap.showMap(
                        booked.getAppointment().lat,
                        booked.getAppointment().lon,
                        booked.getAppointment().mcName,
                        itemView.getContext());


                //Fabric Answer
                VutdAnswers.logEvent(VutdAnswers.EVENT.VIEW_INTERACTION,
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.MAP, booked.getAppointment().lat + ", " + booked.getAppointment().lon ));


            } else if(view.getId() == R.id.more_options){
                popupMenu.show();
            } else if (view.getId() == R.id.main_phone){

                //Fabric Answer
                VutdAnswers.logEvent(VutdAnswers.EVENT.VIEW_INTERACTION,
                        new VutdAnswers.ATTRIBUTE(VutdAnswers.ATTRIBUTE.PHONE, "click"));

                if(mOnBookedAppointmentSelected!= null) mOnBookedAppointmentSelected.onPhoneSelected(getItem(getAdapterPosition()));
            }
        }
    }

    public static class Booked {
        BookedAppointment mAppointment;

        public String getMap() {
            GoogleStaticMap.GoogleMarker marker = new GoogleStaticMap.GoogleMarker();
            marker.setLat(mAppointment.lat);
            marker.setLon(mAppointment.lon);
            return new GoogleStaticMap.Builder().addMarker(marker).build().getUrl();
        }

        public BookedAppointment getAppointment() {
            return mAppointment;
        }

        public void setAppointment(BookedAppointment mAppointment) {
            this.mAppointment = mAppointment;
        }
    }

    public interface OnBookedAppointmentSelected {
        void onMenuItemSelected(MenuItem menuItem, Booked booked);
        void onPhoneSelected(Booked booked);
    }
}
