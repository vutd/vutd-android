package io.vutd.agenda.realm;

/**
 * Created by andresmariscal on 05/07/16.
 */

/**
 *
 * @param <T> Any class that extends from <p>BaseResponse</p>
 */
public interface IResponseTransform<T> {
    //T transform(T responseObject); TODO: Change use this firm
    void transform(T responseObject);
}
