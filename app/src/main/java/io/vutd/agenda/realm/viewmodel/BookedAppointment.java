package io.vutd.agenda.realm.viewmodel;

import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.Index;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.common.AppointmentBookedCommon;

/**
 * Created by andresmariscal on 09/10/16.
 */

public class BookedAppointment extends RealmObject implements IResponseTransform<AppointmentBookedCommon>  {
    /**
     * Represent the state of the appointment
     * BOOKED      = 1;
     * BUSY        = 2;
     * REST_TIME   = 3;
     * FREE        = 4;
     * DELETED     = 5;
     */
    @Ignore
    public static final int BOOKED = 1;
    @Ignore
    public static final int BUSY = 2;
    @Ignore
    public static final int REST_TIME = 3;
    @Ignore
    public static final int FREE = 4;
    @Ignore
    public static final int DELETED = 5;

    @Index
    public String date;
    public long id;
    public long bookTypeId;
    public int appointmentType;
    public String mcName;
    public String mcDescription;
    public String lat;
    public String lon;
    public String address1;
    public String address2;
    public String zipcode;
    public String province;
    public String city;
    public String state;
    public String country;
    public String intNumber;
    public String extNumber;
    public String doctorName;
    public String doctorPhoto;
    public String doctorSpeciality;
    public String startTime;
    public String status;
    public String mcPhones;

    @Override
    public void transform(AppointmentBookedCommon responseObject) {
        if(responseObject.getId() != 0){
            this.id = responseObject.getId();
        } else this.id = 0;

        if(responseObject.getAppointmentType() != 0) {
            this.appointmentType = responseObject.getAppointmentType();
        } else this.appointmentType = -1;

        if(responseObject.getName() != null){
            this.mcName = responseObject.getName();
        } else this.mcName = "";

        if(responseObject.getDescription() != null){
            this.mcDescription = responseObject.getDescription();
        } else this.mcDescription = "";

        if(responseObject.getLocationLat() != null){
            this.lat = responseObject.getLocationLat();
        } else this.lat = "";

        if(responseObject.getLocationLon() != null){
            this.lon = responseObject.getLocationLon();
        } else this.lon = "";

        if(responseObject.getAddress1() != null){
            this.address1 = responseObject.getAddress1();
        } else this.address1 = "";

        if(responseObject.getAddress2() != null){
            this.address2 = responseObject.getAddress2();
        } else this.address2 = "";

        if(responseObject.getZipcode() != null){
            this.zipcode = responseObject.getZipcode();
        } else this.zipcode = "";

        if(responseObject.getProvince() != null){
            this.province = responseObject.getProvince();
        } else this.province = "";

        if(responseObject.getCity() != null){
            this.city = responseObject.getCity();
        } else this.city = "";

        if(responseObject.getState() != null){
            this.state = responseObject.getState();
        } else this.state = "";

        if(responseObject.getCountry() != null){
            this.country = responseObject.getCountry();
        } else this.country = "";

        if(responseObject.getIntNumber() != null){
            this.intNumber = responseObject.getIntNumber();
        } else this.intNumber = "";

        if(responseObject.getExtNumber() != null){
            this.extNumber = responseObject.getExtNumber();
        } else this.extNumber = "";

        if(responseObject.getDoctorName() != null){
            this.doctorName = responseObject.getDoctorName();
        } else this.doctorName = "";

        if(responseObject.getDoctorPhoto() != null){
            this.doctorPhoto = responseObject.getDoctorPhoto();
        } else this.doctorPhoto = "";

        if(responseObject.getSpecialities() != null && !responseObject.getSpecialities().isEmpty()){
            this.doctorSpeciality = responseObject.getSpecialities().get(0).getName();
        } else this.doctorSpeciality = "";

        if(responseObject.getAppointmentDate() != null){
            this.date = responseObject.getAppointmentDate();
        } else this.date = "";

        if(responseObject.getStartTime() != null){
            this.startTime = responseObject.getStartTime();
        } else this.startTime = "";

        if(responseObject.getBookTypeId() != 0){
            this.bookTypeId = responseObject.getBookTypeId();
        } else this.bookTypeId = 0;

        if(responseObject.getStatus() != null){
            this.status = responseObject.getStatus();
        } else this.status = "";

        if(!responseObject.getMcPhones().isEmpty()){
            List<String> phones = responseObject.getMcPhones();
            String tmpPhones = phones.get(0);
            phones.remove(tmpPhones);
            for (String phone : phones){
                tmpPhones += ", " + phone;
            }
            this.mcPhones = tmpPhones;
        } else this.mcPhones = "";
    }
}
