package io.vutd.agenda.realm.viewmodel;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.vutd.agenda.realm.model.Doctor;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 03/09/16.
 */
public class SearchDoctorHistory extends RealmObject {
    public Doctor doctor;
    public long medicalCoreId;
    public String mcAddress;
    public int frequency;
    public int isSearchRecord;

    public static void debugTable(RealmResults<SearchDoctorHistory> results){
        Logger.d("SearchDoctorHistory table", "Total record " + results.size());
        for(SearchDoctorHistory history : results) {
            Logger.d("RECORD", history.toString());
        }
    }
}
