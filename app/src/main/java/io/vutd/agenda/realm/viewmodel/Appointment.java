package io.vutd.agenda.realm.viewmodel;

import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.annotations.Ignore;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.common.AppointmentCommon;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 09/10/16.
 */

public class Appointment extends RealmObject implements IResponseTransform<AppointmentCommon> {

    /**
     * Represent the state of the appointment
     * BOOKED      = 1;
     * BUSY        = 2;
     * REST_TIME   = 3;
     * FREE        = 4;
     * DELETED     = 5;
     */
    @Ignore
    public static final int BOOKED = 1;
    @Ignore
    public static final int BUSY = 2;
    @Ignore
    public static final int REST_TIME = 3;
    @Ignore
    public static final int FREE = 4;
    @Ignore
    public static final int DELETED = 5;

    /**
     * Id from the server
     */
    public long id;
    /**
     * The user who created the appointment
     */
    public long userId;
    /**
     * Id from server
     * Represent the state of the appointment
     * BOOKED      = 1;
     * BUSY        = 2;
     * REST_TIME   = 3;
     * FREE        = 4;
     * DELETED     = 5;
     */
    public long bookTypeId;
    /**
     * General Information
     */
    public long doctorId;
    public long medicalCoreId;
    public String date;
    public String startTime;
    public String endTime;
    public String status;
    public String patientName;
    public String patientPhone;
    public int isConfirmationSend;
    public int isConfirmationDelivered;
    public int subscribers;
    /**
     * This flag enable the selection mode in the adapter
     * 1 = true
     * 0 = false
     */
    public int selectionMode;
    public int isSelected;
    /**
     * This flag indicate if the record has to be updated or not
     * 1 = true
     * 0 = false
     */
    public int isDirty;

    public long getId() {
        return id;
    }

    public long getUserId() {
        return userId;
    }

    public long getBookTypeId() {
        return bookTypeId;
    }

    public long getDoctorId() {
        return doctorId;
    }

    public long getMedicalCoreId() {
        return medicalCoreId;
    }

    public String getDate() {
        return date;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStatus() {
        return status;
    }

    public String getPatientName() {
        return patientName;
    }

    public String getPatientPhone() {
        return patientPhone;
    }

    public int getIsDirty() {
        return isDirty;
    }

    @Override
    public void transform(AppointmentCommon responseObject) {
        if(responseObject.getStartTime() != null)
            this.startTime = responseObject.getStartTime();
        else this.startTime = "";

        if(responseObject.getId() != 0)
            this.id = responseObject.getId();
        else this.id = -1;

        if(responseObject.getEndTime() != null)
            this.endTime = responseObject.getEndTime();
        else this.endTime = "";

        if(responseObject.getAppointmentCreatorId() != 0)
            this.userId = responseObject.getAppointmentCreatorId();
        else this.userId = -1;

        if(responseObject.getBookTypeId() != 0)
            this.bookTypeId = responseObject.getBookTypeId();
        else this.bookTypeId = -1;

        if(responseObject.getDate() != null)
            this.date = responseObject.getDate();
        else this.date = "";

        if(responseObject.getPatientName() != null)
            this.patientName = responseObject.getPatientName();
        else this.patientName = "";

        if(responseObject.getPatientPhone() != null)
            this.patientPhone = responseObject.getPatientPhone();
        else this.patientPhone = "";

        if(responseObject.getStatus() != null)
            this.status = responseObject.getStatus();
        else this.status = "";

        isConfirmationDelivered = responseObject.getConfirmationRequestDelivered();
        isConfirmationSend = responseObject.getConfirmationRequestSend();
        subscribers = responseObject.getSubscribers();

        isDirty = 0;
        isSelected = 0;
    }

    public void clear(){
        this.id = -1;
        this.bookTypeId = 4;
        this.userId = -1;
        this.patientName = "";
        this.patientPhone = "";
        this.status = "";
        this.date = "";
        this.isDirty = 0;
        this.isSelected = 0;
    }

    public static void debugTable(RealmResults<Appointment> results){
        Logger.d("Appointment table", "Total record " + results.size());
        for(Appointment appointment : results) {
            Logger.d("RECORD", appointment.toString());
        }
    }
}
