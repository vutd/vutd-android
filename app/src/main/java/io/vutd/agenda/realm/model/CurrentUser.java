package io.vutd.agenda.realm.model;

import io.realm.RealmObject;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.response.UserLoginResponse;

/**
 * Created by andresmariscal on 05/07/16.
 */
public class CurrentUser extends RealmObject implements IResponseTransform<UserLoginResponse> {
    /**
     * The current session id.
     */
    public String sessionId;
    /**
     * General information about the user
     */
    public String name;
    public String lastName;
    public String phone;
    public String email;
    public String gender;
    public String facebookId;
    public int resetPassword;
    public int roleId;
    /**
     * If the current user is a Doctor then the value will filled with the doctor id
     * If the current user is a Mc User then the value will filled with the mc user id
     * Otherwise if the current user is a patient the value is -1
     */
    public long reference;

    @Override
    public void transform(UserLoginResponse responseObject) {
        if(responseObject.getSessionId()!= null)
            this.sessionId = responseObject.getSessionId();
        else this.sessionId = "";

        if(responseObject.getEmail() != null)
            this.email = responseObject.getEmail();
        else this.email = "";

        if(responseObject.getFacebookToken() != null)
            this.facebookId = responseObject.getFacebookToken();
        else this.facebookId = "";

        if(responseObject.getGender() != null)
            this.gender = responseObject.getGender();
        else this.gender = "";

        if(responseObject.getLastName() != null)
            this.lastName = responseObject.getLastName();
        else this.lastName = "";

        if(responseObject.getName() != null)
            this.name = responseObject.getName();
        else this.name = "";

        if(responseObject.getPhone() != null)
            this.phone = responseObject.getPhone();
        else this.phone = "";

        if(responseObject.getReference() != 0)
            this.reference = responseObject.getReference();
        else this.reference = -1;

        if(responseObject.getResetPassword() != -1)
            this.resetPassword = responseObject.getResetPassword();
        else this.resetPassword = -1;

        if(responseObject.getRoleId() != 0)
            this.roleId = responseObject.getRoleId();
        else this.roleId = -1;
    }
}
