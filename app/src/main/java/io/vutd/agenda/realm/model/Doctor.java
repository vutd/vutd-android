package io.vutd.agenda.realm.model;

import io.realm.RealmObject;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.common.DoctorCommon;

/**
 * Created by andresmariscal on 09/10/16.
 */

public class Doctor extends RealmObject implements IResponseTransform<DoctorCommon> {
    public long id;
    public String name;
    public String phone;
    public String profilePhoto;
    public String speciality;
    public String gender;

    @Override
    public void transform(DoctorCommon responseObject) {
        if(responseObject.getDoctorId() != 0) {
            this.id = responseObject.getDoctorId();
        } else this.id = -1;
        if(responseObject.getName() != null){
            this.name = responseObject.getName();
        } else this.name = "";

        if(responseObject.getPhone() != null) {
            this.phone = responseObject.getPhone();
        } else this.phone = "";

        if(responseObject.getProfilePhoto() != null) {
            this.profilePhoto = responseObject.getProfilePhoto();
        } else this.profilePhoto = "";

        if(responseObject.getSpecialities() != null && !responseObject.getSpecialities().isEmpty()){
            this.speciality = responseObject.getSpecialities().get(0).getName();
        } else this.speciality = "";
    }
}
