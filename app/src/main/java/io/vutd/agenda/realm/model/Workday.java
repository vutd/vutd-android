package io.vutd.agenda.realm.model;

import io.realm.RealmObject;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.common.WorkdayCommon;

/**
 * Created by andresmariscal on 09/10/16.
 */

public class Workday extends RealmObject implements IResponseTransform<WorkdayCommon> {
    public long doctorId;
    public long medicalCoreId;
    public int day;
    public String startTime;
    public String endTime;
    public String restStartTime;
    public String restEndTime;
    public String appointmentDuration;

    @Override
    public void transform(WorkdayCommon responseObject) {
        if(responseObject.getAppointmentDuration() != null)
            this.appointmentDuration = responseObject.getAppointmentDuration();
        else this.appointmentDuration = "";

        if(responseObject.getDay() != 0)
            this.day = responseObject.getDay();
        else this.day = -1;

        if(responseObject.getEndTime() != null)
            this.endTime = responseObject.getEndTime();
        else this.endTime = "";

        if(responseObject.getRestEndTime() != null)
            this.restEndTime = responseObject.getRestEndTime();
        else this.restEndTime = "";

        if(responseObject.getRestStartTime() != null)
            this.restStartTime = responseObject.getRestStartTime();
        else this.restStartTime = "";

        if(responseObject.getStartTime() != null)
            this.startTime = responseObject.getStartTime();
        else this.startTime = "";
    }
}
