package io.vutd.agenda.realm.model;

import io.realm.RealmObject;
import io.vutd.agenda.realm.IResponseTransform;
import io.vutd.agenda.rest.common.MedicalCoreCommon;

/**
 * Created by andresmariscal on 09/10/16.
 */

public class MedicalCore extends RealmObject implements IResponseTransform<MedicalCoreCommon> {
    public long id;
    public String name;
    public String description;
    public String locationLat;
    public String locationLon;
    public String address1;
    public String address2;
    public String zipcode;
    public String province;
    public String city;
    public String state;
    public String country;
    public String intNumber;
    public String extNumber;

    @Override
    public void transform(MedicalCoreCommon responseObject) {
        if(responseObject.getId() != 0)
            this.id = responseObject.getId();
        else this.id = -1;

        if(responseObject.getAddress1() != null)
            this.address1 = responseObject.getAddress1();
        else this.address1 = "";

        if(responseObject.getAddress2() != null)
            this.address2 = responseObject.getAddress2();
        else this.address2 = "";

        if(responseObject.getCity() != null)
            this.city = responseObject.getCity();
        else this.city = "";

        if(responseObject.getCountry() != null)
            this.country = responseObject.getCountry();
        else this.country = "";

        if(responseObject.getDescription() != null)
            this.description = responseObject.getDescription();
        else this.description = "";

        if(responseObject.getExtNumber() != null)
            this.extNumber = responseObject.getExtNumber();
        else this.extNumber = "";

        if(responseObject.getIntNumber() != null)
            this.intNumber = responseObject.getIntNumber();
        else this.intNumber = "";

        if(responseObject.getLocationLat() != null)
            this.locationLat = responseObject.getLocationLat();
        else this.locationLat = "";

        if(responseObject.getLocationLon() != null)
            this.locationLon = responseObject.getLocationLon();
        else this.locationLon = "";

        if(responseObject.getName() != null)
            this.name = responseObject.getName();
        else this.name = "";

        if(responseObject.getProvince() != null)
            this.province = responseObject.getProvince();
        else this.province = "";

        if(responseObject.getState() != null)
            this.state = responseObject.getState();
        else this.state = "";

        if(responseObject.getZipcode() != null)
            this.zipcode = responseObject.getZipcode();
        else this.zipcode = "";
    }
}
