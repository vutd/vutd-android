package io.vutd.agenda.app;

import android.support.annotation.StringDef;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.SignUpEvent;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by andresmariscal on 18/08/16.
 *
 * Look in the all project for the key word <p>Fabric Answers</p>
 */
public class VutdAnswers {

    public static void logEvent(@EVENT.Event String event, ATTRIBUTE... attributes){
        if(!AppSettings.BUILD_CONFIG_DEBUG) {
            Answers answers = Answers.getInstance();
            CustomEvent customEvent = new CustomEvent(event);
            for (ATTRIBUTE attribute : attributes) {
                customEvent.putCustomAttribute(attribute.getAttribute(), attribute.getValue());
            }
            answers.logCustom(customEvent);
        }
    }

    public static void logSignUpEvent(@SESSION.Method String method, boolean success, ATTRIBUTE... attributes){
        if(!AppSettings.BUILD_CONFIG_DEBUG) {
            Answers answers = Answers.getInstance();
            SignUpEvent signUpEvent = new SignUpEvent();
            signUpEvent.putMethod(method);
            signUpEvent.putSuccess(success);
            for (ATTRIBUTE attribute : attributes) {
                signUpEvent.putCustomAttribute(attribute.getAttribute(), attribute.getValue());
            }
            answers.logSignUp(signUpEvent);
        }
    }

    public static void logLoginEvent(@SESSION.Method String method, boolean success, ATTRIBUTE... attributes){
        if(!AppSettings.BUILD_CONFIG_DEBUG) {
            Answers answers = Answers.getInstance();
            LoginEvent loginEvent = new LoginEvent();
            loginEvent.putMethod(method);
            loginEvent.putSuccess(success);
            for (ATTRIBUTE attribute : attributes) {
                loginEvent.putCustomAttribute(attribute.getAttribute(), attribute.getValue());
            }
            answers.logLogin(loginEvent);
        }
    }

    //TODO: Finish this in a future, this will be use for Terms & conditions, Tutorials, profiles views etc…
    /*public static void logContentViewEvent(@EVENT.Event String eventName, ATTRIBUTE... attributes){
        if(!AppSettings.BUILD_CONFIG_DEBUG) {
            Answers answers = Answers.getInstance();
            ContentViewEvent contentViewEvent = new ContentViewEvent();
            contentViewEvent.putContentName(eventName);
            contentViewEvent.putContentType();
            contentViewEvent.putContentId();

            for (ATTRIBUTE attribute : attributes) {
                contentViewEvent.putCustomAttribute(attribute.getAttribute(), attribute.getValue());
            }
            answers.logContentView(contentViewEvent);
        }
    }*/


    public static class SESSION {
        @StringDef({
                EMAIL,
                FACEBOOK
        })
        @Retention(RetentionPolicy.SOURCE)
        public @interface Method{}

        public static final String EMAIL    = "email_password";
        public static final String FACEBOOK = "facebook";
    }

    public static class EVENT {
        @StringDef({

                //View Interaction
                VIEW_INTERACTION,

                //Agenda Events
                APPOINTMENT_CREATION,
                APPOINTMENT_CANCELLED,
                APPOINTMENT_DELETED,
                APPOINTMENT_SELECTED,
                MORE_OPTIONS,
                RELEASE_TIME,
                REQUEST_CONFIRMATION,
                MULTI_SELECTION_MODE,
                SUBSCRIBE_APPOINTMENT,

                //Session Events
                LOGOUT,
                LOGIN,
                REGISTER,
                DIGITS,

                //Search Doctor By Phone
                SEARCH_BY_PHONE,
                DOCTOR_AGENDA_SELECTED,
                DOCTOR_DELETED_FROM_HISTORY,

                //Appointments booked
                CALL_DOCTOR_MC,

                //Confirm Appointment
                CONFIRM_APPOINTMENT,
        })
        @Retention(RetentionPolicy.SOURCE)
        public @interface Event {
        }

        //View Interaction
        public static final String VIEW_INTERACTION         = "View Interaction";

        //Agenda Events
        public static final String APPOINTMENT_CREATION     = "Appointment Creation";
        public static final String APPOINTMENT_CANCELLED    = "Appointment Cancelled";
        public static final String APPOINTMENT_DELETED      = "Appointment Deleted";
        public static final String APPOINTMENT_SELECTED     = "Appointment Selected";
        public static final String MORE_OPTIONS             = "More Option Pressed";
        public static final String RELEASE_TIME             = "Release Time";
        public static final String REQUEST_CONFIRMATION     = "Request Confirmation";
        public static final String MULTI_SELECTION_MODE     = "Multi Selection";
        public static final String SUBSCRIBE_APPOINTMENT    = "Subscribe Appointment";

        //Session Events
        public static final String LOGOUT                   = "Logout";
        public static final String LOGIN                    = "Login";
        public static final String REGISTER                 = "Register";// I used SignUp class from Answers
        public static final String DIGITS                   = "Digits";

        //Search Doctor By Phone
        public static final String SEARCH_BY_PHONE                  = "Search By Phone";
        public static final String DOCTOR_AGENDA_SELECTED           = "Doctor Agenda Selected";
        public static final String DOCTOR_DELETED_FROM_HISTORY      = "Doctor Deleted From History";

        //Appointments booked
        public static final String CALL_DOCTOR_MC          = "Call Doctor MC";

        //Confirm Appointment
        public static final String CONFIRM_APPOINTMENT = "Confirm Appointment";
    }

    public static class ATTRIBUTE {
        @StringDef({
                APPOINTMENT_TYPE,
                CALLBACK,
                DOCTOR_ID,
                DOCTOR_NAME,
                MENU_OPTION,
                USER_ROLE,
                TIME,
                PERMISSION_DENIED,
                FACEBOOK_SIGN_UP_FAIL,
                FROM,
                PHONE,
                SEARCH_RESULT,
                USER,
                MEDICAL_CORE_ID,
                FREQUENCY,
                MAP,
                PREVIEW,
                CONFIRMATION,
        })
        @Retention(RetentionPolicy.SOURCE)
        public @interface Attribute {
        }

        public static final String APPOINTMENT_TYPE      = "appointment_type";
        public static final String CALLBACK              = "callback";
        public static final String DOCTOR_ID             = "doctor_id";
        public static final String DOCTOR_NAME           = "doctor_name";
        public static final String MENU_OPTION           = "menu_option";
        public static final String USER_ROLE             = "user_role";
        public static final String TIME                  = "time";
        public static final String PERMISSION_DENIED     = "permission_denied";
        public static final String FACEBOOK_SIGN_UP_FAIL = "facebook_sign_up_fail";
        public static final String FROM                  = "from";
        public static final String PHONE                 = "phone";
        public static final String SEARCH_RESULT         = "search_result";
        public static final String USER                  = "user";
        public static final String MEDICAL_CORE_ID       = "medical_core_id";
        public static final String FREQUENCY             = "frequency";
        public static final String MAP                   = "map";
        public static final String PREVIEW               = "preview";
        public static final String CONFIRMATION          = "confirmation";

        private @ATTRIBUTE.Attribute String mAttribute;
        private String mValue;

        public ATTRIBUTE(@Attribute String attribute, String value) {
            this.mAttribute = attribute;
            this.mValue = value;
        }

        public @Attribute String getAttribute() {
            return mAttribute;
        }

        public String getValue() {
            return mValue;
        }
    }
}
