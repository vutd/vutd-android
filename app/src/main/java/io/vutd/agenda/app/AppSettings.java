package io.vutd.agenda.app;

import io.vutd.agenda.BuildConfig;

/**
 * Created by andresmariscal on 25/06/16.
 */
public class AppSettings {
    public static boolean BUILD_CONFIG_DEBUG = BuildConfig.DEBUG;
}
