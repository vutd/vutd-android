package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class AddDoctorToMCRequest extends BaseRequest {
    @SerializedName("doctorPhone")
    @Expose
    private String doctorPhone;

    public String getDoctorPhone() {
        return doctorPhone;
    }

    public void setDoctorPhone(String doctorPhone) {
        this.doctorPhone = doctorPhone;
    }
}
