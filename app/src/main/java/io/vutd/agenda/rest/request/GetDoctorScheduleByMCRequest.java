package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class GetDoctorScheduleByMCRequest extends BaseRequest {
    @SerializedName("medicalCoreId")
    @Expose
    private long medicalCoreId;
    @SerializedName("doctorId")
    @Expose
    private long doctorId;
    @SerializedName("date")
    @Expose
    private String date;

    public long getMedicalCoreId() {
        return medicalCoreId;
    }

    public void setMedicalCoreId(long medicalCoreId) {
        this.medicalCoreId = medicalCoreId;
    }

    public long getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(long doctorId) {
        this.doctorId = doctorId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
