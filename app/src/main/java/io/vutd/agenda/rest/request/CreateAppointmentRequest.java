package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 28/06/16.
 */
@EBean
public class CreateAppointmentRequest extends BaseRequest {
    @SerializedName("doctorId")
    @Expose
    private long doctorId;
    @SerializedName("medicalCoreId")
    @Expose
    private long medicalCoreId;
    @SerializedName("bookDate")
    @Expose
    private String bookDate;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("bookType")
    @Expose
    private long bookType;
    @SerializedName("patientName")
    @Expose
    private String patientName;
    @SerializedName("patientPhone")
    @Expose
    private String patientPhone;

    /**
     *
     * @return
     * The doctorId
     */
    public long getDoctorId() {
        return doctorId;
    }

    /**
     *
     * @param doctorId
     * The doctorId
     */
    public void setDoctorId(long doctorId) {
        this.doctorId = doctorId;
    }

    /**
     *
     * @return
     * The medicalCoreId
     */
    public long getMedicalCoreId() {
        return medicalCoreId;
    }

    /**
     *
     * @param medicalCoreId
     * The medicalCoreId
     */
    public void setMedicalCoreId(long medicalCoreId) {
        this.medicalCoreId = medicalCoreId;
    }

    /**
     *
     * @return
     * The bookDate
     */
    public String getBookDate() {
        return bookDate;
    }

    /**
     *
     * @param bookDate
     * The bookDate
     */
    public void setBookDate(String bookDate) {
        this.bookDate = bookDate;
    }

    /**
     *
     * @return
     * The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     * The bookType
     */
    public long getBookType() {
        return bookType;
    }

    /**
     *
     * @param bookType
     * The bookType
     */
    public void setBookType(long bookType) {
        this.bookType = bookType;
    }

    /**
     *
     * @return
     * The patientName
     */
    public String getPatientName() {
        return patientName;
    }

    /**
     *
     * @param patientName
     * The patientName
     */
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    /**
     *
     * @return
     * The patientPhone
     */
    public String getPatientPhone() {
        return patientPhone;
    }

    /**
     *
     * @param patientPhone
     * The patientPhone
     */
    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }
}
