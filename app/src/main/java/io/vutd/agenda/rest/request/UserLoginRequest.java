package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.vutd.agenda.utils.Utils;

/**
 * Created by andresmariscal on 27/06/16.
 */
public class UserLoginRequest {
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("facebookId")
    @Expose
    private String facebookId;
    @SerializedName("deviceOs")
    @Expose
    private String deviceOs;
    @SerializedName("deviceName")
    @Expose
    private String deviceName;
    @SerializedName("deviceModel")
    @Expose
    private String deviceModel;
    @SerializedName("pushNotificationToken")
    @Expose
    private String pushNotificationToken;
    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        if(!password.trim().isEmpty())
            this.password = Utils.md5(password);
    }

    /**
     *
     *
     * @return
     * The facebookId
     */
    public String getFacebookId() {
        return facebookId;
    }

    /**
     *
     *
     * @param facebookId
     * The facebookId
     */
    public void setFacebookId(String facebookId) {
        this.facebookId = facebookId;
    }

    /**
     *
     * @return
     * The deviceOs
     */
    public String getDeviceOs() {
        return deviceOs;
    }

    /**
     *
     * @param deviceOs
     * The deviceOs
     */
    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    /**
     *
     * @return
     * The deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     *
     * @param deviceName
     * The deviceName
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     *
     * @return
     * The deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     *
     * @param deviceModel
     * The deviceModel
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getPushNotificationToken() {
        return pushNotificationToken;
    }

    public void setPushNotificationToken(String pushNotificationToken) {
        this.pushNotificationToken = pushNotificationToken;
    }
}
