package io.vutd.agenda.rest.handler;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 26/06/16.
 */
/**
 *
 * @param <T> Object that represent the response from the web service
 * @param <R> Object that represent the rules for this service call
 */
public abstract class AbstractHandler<T, R> implements IValidator <R>{

    protected ServiceHandler mService;
    protected IHandlerResult mHandlerResult;

    public AbstractHandler(IHandlerResult handlerResult) {
        this.mHandlerResult = handlerResult;
        mService = new ServiceHandler();
        mService.setTAG(getTag());
        mService.setCallback(mCallbackService);
    }

    public AbstractHandler() {
        mService = new ServiceHandler();
        mService.setTAG(getTag());
        mService.setCallback(mCallbackService);
    }

    protected abstract String getTag();
    protected abstract void onHandlerError(T response);

    public void setHandlerResult(IHandlerResult mHandlerResult) {
        this.mHandlerResult = mHandlerResult;
    }

    protected ICallbackService <T> mCallbackService = new ICallbackService<T>() {
        @Override
        public void onSuccess(T response) {
            Logger.d(getTag(), "onSuccess()");
            mHandlerResult.onOkResult(response);
        }

        @Override
        public void onError(T response) {
            Logger.d(getTag(), "onError()");
            onHandlerError(response);
        }

        @Override
        public void onFailure(String message) {
            Logger.d(getTag(), "onFailure() Message: " + message );
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.RETROFIT_FAILURE));
            mHandlerResult.onBadResult(errors);
        }
    };

    protected String getErrors(List<String> errors){
        StringBuilder errorBuilder = new StringBuilder();
        for(String error : errors){
            errorBuilder.append(error);
            errorBuilder.append(", ");
        }
        return errorBuilder.toString();
    }

    @Override
    public Set validateInputs(Map inputs) {
        Map<R, String> in = inputs;
        Set<R> errors = new HashSet<>();
        for(Map.Entry<R, String> validator: in.entrySet()){
            if(validator.getValue() == null || validator.getValue().trim().isEmpty()){
                errors.add(validator.getKey());
            }
        }
        return errors;
    }
}
