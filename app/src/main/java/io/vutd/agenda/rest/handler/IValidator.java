package io.vutd.agenda.rest.handler;

import java.util.Map;
import java.util.Set;

/**
 * Created by andresmariscal on 25/06/16.
 */
public interface IValidator <RULE>{
    Set validateInputs(Map<RULE, String> inputs);
}
