package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class DeleteAppointmentRequest extends BaseRequest {
    @SerializedName("appointmentId")
    @Expose
    private long appointmentId;
    @SerializedName("reasonId")
    @Expose
    private long reasonId;

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getReasonId() {
        return reasonId;
    }

    public void setReasonId(long reasonId) {
        this.reasonId = reasonId;
    }
}
