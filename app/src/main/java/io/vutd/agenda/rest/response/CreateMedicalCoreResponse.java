package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 26/06/16.
 */
public class CreateMedicalCoreResponse extends BaseResponse {

    @SerializedName("medicalCoreUserProfile")
    @Expose
    private String medicalCoreUserProfile;
    @SerializedName("temporalPassword")
    @Expose
    private int temporalPassword;

    public String getMedicalCoreUserProfile() {
        return medicalCoreUserProfile;
    }

    public void setMedicalCoreUserProfile(String medicalCoreUserProfile) {
        this.medicalCoreUserProfile = medicalCoreUserProfile;
    }

    public int getTemporalPassword() {
        return temporalPassword;
    }

    public void setTemporalPassword(int temporalPassword) {
        this.temporalPassword = temporalPassword;
    }
}
