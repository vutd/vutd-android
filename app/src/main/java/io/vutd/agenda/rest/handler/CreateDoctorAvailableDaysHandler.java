package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.CreateDoctorAvailableDaysRequest;
import io.vutd.agenda.rest.common.WorkdayCommon;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 26/06/16.
 */
public class CreateDoctorAvailableDaysHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "AvailableDaysHandler";

    //Error codes
    private static final int SERVER_EXCEPTION = 400;
    private static final int MISSED_FIELDS    = 401;
    private static final int DOCTOR_NOT_IN_MC = 402;

    public CreateDoctorAvailableDaysHandler(IHandlerResult handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet<>();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_NOT_IN_MC:
                Logger.d(TAG, "DOCTOR_NOT_IN_MC");
                errors.add(new Rule(Rule.DOCTOR_NOT_IN_MC));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    public Set validateInputs(String medicalCoreId, List<WorkdayCommon> days) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.MEDICAL_CORE_ID), medicalCoreId);
        fieldsRequired.put(new Rule(Rule.DAYS), days.isEmpty() ? null : "ok");
        for(WorkdayCommon day : days){
            if(day.getAppointmentDuration().trim().isEmpty()) {
                fieldsRequired.put(new Rule(Rule.APPOINTMENT_DURATION), null);
                break;
            }
            if(day.getDay() == 0){
                fieldsRequired.put(new Rule(Rule.DAY), null);
                break;
            }
            if(day.getEndTime().trim().isEmpty()){
                fieldsRequired.put(new Rule(Rule.END_TIME), null);
                break;
            }
            if(day.getRestEndTime().trim().isEmpty()){
                fieldsRequired.put(new Rule(Rule.REST_END_TIME), null);
                break;
            }
            if(day.getRestStartTime().trim().isEmpty()){
                fieldsRequired.put(new Rule(Rule.REST_START_TIME), null);
                break;
            }
            if(day.getStartTime().trim().isEmpty()){
                fieldsRequired.put(new Rule(Rule.START_TIME), null);
                break;
            }
        }

        return super.validateInputs(fieldsRequired);
    }

    public void createDoctorAvailableDays(CreateDoctorAvailableDaysRequest request){
        try{
            Set errors = validateInputs(String.valueOf(request.getMedicalCoreId()), request.getDays());
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }

            mService.debugRquest(request);
            mService.execute(mService.getVutdService().createDoctorAvailableDays(request));
        }catch (Exception e){
            Logger.e(TAG, "Exception: " + e.getMessage(), e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
