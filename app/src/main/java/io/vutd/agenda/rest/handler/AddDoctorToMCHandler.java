package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.AddDoctorToMCRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class AddDoctorToMCHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "AddDoctorToMCHandler";

    //Error Codes
    private final int MISSED_FIELDS             = 1101;
    private final int INVALID_PHONE             = 1102;
    private final int INVALID_USER              = 1103;
    private final int NOT_A_DOCTOR              = 1104;
    private final int DOCTOR_WITHOUT_PROFILE    = 1105;
    private final int DOCTOR_ALREADY_INCLUDED   = 1106;
    private final int INVITATION_ALREADY_SEND   = 1107;


    public AddDoctorToMCHandler(IHandlerResult handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_PHONE:
                Logger.d(TAG, "INVALID_PHONE");
                errors.add(new Rule(Rule.INVALID_PHONE));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_DOCTOR");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            case NOT_A_DOCTOR:
                Logger.d(TAG, "NOT_A_DOCTOR");
                errors.add(new Rule(Rule.NOT_A_DOCTOR));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_WITHOUT_PROFILE:
                Logger.d(TAG, "DOCTOR_WITHOUT_PROFILE");
                errors.add(new Rule(Rule.DOCTOR_WITHOUT_PROFILE));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_ALREADY_INCLUDED:
                Logger.d(TAG, "DOCTOR_ALREADY_INCLUDED");
                errors.add(new Rule(Rule.DOCTOR_ALREADY_INCLUDED));
                mHandlerResult.onBadResult(errors);
                break;
            case INVITATION_ALREADY_SEND:
                Logger.d(TAG, "INVITATION_ALREADY_SEND");
                errors.add(new Rule(Rule.INVITATION_ALREADY_SEND));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String doctorPhone){
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.DOCTOR_PHONE), doctorPhone);
        return super.validateInputs(fieldsRequired);
    }

    public void addDoctor(AddDoctorToMCRequest request){
        Set errors = validateInputs(request.getDoctorPhone());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }
        mService.debugRquest(request);
        mService.execute(mService.getVutdService().addDoctorToMC(request));
    }
}
