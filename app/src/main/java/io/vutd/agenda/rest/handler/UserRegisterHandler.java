package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.UserRegisterRequest;
import io.vutd.agenda.rest.response.UserRegisterResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.Utils;

/**
 * Created by andresmariscal on 24/06/16.
 */
@EBean
public class UserRegisterHandler extends AbstractHandler<UserRegisterResponse, Rule> {

    private static final String TAG = "UserRegisterHandler";

    //error codes
    private final int SERVER_EXCEPTION = 100;
    private final int MISSED_FIELDS = 101;
    private final int EMAIL_UNIQUE = 102;

    /*
    * Android
    * Build.ID; Model
    * Build.BRAND; Name
    * */

    public UserRegisterHandler() {
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(UserRegisterResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case EMAIL_UNIQUE:
                Logger.d(TAG, "EMAIL_UNIQUE");
                errors.add(new Rule(Rule.EMAIL_UNIQUE));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String name, String lastName,String phone,String email, String gender, String role, String deviceOs,String deviceName,
                               String deviceModel) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.NAME), name);
        fieldsRequired.put(new Rule(Rule.LAST_NAME), lastName);
        fieldsRequired.put(new Rule(Rule.PHONE), phone);
        fieldsRequired.put(new Rule(Rule.EMAIL), email);
        fieldsRequired.put(new Rule(Rule.GENDER), gender);
        fieldsRequired.put(new Rule(Rule.ROLE_ID), role);
        fieldsRequired.put(new Rule(Rule.DEVICE_OS), deviceOs);
        fieldsRequired.put(new Rule(Rule.DEVICE_NAME), deviceName);
        fieldsRequired.put(new Rule(Rule.DEVICE_MODEL), deviceModel);
        if(Utils.validateEmailFormat(email))
            fieldsRequired.put(new Rule(Rule.INVALID_EMAIL), "ok");
        else
            fieldsRequired.put(new Rule(Rule.INVALID_EMAIL), null);

        return super.validateInputs(fieldsRequired);

    }

    public void registerUser(UserRegisterRequest request) {
        Set errors = validateInputs(request.getName(),request.getLastName(),request.getPhone(),request.getEmail(),
                request.getGender(),request.getRoleId(),request.getDeviceOs(),request.getDeviceName(),
                request.getDeviceModel());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }


        mService.debugRquest(request);
        mService.execute(mService.getVutdService().userRegister(request));
    }
}
