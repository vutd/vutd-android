package io.vutd.agenda.rest;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by andresmariscal on 23/06/16.
 */
public class RetrofitConfig {
    private static Retrofit retrofit;

    /**
     * Singleton that return the Retrofit instance pointing to http://www.vutd.io/
     * @return
     */
    public static Retrofit getRetrofitInstance(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://api.govutd.com/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
