package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class AddDoctorToMCConfirmationRequest extends BaseRequest {
    @SerializedName("response")
    @Expose
    private boolean response;
    @SerializedName("requestId")
    @Expose
    private long requestId;

    /**
     *
     * @return
     * The response
     */
    public boolean isResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(boolean response) {
        this.response = response;
    }

    /**
     *
     * @return
     * The requestId
     */
    public long getRequestId() {
        return requestId;
    }

    /**
     *
     * @param requestId
     * The requestId
     */
    public void setRequestId(long requestId) {
        this.requestId = requestId;
    }
}
