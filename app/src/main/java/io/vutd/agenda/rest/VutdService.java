package io.vutd.agenda.rest;

import io.vutd.agenda.rest.request.AddDoctorToMCConfirmationRequest;
import io.vutd.agenda.rest.request.AddDoctorToMCRequest;
import io.vutd.agenda.rest.request.ConfirmAppointmentRequest;
import io.vutd.agenda.rest.request.CreateAppointmentRequest;
import io.vutd.agenda.rest.request.CreateConcurrentAppointmentRequest;
import io.vutd.agenda.rest.request.CreateDoctorAvailableDaysRequest;
import io.vutd.agenda.rest.request.CreateDoctorProfileRequest;
import io.vutd.agenda.rest.request.CreateMedicalCoreRequest;
import io.vutd.agenda.rest.request.DeleteAppointmentRequest;
import io.vutd.agenda.rest.request.DeliveredConfirmationRequest;
import io.vutd.agenda.rest.request.GetAppointmentsBookedRequest;
import io.vutd.agenda.rest.request.GetDoctorScheduleByMCRequest;
import io.vutd.agenda.rest.request.GetDoctorsByMCRequest;
import io.vutd.agenda.rest.request.GetMedicalCoreByDoctorRequest;
import io.vutd.agenda.rest.request.RegisterPushNotificationTokenRequest;
import io.vutd.agenda.rest.request.SearchDoctorByPhoneRequest;
import io.vutd.agenda.rest.request.SendConfirmationRequest;
import io.vutd.agenda.rest.request.SubscribeAppointmentRequest;
import io.vutd.agenda.rest.request.UserLoginRequest;
import io.vutd.agenda.rest.request.UserRegisterRequest;
import io.vutd.agenda.rest.request.UserResetPasswordRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.rest.response.CreateAppointmentResponse;
import io.vutd.agenda.rest.response.CreateMedicalCoreResponse;
import io.vutd.agenda.rest.response.GetAppointmentsBookedResponse;
import io.vutd.agenda.rest.response.GetDoctorScheduleByMCResponse;
import io.vutd.agenda.rest.response.GetDoctorsByMCResponse;
import io.vutd.agenda.rest.response.GetMedicalCoreByDoctorResponse;
import io.vutd.agenda.rest.response.SearchDoctorsByPhoneResponse;
import io.vutd.agenda.rest.response.UserLoginResponse;
import io.vutd.agenda.rest.response.UserRegisterResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by andresmariscal on 23/06/16.
 */
public interface VutdService {

    @POST("UserRegister")
    Call<UserRegisterResponse> userRegister(@Body UserRegisterRequest userRegisterRequest);

    @POST("CreateDoctorProfile")
    Call<BaseResponse> createDoctorProfile(@Body CreateDoctorProfileRequest createDoctorProfileRequest);

    @POST("CreateMedicalCore")
    Call<CreateMedicalCoreResponse> createMedicalCore(@Body CreateMedicalCoreRequest createMedicalCoreRequest);

    @POST("CreateDoctorAvailableDays")
    Call<BaseResponse> createDoctorAvailableDays(@Body CreateDoctorAvailableDaysRequest createDoctorAvailableDaysRequest);

    @POST("UserLogin")
    Call<UserLoginResponse> userLogin(@Body UserLoginRequest userLoginRequest);

    @POST("UserResetPassword")
    Call<BaseResponse> userResetPassword(@Body UserResetPasswordRequest userResetPasswordRequest);

    @POST("CreateAppointment")
    Call<CreateAppointmentResponse> createAppointment(@Body CreateAppointmentRequest createAppointmentRequest);

    @POST("GetDoctorsByMC")
    Call<GetDoctorsByMCResponse> getDoctorsByMC(@Body GetDoctorsByMCRequest baseRequest);

    @POST("DeleteAppointment")
    Call<BaseResponse> deleteAppointment(@Body DeleteAppointmentRequest deleteAppointmentRequest);

    @POST("GetDoctorScheduleByMC")
    Call<GetDoctorScheduleByMCResponse> getDoctorScheduleByMC(@Body GetDoctorScheduleByMCRequest getDoctorScheduleByMCRequest);

    @POST("AddDoctorToMedicalCore")
    Call<BaseResponse> addDoctorToMC(@Body AddDoctorToMCRequest addDoctorToMCRequest);

    @POST("AddDoctorToMedicalCoreConfirmation")
    Call<BaseResponse> addDoctorToMCConfirmation(@Body AddDoctorToMCConfirmationRequest addDoctorToMCConfirmationRequest);

    @POST("GetMedicalCoresByDoctor")
    Call<GetMedicalCoreByDoctorResponse> getMedicalCoresByDoctor(@Body GetMedicalCoreByDoctorRequest baseRequest);

    @POST("SearchDoctorByPhone")
    Call<SearchDoctorsByPhoneResponse> searchDoctorByPhone(@Body SearchDoctorByPhoneRequest baseRequest);

    @POST("GetAppointmentsBooked")
    Call<GetAppointmentsBookedResponse> getAppointmentsBooked(@Body GetAppointmentsBookedRequest getAppointmentsBookedRequest);

    @POST("RegisterPushNotificationToken")
    Call<BaseResponse> registerPushNotificationToken(@Body RegisterPushNotificationTokenRequest registerPushNotificationTokenRequest);

    @POST("CreateConcurrentAppointment")
    Call<BaseResponse> createConcurrentAppointments(@Body CreateConcurrentAppointmentRequest createConcurrentAppointmentRequest);

    @POST("SendConfirmationRequest")
    Call<BaseResponse> sendConfirmationRequest(@Body SendConfirmationRequest sendConfirmationRequest);

    @POST("DeliveredConfirmationRequest")
    Call<BaseResponse> deliveredConfirmationRequest(@Body DeliveredConfirmationRequest deliveredConfirmationRequest);

    @POST("SubscribeAppointment")
    Call<BaseResponse> subscribeAppointment(@Body SubscribeAppointmentRequest subscribeAppointmentRequest);

    @POST("ConfirmAppointment")
    Call<BaseResponse> confirmAppointment(@Body ConfirmAppointmentRequest confirmAppointmentRequest);
}
