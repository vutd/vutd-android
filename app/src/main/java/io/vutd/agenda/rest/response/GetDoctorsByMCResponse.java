package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.DoctorCommon;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class GetDoctorsByMCResponse extends BaseResponse{
    @SerializedName("doctors")
    @Expose
    private List<DoctorCommon> doctorCommons = new ArrayList<DoctorCommon>();

    /**
     *
     * @return
     * The doctorCommons
     */
    public List<DoctorCommon> getDoctorCommons() {
        return doctorCommons;
    }

    /**
     *
     * @param doctorCommons
     * The doctorCommons
     */
    public void setDoctorCommons(List<DoctorCommon> doctorCommons) {
        this.doctorCommons = doctorCommons;
    }

}
