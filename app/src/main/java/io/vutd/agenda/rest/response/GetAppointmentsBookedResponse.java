package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.AppointmentBookedCommon;


/**
 * Created by andresmariscal on 06/09/16.
 */
@EBean
public class GetAppointmentsBookedResponse extends BaseResponse {
    @SerializedName("appointmentsBooked")
    @Expose
    private List<AppointmentBookedCommon> appointmentBookedCommons = new ArrayList<AppointmentBookedCommon>();

    public List<AppointmentBookedCommon> getAppointmentBookedCommons() {
        return appointmentBookedCommons;
    }

    public void setDoctorCommons(List<AppointmentBookedCommon> doctorCommons) {
        this.appointmentBookedCommons = doctorCommons;
    }
}
