package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.GetDoctorScheduleByMCRequest;
import io.vutd.agenda.rest.response.GetDoctorScheduleByMCResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class GetDoctorScheduleByMCHandler extends AbstractHandler<GetDoctorScheduleByMCResponse, Rule> {

    public static final String TAG = "ScheduleByMCHandler";
    //Error codes
    private final int MISSED_FIELDS           = 1001;
    private final int INVALID_DOCTOR          = 1002;
    private final int INVALID_MEDICAL_CORE    = 1003;
    private final int DOCTOR_NOT_IN_MC        = 1004;
    private final int DATE_FILTER             = 1005;

    public GetDoctorScheduleByMCHandler(){
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(GetDoctorScheduleByMCResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet<>();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_DOCTOR:
                Logger.d(TAG, "INVALID_DOCTOR");
                errors.add(new Rule(Rule.INVALID_DOCTOR));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_MEDICAL_CORE:
                Logger.d(TAG, "INVALID_MEDICAL_CORE");
                errors.add(new Rule(Rule.INVALID_MEDICAL_CORE));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_NOT_IN_MC:
                Logger.d(TAG, "DOCTOR_NOT_IN_MC");
                errors.add(new Rule(Rule.DOCTOR_NOT_IN_MC));
                mHandlerResult.onBadResult(errors);
                break;
            case DATE_FILTER:
                Logger.d(TAG, "DATE_FILTER");
                errors.add(new Rule(Rule.DATE_FILTER));
                mHandlerResult.onBadResult(errors);
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    public Set validateInputs(String medicalCoreId, String doctorId){
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.MEDICAL_CORE_ID), medicalCoreId);
        fieldsRequired.put(new Rule(Rule.DOCTOR_ID), doctorId);
        return super.validateInputs(fieldsRequired);
    }

    public void getSchedule(GetDoctorScheduleByMCRequest request){
        try{
            Set errors = validateInputs(String.valueOf(request.getMedicalCoreId()), String.valueOf(request.getDoctorId()));
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }

            mService.debugRquest(request);
            mService.execute(mService.getVutdService().getDoctorScheduleByMC(request));
        }catch (Exception e){
            Logger.e(TAG, "Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }

    }
}
