package io.vutd.agenda.rest.handler;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 04/07/16.
 */
public class Rule {

    @IntDef({

            //AddDoctorToMCConfirmationRule
            RESPONSE,
            REQUEST_ID,
            INVALID_REQUEST_ID,
            INVALID_USER,
            INVALID_INVITATION,

            //AddDoctorToMCRule
            DOCTOR_PHONE,
            INVALID_PHONE,
            NOT_A_DOCTOR,
            DOCTOR_WITHOUT_PROFILE,
            DOCTOR_ALREADY_INCLUDED,
            INVITATION_ALREADY_SEND,

            //CreateAppointmentRule
            DOCTOR_ID,
            MEDICAL_CORE_ID,
            BOOK_DATE,
            START_TIME,
            BOOK_TYPE,
            PATIENT_NAME,
            PATIENT_PHONE,
            INVALID_BOOK_TYPE,
            INVALID_DOCTOR,
            INVALID_MEDICAL_CORE,
            DUE_APPOINTMENT,
            APPOINTMENT_OUT_OF_RANGE,

            //CreateDoctorAvailableDaysRule
            DAYS,
            DAY,
            END_TIME,
            REST_END_TIME,
            REST_START_TIME,
            APPOINTMENT_DURATION,
            DOCTOR_NOT_IN_MC,

            //CreateDoctorProfileRule
            PROFESSIONAL_STATEMENT,
            SCHOOL,
            LANGUAGES,
            SPECIALITY,
            INVALID_DOCTOR_ROLE,
            PROFILE_ALREADY_EXISTS,

            //CreateMedicalCoreRule
            NAME,
            LAT,
            LON,
            ADDRESS,
            ZIPCODE,
            PROVINCE,
            CITY,
            STATE,
            COUNTRY,
            NUM_EXT,
            PHONE_NUMBERS,
            INVALID_MC_ROLE,

            //DeleteAppointmentRule
            APPOINTMENT_ID,
            REASON_ID,
            INVALID_APPOINTMENT_ID,
            INVALID_REASON_ID,

            //GetDoctorsByMCRule
            MC_WITHOUT_DOCTORS,
            DATE_FILTER,

            //GetMedicalCoresByDoctorRule
            DOCTOR_WITHOUT_MC,

            //UserLoginRule
            EMAIL,
            PASSWORD,
            DEVICE_OS,
            DEVICE_NAME,
            DEVICE_MODEL,
            WRONG_PASSWORD,

            //UserRegisterRule
            LAST_NAME,
            PHONE,
            GENDER,
            BIRTHDAY,
            ROLE_ID,
            FACEBOOK_TOKEN,
            EMAIL_UNIQUE,
            CONFIRM_PASSWORD,

            //UserResetPasswordRule
            NEW_PASSWORD,
            OLD_PASSWORD,
            WRONG_OLD_PASSWORD,

            //General rules
            SESSION_ID,
            INVALID_FORMAT,
            INVALID_EMAIL,
            MISSED_FIELDS,
            SERVER_EXCEPTION,
            UNHANDLED_ERROR,
            RETROFIT_FAILURE,
            LOCAL_EXCEPTION,

            //Register Push Notification Token
            PUSH_NOTIFICATION_TOKEN_REQUIRED,

            //Create Concurrent Appointment
            MAX_30,

            //Send Confirmation Request
            USER_NOT_FOUND,
            FCM_FAILURE,

            //Delivered Confirmation Request
            INVALID_STATE,

            //Confirm Appointment,
            BOOK_TYPE_INVALID,
            OUT_OF_RANGE,

            //Subscribe appointment
            USER_ALREADY_SUBSCRIBED,
            OWNER_ERROR,
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface ASRule {
    }

    @ASRule int rule;

    //General rules
    public static final int SESSION_ID       = R.string.session_id_field_required;
    public static final int INVALID_FORMAT   = R.string.invalid_format;
    public static final int INVALID_EMAIL    = R.string.invalid_email;
    public static final int MISSED_FIELDS    = R.string.missed_fields;
    public static final int SERVER_EXCEPTION = R.string.server_exception;
    public static final int UNHANDLED_ERROR  = R.string.unhandled_error;
    public static final int RETROFIT_FAILURE = R.string.retrofit_failure;
    public static final int LOCAL_EXCEPTION  = R.string.internal_failure;

    //User Login Rules
    public static final int EMAIL          = R.string.email_field_required;
    public static final int PASSWORD       = R.string.password_field_required;
    public static final int DEVICE_OS      = R.string.device_os_field_required;
    public static final int DEVICE_NAME    = R.string.device_name_field_required;
    public static final int DEVICE_MODEL   = R.string.device_model_field_required;
    public static final int WRONG_PASSWORD = R.string.user_login_handler_wrong_password;

    //User Register Rules
    public static final int NAME      = R.string.name_field_required;
    public static final int LAST_NAME       = R.string.last_name_field_required;
    public static final int PHONE           = R.string.phone_field_required;
    public static final int GENDER          = R.string.gender_field_required;
    public static final int BIRTHDAY        = R.string.birthday_field_required;
    public static final int ROLE_ID         = R.string.role_id_field_required;
    public static final int FACEBOOK_TOKEN  = R.string.facebook_token_field_required;
    public static final int EMAIL_UNIQUE    = R.string.user_register_handler_email_unique;
    public static final int CONFIRM_PASSWORD = R.string.user_register_handler_confirm_password;

    //User Reset Password Rules
    public static final int NEW_PASSWORD        = R.string.new_password_field_required;
    public static final int OLD_PASSWORD        = R.string.old_password_field_required;
    public static final int WRONG_OLD_PASSWORD  = R.string.user_reset_password_handler_wrong_old_password;

    //Get Doctor Schedule By MC Rules
    public static final int DOCTOR_NOT_IN_MC        = R.string.doctor_not_in_mc;

    //Get Doctors By MC Rules
    public static final int MC_WITHOUT_DOCTORS  = R.string.get_doctors_by_mc_handler_mc_without_doctors;
    public static final int DATE_FILTER         = R.string.date_filter;

    //Delete Appointment Rules
    public static final int APPOINTMENT_ID          = R.string.appointment_id_field_required;
    public static final int REASON_ID               = R.string.reason_id_field_required;
    public static final int INVALID_APPOINTMENT_ID  = R.string.delete_appointment_handler_invalid_appointment_id;
    public static final int INVALID_REASON_ID       = R.string.delete_appointment_handler_invalid_reason_id;

    //Create Medical Core Rules
    public static final int LAT                 = R.string.lat_field_required;
    public static final int LON                 = R.string.lon_field_required;
    public static final int ADDRESS             = R.string.address_field_required;
    public static final int ZIPCODE             = R.string.zipcode_field_required;
    public static final int PROVINCE            = R.string.province_field_required;
    public static final int CITY                = R.string.city_field_required;
    public static final int STATE               = R.string.state_field_required;
    public static final int COUNTRY             = R.string.country_field_required;
    public static final int NUM_EXT             = R.string.num_ext_field_required;
    public static final int PHONE_NUMBERS       = R.string.phone_number_field_required;
    public static final int INVALID_MC_ROLE     = R.string.create_medical_core_handler_invalid_user;

    //Create Doctor Profile Rules
    public static final int PROFESSIONAL_STATEMENT  = R.string.professional_statement_field_required;
    public static final int SCHOOL                  = R.string.school_field_required;
    public static final int LANGUAGES               = R.string.languages_field_required;
    public static final int SPECIALITY              = R.string.speciality_field_required;
    public static final int INVALID_DOCTOR_ROLE     = R.string.create_doctor_profile_handler_invalid_role;
    public static final int PROFILE_ALREADY_EXISTS  = R.string.create_doctor_profile_handler_profile_alredy_exists;

    //Create Doctor Available Days Rules
    public static final int DAYS                    = R.string.days_field_required;
    public static final int DAY                     = R.string.day_field_required;
    public static final int END_TIME                = R.string.end_time_field_required;
    public static final int REST_START_TIME         = R.string.rest_start_time_field_required;
    public static final int REST_END_TIME           = R.string.rest_end_time_field_required;
    public static final int APPOINTMENT_DURATION    = R.string.appointment_duration_field_required;

    //Create Appointment Rule
    public static final int DOCTOR_ID                   = R.string.doctor_id_field_required;
    public static final int MEDICAL_CORE_ID             = R.string.medical_core_id_field_required;
    public static final int BOOK_DATE                   = R.string.book_date_field_required;
    public static final int START_TIME                  = R.string.start_time_field_required;
    public static final int BOOK_TYPE                   = R.string.book_type_field_required;
    public static final int PATIENT_NAME                = R.string.patient_name_field_required;
    public static final int PATIENT_PHONE               = R.string.patient_phone_field_required;
    public static final int INVALID_BOOK_TYPE           = R.string.create_appointment_handler_invalid_book_type;
    public static final int INVALID_DOCTOR              = R.string.invalid_doctor;
    public static final int INVALID_MEDICAL_CORE        = R.string.invalid_medical_core;
    public static final int DUE_APPOINTMENT             = R.string.create_appointment_handler_due_appointment;
    public static final int APPOINTMENT_OUT_OF_RANGE    = R.string.create_appointment_handler_appointment_out_of_range;

    //AddDoctorToMCRule
    public static final int DOCTOR_PHONE             = R.string.doctor_phone_field_required;
    public static final int INVALID_PHONE            = R.string.add_doctor_handler_invalid_phone;
    public static final int NOT_A_DOCTOR             = R.string.add_doctor_handler_not_a_doctor;
    public static final int DOCTOR_WITHOUT_PROFILE   = R.string.add_doctor_handler_doctor_without_profile;
    public static final int DOCTOR_ALREADY_INCLUDED  = R.string.add_doctor_handler_doctor_already_included;
    public static final int INVITATION_ALREADY_SEND  = R.string.add_doctor_handler_invitation_already_send;

    public static final int RESPONSE           = R.string.response_field_required;
    public static final int REQUEST_ID         = R.string.request_id_field_required;
    public static final int INVALID_USER       = R.string.add_doctor_to_mc_confirmation_handler_invalid_user;
    public static final int INVALID_REQUEST_ID = R.string.add_doctor_to_mc_confirmation_handler_invalid_request_id;
    public static final int INVALID_INVITATION = R.string.add_doctor_to_mc_confirmation_handler_invalid_invitation;

    public static final int DOCTOR_WITHOUT_MC = R.string.doctor_without_mc;

    //Register Push Notification
    public static final int PUSH_NOTIFICATION_TOKEN_REQUIRED = R.string.register_push_notification_required;

    //Create Concurrent Appointment
    public static final int MAX_30 = R.string.create_concurrent_appointment_max_30;

    //Send Confirmation Request
    public static final int USER_NOT_FOUND = R.string.send_confirmation_request_user_not_found;
    public static final int FCM_FAILURE = R.string.send_confirmation_request_fcm_failure;

    //Delivered Confirmation Request
    public static final int INVALID_STATE = R.string.delivered_confirmation_request_invalid_state;

    //Confirm Appointment
    public static final int BOOK_TYPE_INVALID = R.string.confirm_appointment_invalid_book_type;
    public static final int OUT_OF_RANGE = R.string.confirm_appointment_out_of_range;

    //Subscribe Appointment
    public static final int USER_ALREADY_SUBSCRIBED = R.string.subscribe_appointment_user_already_subscribed;
    public static final int OWNER_ERROR = R.string.subscribe_appointment_owner_error;


    public Rule(@ASRule int rule) {
        this.rule = rule;
    }

    public int getErrorStringId() {
        return rule;
    }

    @Override
    public boolean equals(Object obj) {
        return this.rule == ((Rule)obj).getErrorStringId();
    }

    @Override
    public int hashCode() {
        return rule;
    }
}
