package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.SimpleCatalogCommon;

/**
 * Created by andresmariscal on 25/06/16.
 */
@EBean
public class CreateDoctorProfileRequest extends BaseRequest{
    @SerializedName("professionalStatement")
    @Expose
    private String professionalStatement;
    @SerializedName("facebookUrl")
    @Expose
    private String facebookUrl;
    @SerializedName("instagramUrl")
    @Expose
    private String instagramUrl;
    @SerializedName("twitterUrl")
    @Expose
    private String twitterUrl;
    @SerializedName("professionalNumber")
    @Expose
    private String professionalNumber;
    @SerializedName("reachOnEmergency")
    @Expose
    private boolean reachOnEmergency;
    @SerializedName("school")
    @Expose
    private List<String> school = new ArrayList<String>();
    @SerializedName("languagesSpoken")
    @Expose
    private List<Integer> languagesSpoken = new ArrayList<Integer>();
    @SerializedName("professionalMembership")
    @Expose
    private SimpleCatalogCommon professionalMembership;
    @SerializedName("speciality")
    @Expose
    private int speciality;
    @SerializedName("lifeInsurance")
    @Expose
    private List<Integer> lifeInsurance = new ArrayList<Integer>();

    /**
     *
     * @return
     * The professionalStatement
     */
    public String getProfessionalStatement() {
        return professionalStatement;
    }

    /**
     *
     * @param professionalStatement
     * The professionalStatement
     */
    public void setProfessionalStatement(String professionalStatement) {
        this.professionalStatement = professionalStatement;
    }

    /**
     *
     * @return
     * The facebookUrl
     */
    public String getFacebookUrl() {
        return facebookUrl;
    }

    /**
     *
     * @param facebookUrl
     * The facebookUrl
     */
    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    /**
     *
     * @return
     * The instagramUrl
     */
    public String getInstagramUrl() {
        return instagramUrl;
    }

    /**
     *
     * @param instagramUrl
     * The instagramUrl
     */
    public void setInstagramUrl(String instagramUrl) {
        this.instagramUrl = instagramUrl;
    }

    /**
     *
     * @return
     * The twitterUrl
     */
    public String getTwitterUrl() {
        return twitterUrl;
    }

    /**
     *
     * @param twitterUrl
     * The twitterUrl
     */
    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    /**
     *
     * @return
     * The professionalNumber
     */
    public String getProfessionalNumber() {
        return professionalNumber;
    }

    /**
     *
     * @param professionalNumber
     * The professionalNumber
     */
    public void setProfessionalNumber(String professionalNumber) {
        this.professionalNumber = professionalNumber;
    }

    /**
     *
     * @return
     * The reachOnEmergency
     */
    public boolean isReachOnEmergency() {
        return reachOnEmergency;
    }

    /**
     *
     * @param reachOnEmergency
     * The reachOnEmergency
     */
    public void setReachOnEmergency(boolean reachOnEmergency) {
        this.reachOnEmergency = reachOnEmergency;
    }

    /**
     *
     * @return
     * The school
     */
    public List<String> getSchool() {
        return school;
    }

    /**
     *
     * @param school
     * The school
     */
    public void setSchool(List<String> school) {
        this.school = school;
    }

    /**
     *
     * @return
     * The languagesSpoken
     */
    public List<Integer> getLanguagesSpoken() {
        return languagesSpoken;
    }

    /**
     *
     * @param languagesSpoken
     * The languagesSpoken
     */
    public void setLanguagesSpoken(List<Integer> languagesSpoken) {
        this.languagesSpoken = languagesSpoken;
    }

    /**
     *
     * @return
     * The professionalMembership
     */
    public SimpleCatalogCommon getProfessionalMembership() {
        return professionalMembership;
    }

    /**
     *
     * @param professionalMembership
     * The professionalMembership
     */
    public void setProfessionalMembership(SimpleCatalogCommon professionalMembership) {
        this.professionalMembership = professionalMembership;
    }

    /**
     *
     * @return
     * The speciality
     */
    public int getSpeciality() {
        return speciality;
    }

    /**
     *
     * @param speciality
     * The speciality
     */
    public void setSpeciality(int speciality) {
        this.speciality = speciality;
    }

    /**
     *
     * @return
     * The lifeInsurance
     */
    public List<Integer> getLifeInsurance() {
        return lifeInsurance;
    }

    /**
     *
     * @param lifeInsurance
     * The lifeInsurance
     */
    public void setLifeInsurance(List<Integer> lifeInsurance) {
        this.lifeInsurance = lifeInsurance;
    }
}
