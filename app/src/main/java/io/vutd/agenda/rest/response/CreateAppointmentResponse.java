package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 24/07/16.
 */
public class CreateAppointmentResponse extends BaseResponse {
    @SerializedName("appointmentCreated")
    @Expose
    private long appointmentCreated;

    public long getAppointmentCreated() {
        return appointmentCreated;
    }

    public void setAppointmentCreated(long appointmentCreated) {
        this.appointmentCreated = appointmentCreated;
    }
}
