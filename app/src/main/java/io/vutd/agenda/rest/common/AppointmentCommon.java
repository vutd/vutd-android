package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class AppointmentCommon {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("bookTypeId")
    @Expose
    private long bookTypeId;
    @SerializedName("userId")
    @Expose
    private long appointmentCreatorId;
    @SerializedName("patientName")
    @Expose
    private String patientName;
    @SerializedName("patientPhone")
    @Expose
    private String patientPhone;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("confirmationRequestSend")
    @Expose
    private int confirmationRequestSend;
    @SerializedName("confirmationRequestDelivered")
    @Expose
    private int confirmationRequestDelivered;
    @SerializedName("subscribers")
    @Expose
    private int subscribers;
    /**
     *
     * @return
     * The id
     */
    public long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The date
     */
    public String getDate() {
        return date;
    }

    /**
     *
     * @param date
     * The date
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     *
     * @return
     * The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     * The endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     *
     * @param endTime
     * The endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     *
     * @return
     * The bookTypeId
     */
    public long getBookTypeId() {
        return bookTypeId;
    }

    /**
     *
     * @param bookTypeId
     * The bookTypeId
     */
    public void setBookTypeId(long bookTypeId) {
        this.bookTypeId = bookTypeId;
    }

    /**
     *
     * @return
     * The appointmentCreatorId
     */
    public long getAppointmentCreatorId() {
        return appointmentCreatorId;
    }

    /**
     *
     * @param appointmentCreatorId
     * The appointmentCreatorId
     */
    public void setAppointmentCreatorId(long appointmentCreatorId) {
        this.appointmentCreatorId = appointmentCreatorId;
    }

    /**
     *
     * @return
     * The patientName
     */
    public String getPatientName() {
        return patientName;
    }

    /**
     *
     * @param patientName
     * The patientName
     */
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    /**
     *
     * @return
     * The patientPhone
     */
    public String getPatientPhone() {
        return patientPhone;
    }

    /**
     *
     * @param patientPhone
     * The patientPhone
     */
    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public int getConfirmationRequestSend() {
        return confirmationRequestSend;
    }

    public void setConfirmationRequestSend(int confirmationRequestSend) {
        this.confirmationRequestSend = confirmationRequestSend;
    }

    public int getConfirmationRequestDelivered() {
        return confirmationRequestDelivered;
    }

    public void setConfirmationRequestDelivered(int confirmationRequestDelivered) {
        this.confirmationRequestDelivered = confirmationRequestDelivered;
    }

    public int getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(int subscribers) {
        this.subscribers = subscribers;
    }
}
