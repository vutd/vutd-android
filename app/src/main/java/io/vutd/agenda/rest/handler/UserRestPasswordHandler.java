package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.UserResetPasswordRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 28/06/16.
 */
public class UserRestPasswordHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "RestPasswordHandler";

    //Error Codes
    private final int SERVER_EXCEPTION      = 600;
    private final int MISSED_FIELDS         = 601;
    private final int OLD_PASSWORD          = 602;
    private final int WRONG_OLD_PASSWORD    = 603;

    public UserRestPasswordHandler(IHandlerResult handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case OLD_PASSWORD:
                Logger.d(TAG, "OLD_PASSWORD");
                errors.add(new Rule(Rule.OLD_PASSWORD));
                mHandlerResult.onBadResult(errors);
                break;
            case WRONG_OLD_PASSWORD:
                Logger.d(TAG, "WRONG_OLD_PASSWORD");
                errors.add(new Rule(Rule.WRONG_OLD_PASSWORD));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String newPassword) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.NEW_PASSWORD), newPassword);
        return super.validateInputs(fieldsRequired);
    }

    public void resetPassword(UserResetPasswordRequest request){
        Set errors = validateInputs(request.getNewPassword());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }

        mService.debugRquest(request);
        mService.execute(mService.getVutdService().userResetPassword(request));
    }
}
