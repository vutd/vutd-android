package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 24/09/16.
 */
@EBean
public class RegisterPushNotificationTokenRequest extends BaseRequest {
    @SerializedName("pushNotificationToken")
    @Expose
    private String pushNotificationToken;
    @SerializedName("deviceOs")
    @Expose
    private String deviceOs;
    @SerializedName("deviceName")
    @Expose
    private String deviceName;
    @SerializedName("deviceModel")
    @Expose
    private String deviceModel;

    public String getPushNotificationToken() {
        return pushNotificationToken;
    }

    public void setPushNotificationToken(String pushNotificationToken) {
        this.pushNotificationToken = pushNotificationToken;
    }

    /**
     *
     * @return
     * The deviceOs
     */
    public String getDeviceOs() {
        return deviceOs;
    }

    /**
     *
     * @param deviceOs
     * The deviceOs
     */
    public void setDeviceOs(String deviceOs) {
        this.deviceOs = deviceOs;
    }

    /**
     *
     * @return
     * The deviceName
     */
    public String getDeviceName() {
        return deviceName;
    }

    /**
     *
     * @param deviceName
     * The deviceName
     */
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     *
     * @return
     * The deviceModel
     */
    public String getDeviceModel() {
        return deviceModel;
    }

    /**
     *
     * @param deviceModel
     * The deviceModel
     */
    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }
}
