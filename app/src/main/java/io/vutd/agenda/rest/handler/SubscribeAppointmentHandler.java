package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.SubscribeAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 02/10/16.
 */
@EBean
public class SubscribeAppointmentHandler extends AbstractHandler<BaseResponse, Rule> {
    public static final String TAG = "SubscribeAppointmentHandler";
    private final int SERVER_EXCEPTION          = 2100;
    private final int MISSED_FIELDS             = 2101;
    private final int USER_ALREADY_SUBSCRIBED   = 2102;
    private final int OWNER_ERROR               = 2103;

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case USER_ALREADY_SUBSCRIBED:
                Logger.d(TAG, "USER_ALREADY_SUBSCRIBED");
                errors.add(new Rule(Rule.USER_ALREADY_SUBSCRIBED));
                mHandlerResult.onBadResult(errors);
                break;
            case OWNER_ERROR:
                Logger.d(TAG, "OWNER_ERROR");
                errors.add(new Rule(Rule.OWNER_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String appointmentId) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.APPOINTMENT_ID), appointmentId);
        return super.validateInputs(fieldsRequired);
    }

    public void subscribeAppointment(SubscribeAppointmentRequest request){
        try {
            Set errors = validateInputs(String.valueOf(request.getAppointmentId()));
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().subscribeAppointment(request));
        } catch (Exception e){
            Logger.e(TAG, "createAppointment Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
