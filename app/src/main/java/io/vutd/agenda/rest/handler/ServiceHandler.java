package io.vutd.agenda.rest.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import io.vutd.agenda.rest.RetrofitConfig;
import io.vutd.agenda.rest.VutdService;
import io.vutd.agenda.utils.Logger;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by andresmariscal on 24/06/16.
 */
public class ServiceHandler<REQUEST, RESPONSE> {

    private String TAG;
    private VutdService vutdService;
    private ICallbackService<RESPONSE> mCallback;

    public ServiceHandler() {
        vutdService = RetrofitConfig.getRetrofitInstance().create(VutdService.class);
    }

    public void setTAG(String TAG) {
        this.TAG = TAG;
    }

    public String getTAG() {
        if(TAG != null) {
            return TAG;
        } else {
            return getClass().getName();
        }
    }

    public VutdService getVutdService() {
        return vutdService;
    }

    public void debugRquest(REQUEST mRequest) {
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
        //TODO: Delete or comment this log on PROD
        GsonBuilder builder = new GsonBuilder();
        builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
        builder.excludeFieldsWithoutExposeAnnotation();
        Gson gson = builder.create();
        Logger.d(getTAG(), "requestObject(): " + gson.toJson(mRequest));
        //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    }

    public void setCallback(ICallbackService<RESPONSE> mCallback) {
        this.mCallback = mCallback;
    }

    public void execute(Call<RESPONSE> call) {
        if (!call.isExecuted()) {
            Logger.d(getTAG(), "execute(): URL: " + call.request().url().toString());
            call.enqueue(new Callback<RESPONSE>() {
                @Override
                public void onResponse(Call<RESPONSE> call, Response<RESPONSE> response) {
                    //Here we can handle the server codes 200, 400 etc...
                    if (response.isSuccessful()) {
                        try {
                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                            //TODO: Delete or comment this log on PROD
                            GsonBuilder builder = new GsonBuilder();
                            builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                            builder.excludeFieldsWithoutExposeAnnotation();
                            Gson gson = builder.create();
                            Logger.d(getTAG(), "responseObject(): " + gson.toJson(response.body()));
                            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                            Method method = response.body().getClass().getMethod("isError");
                            Boolean isError = (Boolean) method.invoke(response.body());

                            if (isError) {
                                mCallback.onError(response.body());
                            } else {
                                mCallback.onSuccess(response.body());
                            }

                        } catch (NoSuchMethodException e) {
                            Logger.e("NoSuchMethodException", e.getMessage());
                            mCallback.onFailure(e.getMessage());
                        } catch (InvocationTargetException e) {
                            Logger.e("InvocationTargetException", e.getMessage());
                            mCallback.onFailure(e.getMessage());
                        } catch (IllegalAccessException e) {
                            Logger.e("IllegalAccessException", e.getMessage());
                            mCallback.onFailure(e.getMessage());
                        } catch (Exception e) {
                            Logger.e("Exception", e.getMessage());
                            mCallback.onFailure(e.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<RESPONSE> call, Throwable t) {
                    Logger.e(getTAG(), "onFailure(): " + t.getMessage(), t);
                    mCallback.onFailure(t.getMessage());
                }
            });
        }
    }
}
