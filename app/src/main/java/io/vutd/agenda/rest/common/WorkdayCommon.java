package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 26/06/16.
 */
public class WorkdayCommon {
    @SerializedName("day")
    @Expose
    private int day;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("endTime")
    @Expose
    private String endTime;
    @SerializedName("restStartTime")
    @Expose
    private String restStartTime;
    @SerializedName("restEndTime")
    @Expose
    private String restEndTime;
    @SerializedName("appointmentDuration")
    @Expose
    private String appointmentDuration;

    /**
     *
     * @return
     * The day
     */
    public int getDay() {
        return day;
    }

    /**
     *
     * @param day
     * The day
     */
    public void setDay(int day) {
        this.day = day;
    }

    /**
     *
     * @return
     * The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     * The endTime
     */
    public String getEndTime() {
        return endTime;
    }

    /**
     *
     * @param endTime
     * The endTime
     */
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     *
     * @return
     * The restStartTime
     */
    public String getRestStartTime() {
        return restStartTime;
    }

    /**
     *
     * @param restStartTime
     * The restStartTime
     */
    public void setRestStartTime(String restStartTime) {
        this.restStartTime = restStartTime;
    }

    /**
     *
     * @return
     * The restEndTime
     */
    public String getRestEndTime() {
        return restEndTime;
    }

    /**
     *
     * @param restEndTime
     * The restEndTime
     */
    public void setRestEndTime(String restEndTime) {
        this.restEndTime = restEndTime;
    }

    /**
     *
     * @return
     * The appointmentDuration
     */
    public String getAppointmentDuration() {
        return appointmentDuration;
    }

    /**
     *
     * @param appointmentDuration
     * The appointmentDuration
     */
    public void setAppointmentDuration(String appointmentDuration) {
        this.appointmentDuration = appointmentDuration;
    }
}
