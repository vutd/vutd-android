package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.DoctorProfileCommon;

/**
 * Created by andresmariscal on 29/08/16.
 */
@EBean
public class SearchDoctorsByPhoneResponse extends BaseResponse {
    @SerializedName("doctors")
    @Expose
    private List<DoctorProfileCommon> doctorCommons = new ArrayList<DoctorProfileCommon>();

    public List<DoctorProfileCommon> getDoctorCommons() {
        return doctorCommons;
    }

    public void setDoctorCommons(List<DoctorProfileCommon> doctorCommons) {
        this.doctorCommons = doctorCommons;
    }
}
