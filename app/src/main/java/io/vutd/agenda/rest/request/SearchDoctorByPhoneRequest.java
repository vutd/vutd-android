package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 29/08/16.
 */
@EBean
public class SearchDoctorByPhoneRequest extends BaseRequest {
    @SerializedName("phone")
    @Expose
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
