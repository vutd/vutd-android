package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.UserLoginRequest;
import io.vutd.agenda.rest.response.UserLoginResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.Utils;

/**
 * Created by andresmariscal on 27/06/16.
 */
@EBean
public class UserLoginHandler extends AbstractHandler<UserLoginResponse, Rule> {

    public static final String TAG = "UserLoginHandler";
    private final int SERVER_EXCEPTION  = 500;
    private final int MISSED_FIELDS     = 501;
    private final int INVALID_EMAIL     = 502;
    private final int WRONG_PASSWORD    = 503;

    public UserLoginHandler() {
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(UserLoginResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_EMAIL:
                Logger.d(TAG, "INVALID_EMAIL");
                errors.add(new Rule(Rule.INVALID_EMAIL));
                mHandlerResult.onBadResult(errors);
                break;
            case WRONG_PASSWORD:
                Logger.d(TAG, "WRONG_PASSWORD");
                errors.add(new Rule(Rule.WRONG_PASSWORD));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String email, String password, String deviceOs, String deviceName, String deviceModel) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.EMAIL), email);
        fieldsRequired.put(new Rule(Rule.PASSWORD), password);
        fieldsRequired.put(new Rule(Rule.DEVICE_OS), deviceOs);
        fieldsRequired.put(new Rule(Rule.DEVICE_NAME), deviceName);
        fieldsRequired.put(new Rule(Rule.DEVICE_MODEL), deviceModel);
        if(Utils.validateEmailFormat(email))
            fieldsRequired.put(new Rule(Rule.INVALID_EMAIL), "ok");
        else
            fieldsRequired.put(new Rule(Rule.INVALID_EMAIL), null);

        return super.validateInputs(fieldsRequired);
    }

    public void login(UserLoginRequest request) {
        String passwordByPass = request.getPassword();
        if(request.getFacebookId()!= null)
            passwordByPass = "ok";

        Set errors = validateInputs(request.getEmail(), passwordByPass, request.getDeviceOs(),
                request.getDeviceName(), request.getDeviceModel());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }
        mService.debugRquest(request);
        mService.execute(mService.getVutdService().userLogin(request));
    }
}
