package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.SendConfirmationRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 02/10/16.
 */
@EBean
public class SendConfirmationRequestHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "SendConfirmationRequestHandler";
    private final int SERVER_EXCEPTION          = 1900;
    private final int MISSED_FIELDS             = 1901;
    private final int INVALID_USER              = 1902;
    private final int USER_NOT_FOUND            = 1903;
    private final int FCM_FAILURE               = 1904;

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_USER");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            case USER_NOT_FOUND:
                Logger.d(TAG, "USER_NOT_FOUND");
                errors.add(new Rule(Rule.USER_NOT_FOUND));
                mHandlerResult.onBadResult(errors);
                break;
            case FCM_FAILURE:
                Logger.d(TAG, "FCM_FAILURE");
                errors.add(new Rule(Rule.FCM_FAILURE));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String appointmentId) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.APPOINTMENT_ID), appointmentId);
        return super.validateInputs(fieldsRequired);
    }

    public void sendConfirmationRequest(SendConfirmationRequest request){
        try {
            Set errors = validateInputs(String.valueOf(request.getAppointmentId()));
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().sendConfirmationRequest(request));
        } catch (Exception e){
            Logger.e(TAG, "createAppointment Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
