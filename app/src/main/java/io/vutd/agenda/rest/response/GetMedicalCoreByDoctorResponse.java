package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.DoctorCommon;
import io.vutd.agenda.rest.common.MedicalCoreCommon;

/**
 * Created by andresmariscal on 13/07/16.
 */
public class GetMedicalCoreByDoctorResponse extends BaseResponse {

    @SerializedName("doctorReference")
    @Expose
    private DoctorCommon doctorReference;
    @SerializedName("medicalCores")
    @Expose
    private List<MedicalCoreCommon> medicalCores = new ArrayList<MedicalCoreCommon>();

    public List<MedicalCoreCommon> getMedicalCores() {
        return medicalCores;
    }

    public void setMedicalCores(List<MedicalCoreCommon> medicalCores) {
        this.medicalCores = medicalCores;
    }

    public DoctorCommon getDoctorReference() {
        return doctorReference;
    }

    public void setDoctorReference(DoctorCommon doctorReference) {
        this.doctorReference = doctorReference;
    }
}
