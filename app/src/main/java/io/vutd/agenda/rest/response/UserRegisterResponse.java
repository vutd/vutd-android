package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 23/06/16.
 */
public class UserRegisterResponse extends BaseResponse{

    @SerializedName("sessionId")
    @Expose
    private String sessionId;

    public UserRegisterResponse() {
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
}
