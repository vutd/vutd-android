package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.ConcurrentCommon;

/**
 * Created by andresmariscal on 02/10/16.
 */

public class CreateConcurrentAppointmentRequest extends BaseRequest {
    @SerializedName("doctorId")
    @Expose
    private long doctorId;
    @SerializedName("medicalCoreId")
    @Expose
    private long medicalCoreId;
    @SerializedName("bookType")
    @Expose
    private long bookType;
    @SerializedName("patientName")
    @Expose
    private String patientName;
    @SerializedName("patientPhone")
    @Expose
    private String patientPhone;
    @SerializedName("concurrent")
    @Expose
    private List<ConcurrentCommon> concurrent = new ArrayList<ConcurrentCommon>();

    /**
     *
     * @return
     * The doctorId
     */
    public long getDoctorId() {
        return doctorId;
    }

    /**
     *
     * @param doctorId
     * The doctorId
     */
    public void setDoctorId(long doctorId) {
        this.doctorId = doctorId;
    }

    /**
     *
     * @return
     * The medicalCoreId
     */
    public long getMedicalCoreId() {
        return medicalCoreId;
    }

    /**
     *
     * @param medicalCoreId
     * The medicalCoreId
     */
    public void setMedicalCoreId(long medicalCoreId) {
        this.medicalCoreId = medicalCoreId;
    }

    /**
     *
     * @return
     * The bookType
     */
    public long getBookType() {
        return bookType;
    }

    /**
     *
     * @param bookType
     * The bookType
     */
    public void setBookType(long bookType) {
        this.bookType = bookType;
    }

    /**
     *
     * @return
     * The patientName
     */
    public String getPatientName() {
        return patientName;
    }

    /**
     *
     * @param patientName
     * The patientName
     */
    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    /**
     *
     * @return
     * The patientPhone
     */
    public String getPatientPhone() {
        return patientPhone;
    }

    /**
     *
     * @param patientPhone
     * The patientPhone
     */
    public void setPatientPhone(String patientPhone) {
        this.patientPhone = patientPhone;
    }

    /**
     *
     * @return
     * The concurrent
     */
    public List<ConcurrentCommon> getConcurrent() {
        return concurrent;
    }

    /**
     *
     * @param concurrent
     * The concurrent
     */
    public void setConcurrent(List<ConcurrentCommon> concurrent) {
        this.concurrent = concurrent;
    }
}
