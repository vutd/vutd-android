package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class DoctorCommon {

    @SerializedName("doctorId")
    @Expose
    private int doctorId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("profilePhoto")
    @Expose
    private String profilePhoto;
    @SerializedName("days")
    @Expose
    private List<WorkdayCommon> days = new ArrayList<WorkdayCommon>();
    @SerializedName("appointments")
    @Expose
    private List<AppointmentCommon> appointments = new ArrayList<AppointmentCommon>();
    @SerializedName("specialities")
    @Expose
    private List<SpecialityCommon> specialities = new ArrayList<SpecialityCommon>();

    /**
     *
     * @return
     * The doctorId
     */
    public int getDoctorId() {
        return doctorId;
    }

    /**
     *
     * @param doctorId
     * The doctorId
     */
    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The profilePhoto
     */
    public String getProfilePhoto() {
        return profilePhoto;
    }

    /**
     *
     * @param profilePhoto
     * The profilePhoto
     */
    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public List<WorkdayCommon> getDays() {
        return days;
    }

    public void setDays(List<WorkdayCommon> days) {
        this.days = days;
    }

    public List<AppointmentCommon> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<AppointmentCommon> appointments) {
        this.appointments = appointments;
    }

    public List<SpecialityCommon> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<SpecialityCommon> specialities) {
        this.specialities = specialities;
    }
}
