package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 02/10/16.
 */

public class DeliveredConfirmationRequest extends BaseRequest {
    @SerializedName("appointmentId")
    @Expose
    private long appointmentId;

    public long getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(long appointmentId) {
        this.appointmentId = appointmentId;
    }
}
