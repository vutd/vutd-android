package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.CreateDoctorProfileRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;


/**
 * Created by andresmariscal on 25/06/16.
 */
public class CreateDoctorProfileHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "CreateDocProfileHandler";

    //Error codes
    private final int SERVER_EXCEPTION          = 200;
    private final int MISSED_FIELDS             = 201;
    private final int PROFESSIONAL_STATEMENT    = 202;
    private final int SCHOOL                    = 203;
    private final int LANGUAGES                 = 204;
    private final int SPECIALITY                = 205;
    private final int INVALID_ROLE              = 206;
    private final int PROFILE_ALREADY_EXISTS    = 207;

    public CreateDoctorProfileHandler(IHandlerResult<Rule, Object> handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case PROFESSIONAL_STATEMENT:
                Logger.d(TAG, "PROFESSIONAL_STATEMENT");
                errors.add(new Rule(Rule.PROFESSIONAL_STATEMENT));
                mHandlerResult.onBadResult(errors);
                break;
            case SCHOOL:
                Logger.d(TAG, "SCHOOL");
                errors.add(new Rule(Rule.SCHOOL));
                mHandlerResult.onBadResult(errors);
                break;
            case LANGUAGES:
                Logger.d(TAG, "LANGUAGES");
                errors.add(new Rule(Rule.LANGUAGES));
                mHandlerResult.onBadResult(errors);
                break;
            case SPECIALITY:
                Logger.d(TAG, "SPECIALITY");
                errors.add(new Rule(Rule.SPECIALITY));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_ROLE:
                Logger.d(TAG, "INVALID_ROLE");
                errors.add(new Rule(Rule.INVALID_DOCTOR_ROLE));
                mHandlerResult.onBadResult(errors);
                break;
            case PROFILE_ALREADY_EXISTS:
                Logger.d(TAG, "PROFILE_ALREADY_EXISTS");
                errors.add(new Rule(Rule.PROFILE_ALREADY_EXISTS));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String professionalStatement,List<String> school, List<Integer> languages,int speciality){
        Map<Rule, String> fieldsRequired = new HashMap<>(4);
        fieldsRequired.put(new Rule(Rule.PROFESSIONAL_STATEMENT), professionalStatement);
        fieldsRequired.put(new Rule(Rule.SCHOOL), school.isEmpty() ? null : "ok");
        fieldsRequired.put(new Rule(Rule.SPECIALITY), speciality == 0 ? null : "ok");
        fieldsRequired.put(new Rule(Rule.LANGUAGES), languages.isEmpty() ? null : "ok");
        return super.validateInputs(fieldsRequired);
    }

    public void addDoctorProfile(CreateDoctorProfileRequest request){

        Set errors = validateInputs(request.getProfessionalStatement(),request.getSchool(),request.getLanguagesSpoken(),
                request.getSpeciality());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }

        mService.debugRquest(request);
        mService.execute(mService.getVutdService().createDoctorProfile(request));
    }
}
