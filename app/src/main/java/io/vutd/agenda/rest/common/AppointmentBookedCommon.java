package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresmariscal on 06/09/16.
 */
public class AppointmentBookedCommon {
    @SerializedName("id")
    @Expose
    private Long id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("locationLat")
    @Expose
    private String locationLat;
    @SerializedName("locationLon")
    @Expose
    private String locationLon;
    @SerializedName("address1")
    @Expose
    private String address1;
    @SerializedName("address2")
    @Expose
    private String address2;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("province")
    @Expose
    private String province;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("intNumber")
    @Expose
    private String intNumber;
    @SerializedName("extNumber")
    @Expose
    private String extNumber;
    @SerializedName("doctorName")
    @Expose
    private String doctorName;
    @SerializedName("doctorPhoto")
    @Expose
    private String doctorPhoto;
    @SerializedName("specialities")
    @Expose
    private List<SpecialityCommon> specialities = new ArrayList<SpecialityCommon>();
    @SerializedName("appointmentDate")
    @Expose
    private String appointmentDate;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("bookTypeId")
    @Expose
    private Long bookTypeId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("mcPhones")
    @Expose
    private List<String> mcPhones = new ArrayList<>();
    @SerializedName("appointmentType")
    @Expose
    private int appointmentType;


    /**
     *
     * @return
     * The id
     */
    public Long getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The description
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @param description
     * The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @return
     * The locationLat
     */
    public String getLocationLat() {
        return locationLat;
    }

    /**
     *
     * @param locationLat
     * The location_lat
     */
    public void setLocationLat(String locationLat) {
        this.locationLat = locationLat;
    }

    /**
     *
     * @return
     * The locationLon
     */
    public String getLocationLon() {
        return locationLon;
    }

    /**
     *
     * @param locationLon
     * The location_lon
     */
    public void setLocationLon(String locationLon) {
        this.locationLon = locationLon;
    }

    /**
     *
     * @return
     * The address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     *
     * @param address1
     * The address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     *
     * @return
     * The address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     *
     * @param address2
     * The address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     *
     * @return
     * The zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     *
     * @param zipcode
     * The zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     *
     * @return
     * The province
     */
    public String getProvince() {
        return province;
    }

    /**
     *
     * @param province
     * The province
     */
    public void setProvince(String province) {
        this.province = province;
    }

    /**
     *
     * @return
     * The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     * The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     * The state
     */
    public String getState() {
        return state;
    }

    /**
     *
     * @param state
     * The state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     *
     * @return
     * The country
     */
    public String getCountry() {
        return country;
    }

    /**
     *
     * @param country
     * The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     *
     * @return
     * The intNumber
     */
    public String getIntNumber() {
        return intNumber;
    }

    /**
     *
     * @param intNumber
     * The int_number
     */
    public void setIntNumber(String intNumber) {
        this.intNumber = intNumber;
    }

    /**
     *
     * @return
     * The extNumber
     */
    public String getExtNumber() {
        return extNumber;
    }

    /**
     *
     * @param extNumber
     * The ext_number
     */
    public void setExtNumber(String extNumber) {
        this.extNumber = extNumber;
    }

    /**
     *
     * @return
     * The doctorName
     */
    public String getDoctorName() {
        return doctorName;
    }

    /**
     *
     * @param doctorName
     * The doctorName
     */
    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    /**
     *
     * @return
     * The doctorPhoto
     */
    public String getDoctorPhoto() {
        return doctorPhoto;
    }

    /**
     *
     * @param doctorPhoto
     * The doctorPhoto
     */
    public void setDoctorPhoto(String doctorPhoto) {
        this.doctorPhoto = doctorPhoto;
    }

    /**
     *
     * @return
     * The specialities
     */
    public List<SpecialityCommon> getSpecialities() {
        return specialities;
    }

    /**
     *
     * @param specialities
     * The specialities
     */
    public void setSpecialities(List<SpecialityCommon> specialities) {
        this.specialities = specialities;
    }

    /**
     *
     * @return
     * The appointmentDate
     */
    public String getAppointmentDate() {
        return appointmentDate;
    }

    /**
     *
     * @param appointmentDate
     * The appointmentDate
     */
    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    /**
     *
     * @return
     * The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     *
     * @return
     * The bookTypeId
     */
    public Long getBookTypeId() {
        return bookTypeId;
    }

    /**
     *
     * @param bookTypeId
     * The bookTypeId
     */
    public void setBookTypeId(Long bookTypeId) {
        this.bookTypeId = bookTypeId;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getMcPhones() {
        return mcPhones;
    }

    public void setMcPhones(List<String> mcPhones) {
        this.mcPhones = mcPhones;
    }

    public int getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(int appointmentType) {
        this.appointmentType = appointmentType;
    }
}
