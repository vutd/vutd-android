package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.GetAppointmentsBookedRequest;
import io.vutd.agenda.rest.response.GetAppointmentsBookedResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 06/09/16.
 */
@EBean
public class GetAppointmentsBookedHandler extends AbstractHandler<GetAppointmentsBookedResponse, Rule> {

    public static final String TAG = "GetAppointmentsBookedHandler";
    //Error codes
    private final int MISSED_FIELDS      = 1601;
    private final int DATE_FILTER        = 1602;

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(GetAppointmentsBookedResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet<>();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case DATE_FILTER:
                Logger.d(TAG, "DATE_FILTER");
                errors.add(new Rule(Rule.DATE_FILTER));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String sessionId, String dateFilter){
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.SESSION_ID), sessionId);
        fieldsRequired.put(new Rule(Rule.DATE_FILTER), dateFilter);
        return super.validateInputs(fieldsRequired);
    }

    public void getAppointments(GetAppointmentsBookedRequest request){
        Set errors = validateInputs(request.getSessionId(), request.getDate());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }

        mService.debugRquest(request);
        mService.execute(mService.getVutdService().getAppointmentsBooked(request));
    }
}
