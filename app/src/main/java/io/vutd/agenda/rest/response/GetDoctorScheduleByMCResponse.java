package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.AppointmentCommon;
import io.vutd.agenda.rest.common.WorkdayCommon;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class GetDoctorScheduleByMCResponse extends BaseResponse {
    @SerializedName("appointments")
    @Expose
    private List<AppointmentCommon> appointmentCommons = new ArrayList<AppointmentCommon>();
    @SerializedName("availableDays")
    @Expose
    private List<WorkdayCommon> days = new ArrayList<WorkdayCommon>();
    /**
     *
     * @return
     * The appointmentCommons
     */
    public List<AppointmentCommon> getAppointmentCommons() {
        return appointmentCommons;
    }

    /**
     *
     * @param appointmentCommons
     * The appointmentCommons
     */
    public void setAppointmentCommons(List<AppointmentCommon> appointmentCommons) {
        this.appointmentCommons = appointmentCommons;
    }

    public List<WorkdayCommon> getDays() {
        return days;
    }

    public void setDays(List<WorkdayCommon> days) {
        this.days = days;
    }
}
