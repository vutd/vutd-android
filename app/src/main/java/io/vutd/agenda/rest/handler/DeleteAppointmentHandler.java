package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.DeleteAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 29/06/16.
 */
@EBean
public class DeleteAppointmentHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "DeleteAppointmentHandler";
    //Error codes
    private final int MISSED_FIELDS             = 901;
    private final int INVALID_APPOINTMENT_ID    = 902;
    private final int INVALID_REASON_ID         = 903;
    private final int USER_UNAUTHORIZED         = 904;

    public DeleteAppointmentHandler(){
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_APPOINTMENT_ID:
                Logger.d(TAG, "INVALID_APPOINTMENT_ID");
                errors.add(new Rule(Rule.INVALID_APPOINTMENT_ID));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_REASON_ID:
                Logger.d(TAG, "INVALID_REASON_ID");
                errors.add(new Rule(Rule.INVALID_REASON_ID));
                mHandlerResult.onBadResult(errors);
                break;
            case USER_UNAUTHORIZED:
                Logger.d(TAG, "USER_UNAUTHORIZED");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     *
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String appointmentId, String reasonId) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.APPOINTMENT_ID), appointmentId);
        fieldsRequired.put(new Rule(Rule.REASON_ID), reasonId);
        return super.validateInputs(fieldsRequired);
    }

    public void deleteAppointment(DeleteAppointmentRequest request){
        try{
            Set errors = validateInputs(String.valueOf(request.getAppointmentId()), String.valueOf(request.getReasonId()));
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }

            mService.debugRquest(request);
            mService.execute(mService.getVutdService().deleteAppointment(request));
        }catch (Exception e){
            Logger.e(TAG, "Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}