package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.CreateMedicalCoreRequest;
import io.vutd.agenda.rest.response.CreateMedicalCoreResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 26/06/16.
 */
public class CreateMedicalCoreHandler extends AbstractHandler<CreateMedicalCoreResponse, Rule> {

    public static final String TAG = "CreateMedicalCoreHandler";

    //Error codes
    public static final int SERVER_EXCEPTION    = 300;
    public static final int MISSED_FIELDS       = 301;

    public static final int NAME                = 302;
    public static final int LAT                 = 303;
    public static final int LON                 = 304;
    public static final int ZIPCODE             = 305;
    public static final int PHONE_NUMBERS       = 306;
    public static final int INVALID_ROLE        = 307;

    public CreateMedicalCoreHandler(IHandlerResult<Rule, CreateMedicalCoreResponse> handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(CreateMedicalCoreResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet<>();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case NAME:
                Logger.d(TAG, "NAME");
                errors.add(new Rule(Rule.NAME));
                mHandlerResult.onBadResult(errors);
                break;
            case LAT:
                Logger.d(TAG, "LAT");
                errors.add(new Rule(Rule.LAT));
                mHandlerResult.onBadResult(errors);
                break;
            case LON:
                Logger.d(TAG, "LON");
                errors.add(new Rule(Rule.LON));
                mHandlerResult.onBadResult(errors);
                break;
            case ZIPCODE:
                Logger.d(TAG, "ZIPCODE");
                errors.add(new Rule(Rule.ZIPCODE));
                mHandlerResult.onBadResult(errors);
                break;
            case PHONE_NUMBERS:
                Logger.d(TAG, "PHONE_NUMBERS");
                errors.add(new Rule(Rule.PHONE_NUMBERS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_ROLE:
                Logger.d(TAG, "INVALID_ROLE");
                errors.add(new Rule(Rule.INVALID_MEDICAL_CORE));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    public Set validateInputs(String name, String lat, String lon, String address1, String zipcode,
                              String province, String city, String state, String country, String numExt,
                              List<String> phones) {
        Map<Rule, String> fieldsRequired = new HashMap<>(11);
        fieldsRequired.put(new Rule(Rule.NAME), name);
        fieldsRequired.put(new Rule(Rule.LAT), lat);
        fieldsRequired.put(new Rule(Rule.LON), lon);
        fieldsRequired.put(new Rule(Rule.ADDRESS), address1);
        fieldsRequired.put(new Rule(Rule.ZIPCODE), zipcode);
        fieldsRequired.put(new Rule(Rule.PROVINCE), province);
        fieldsRequired.put(new Rule(Rule.CITY), city);
        fieldsRequired.put(new Rule(Rule.STATE), state);
        fieldsRequired.put(new Rule(Rule.COUNTRY), country);
        fieldsRequired.put(new Rule(Rule.NUM_EXT), numExt);
        fieldsRequired.put(new Rule(Rule.PHONE_NUMBERS), phones.isEmpty() ? null: "ok");
        return super.validateInputs(fieldsRequired);
    }

    public void createMedicalCore(CreateMedicalCoreRequest request) {
        Set errors = validateInputs(request.getName(), request.getLat(), request.getLon(),
                request.getAddress1(), request.getZipcode(), request.getProvince(), request.getCity(),
                request.getState(), request.getCountry(), request.getNumberExt(), request.getPhoneNumbers());

        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }
        mService.debugRquest(request);
        mService.execute(mService.getVutdService().createMedicalCore(request));
    }


}
