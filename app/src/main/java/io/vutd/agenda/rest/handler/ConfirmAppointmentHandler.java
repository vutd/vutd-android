package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.ConfirmAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 02/10/16.
 */
@EBean
public class ConfirmAppointmentHandler extends AbstractHandler<BaseResponse, Rule> {
    public static final String TAG = "ConfirmAppointmentHandler";
    private final int SERVER_EXCEPTION  = 2200;
    private final int MISSED_FIELDS     = 2201;
    private final int BOOK_TYPE_INVALID = 2202;
    private final int OUT_OF_RANGE      = 2203;
    private final int FCM_FAILURE       = 2204;
    private final int INVALID_USER      = 2205;


    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case BOOK_TYPE_INVALID:
                Logger.d(TAG, "BOOK_TYPE_INVALID");
                errors.add(new Rule(Rule.BOOK_TYPE_INVALID));
                mHandlerResult.onBadResult(errors);
                break;
            case OUT_OF_RANGE:
                Logger.d(TAG, "OUT_OF_RANGE");
                errors.add(new Rule(Rule.OUT_OF_RANGE));
                mHandlerResult.onBadResult(errors);
                break;
            case FCM_FAILURE:
                Logger.d(TAG, "FCM_FAILURE");
                errors.add(new Rule(Rule.FCM_FAILURE));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_USER");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String appointmentId, String bookDate) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.APPOINTMENT_ID), appointmentId);
        fieldsRequired.put(new Rule(Rule.BOOK_DATE), bookDate);
        return super.validateInputs(fieldsRequired);
    }

    public void requestConfirmation(ConfirmAppointmentRequest request){
        try {
            Set errors = validateInputs(String.valueOf(request.getAppointmentId()), request.getBookDate());
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().confirmAppointment(request));
        } catch (Exception e){
            Logger.e(TAG, "createAppointment Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
