package io.vutd.agenda.rest.handler;

import java.util.Set;

/**
 * Created by andresmariscal on 25/06/16.
 */

/**
 *This interface is use to interact with the Android components
 * @param <E> Web service rules
 * @param <T> Object that represent the response from the web service
 */
public interface IHandlerResult <E, T> {
    void onBadResult(Set<E> errors);
    void onOkResult(T result);
}
