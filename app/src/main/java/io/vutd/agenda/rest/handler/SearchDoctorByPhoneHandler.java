package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.SearchDoctorByPhoneRequest;
import io.vutd.agenda.rest.response.SearchDoctorsByPhoneResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 29/08/16.
 */
@EBean
public class SearchDoctorByPhoneHandler extends AbstractHandler<SearchDoctorsByPhoneResponse, Rule> {

    public static final String TAG = "SearchDoctorByPhoneHandler";

    private final int SERVER_EXCEPTION  = 1500;
    private final int MISSED_FIELDS     = 1501;
    private final int INVALID_USER      = 1502;

    public SearchDoctorByPhoneHandler() {
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(SearchDoctorsByPhoneResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()) {
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_USER");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String sessionId, String phone) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.SESSION_ID), sessionId);
        fieldsRequired.put(new Rule(Rule.PHONE), phone);
        return super.validateInputs(fieldsRequired);
    }

    public void searchDoctors(SearchDoctorByPhoneRequest request){
        Set errors = validateInputs(request.getSessionId(), request.getPhone());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }

        mService.debugRquest(request);
        mService.execute(mService.getVutdService().searchDoctorByPhone(request));
    }
}
