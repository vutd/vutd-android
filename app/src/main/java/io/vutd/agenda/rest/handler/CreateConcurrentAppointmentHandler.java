package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.CreateConcurrentAppointmentRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 02/10/16.
 */
@EBean
public class CreateConcurrentAppointmentHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "CreateConcurrentAppointmentHandler";

    private final int SERVER_EXCEPTION          = 1800;
    private final int MISSED_FIELDS             = 1801;
    private final int INVALID_USER              = 1802;
    private final int DOCTOR_NOT_IN_MC          = 1803;
    private final int NOT_AN_ARRAY              = 1804;
    private final int MAX_30                    = 1805;
    private final int DUE_APPOINTMENT           = 1806;
    private final int APPOINTMENT_OUT_OF_RANGE  = 1807;

    public CreateConcurrentAppointmentHandler() {
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_USER");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_NOT_IN_MC:
                Logger.d(TAG, "DOCTOR_NOT_IN_MC");
                errors.add(new Rule(Rule.DOCTOR_NOT_IN_MC));
                mHandlerResult.onBadResult(errors);
                break;
            case NOT_AN_ARRAY:
                Logger.d(TAG, "NOT_AN_ARRAY");
                errors.add(new Rule(Rule.INVALID_FORMAT));
                mHandlerResult.onBadResult(errors);
                break;
            case MAX_30:
                Logger.d(TAG, "MAX_30");
                errors.add(new Rule(Rule.MAX_30));
                mHandlerResult.onBadResult(errors);
                break;
            case DUE_APPOINTMENT:
                Logger.d(TAG, "DUE_APPOINTMENT");
                errors.add(new Rule(Rule.DUE_APPOINTMENT));
                mHandlerResult.onBadResult(errors);
                break;
            case APPOINTMENT_OUT_OF_RANGE:
                Logger.d(TAG, "APPOINTMENT_OUT_OF_RANGE");
                errors.add(new Rule(Rule.APPOINTMENT_OUT_OF_RANGE));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String doctorId, String medicalCoreId, String bookType,
                               String patientName, String patientPhone, List<?> concurrent) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.DOCTOR_ID), doctorId);
        fieldsRequired.put(new Rule(Rule.MEDICAL_CORE_ID), medicalCoreId);
        fieldsRequired.put(new Rule(Rule.MISSED_FIELDS), concurrent.isEmpty() ? null : "ok");
        fieldsRequired.put(new Rule(Rule.BOOK_TYPE), bookType);
        fieldsRequired.put(new Rule(Rule.PATIENT_NAME), patientName);
        fieldsRequired.put(new Rule(Rule.PATIENT_PHONE), patientPhone);
        return super.validateInputs(fieldsRequired);
    }

    public void createConcurrentAppointment(CreateConcurrentAppointmentRequest request) {
        try {
            Set errors = validateInputs(String.valueOf(request.getDoctorId()), String.valueOf(request.getMedicalCoreId()),
                    String.valueOf(request.getBookType()), request.getPatientName(), request.getPatientPhone(), request.getConcurrent());
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().createConcurrentAppointments(request));
        } catch (Exception e){
            Logger.e(TAG, "createAppointment Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
