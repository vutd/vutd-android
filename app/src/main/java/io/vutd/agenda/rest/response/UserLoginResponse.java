package io.vutd.agenda.rest.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

/**
 * Created by andresmariscal on 27/06/16.
 */
@EBean
public class UserLoginResponse extends BaseResponse {
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("roleId")
    @Expose
    private int roleId;
    @SerializedName("resetPassword")
    @Expose
    private int resetPassword;
    @SerializedName("facebookToken")
    @Expose
    private String facebookToken;
    @SerializedName("reference")
    @Expose
    private int reference;

    /**
     *
     * @return
     * The sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     *
     * @param sessionId
     * The sessionId
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }
    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *
     * @param lastName
     * The lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     *
     * @return
     * The phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     * The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The roleId
     */
    public int getRoleId() {
        return roleId;
    }

    /**
     *
     * @param roleId
     * The roleId
     */
    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    /**
     *
     * @return
     * The resetPassword
     */
    public int getResetPassword() {
        return resetPassword;
    }

    /**
     *
     * @param resetPassword
     * The resetPassword
     */
    public void setResetPassword(int resetPassword) {
        this.resetPassword = resetPassword;
    }

    /**
     *
     * @return
     * The facebookToken
     */
    public String getFacebookToken() {
        return facebookToken;
    }

    /**
     *
     * @param facebookToken
     * The facebookToken
     */
    public void setFacebookToken(String facebookToken) {
        this.facebookToken = facebookToken;
    }

    /**
     *
     * @return
     * The reference
     */
    public int getReference() {
        return reference;
    }

    /**
     *
     * @param reference
     * The reference
     */
    public void setReference(int reference) {
        this.reference = reference;
    }
}
