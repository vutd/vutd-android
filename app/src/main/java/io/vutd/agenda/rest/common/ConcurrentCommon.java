package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by andresmariscal on 02/10/16.
 */

public class ConcurrentCommon {
    @SerializedName("bookDate")
    @Expose
    private String bookDate;
    @SerializedName("startTime")
    @Expose
    private String startTime;

    /**
     *
     * @return
     * The bookDate
     */
    public String getBookDate() {
        return bookDate;
    }

    /**
     *
     * @param bookDate
     * The bookDate
     */
    public void setBookDate(String bookDate) {
        this.bookDate = bookDate;
    }

    /**
     *
     * @return
     * The startTime
     */
    public String getStartTime() {
        return startTime;
    }

    /**
     *
     * @param startTime
     * The startTime
     */
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

}
