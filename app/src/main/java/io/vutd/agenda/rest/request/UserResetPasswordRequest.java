package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import io.vutd.agenda.utils.Utils;

/**
 * Created by andresmariscal on 28/06/16.
 */
@EBean
public class UserResetPasswordRequest extends BaseRequest {

    @SerializedName("newPassword")
    @Expose
    private String newPassword;

    @SerializedName("oldPassword")
    @Expose
    private String oldPassword;

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        if(!newPassword.trim().isEmpty())
            this.newPassword = Utils.md5(newPassword);
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        if(!oldPassword.trim().isEmpty())
            this.oldPassword = Utils.md5(oldPassword);
    }
}
