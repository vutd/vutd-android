package io.vutd.agenda.rest.common;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by andresmariscal on 29/08/16.
 */
public class DoctorProfileCommon {
    @SerializedName("doctorId")
    @Expose
    private int doctorId;
    @SerializedName("professionalStatement")
    @Expose
    private String professionalStatement;
    @SerializedName("facebookUrl")
    @Expose
    private String facebookUrl;
    @SerializedName("instagramUrl")
    @Expose
    private String instagramUrl;
    @SerializedName("twitterUrl")
    @Expose
    private String twitterUrl;
    @SerializedName("youtubeUrl")
    @Expose
    private String youtubeUrl;
    @SerializedName("reachOnEmergency")
    @Expose
    private int reachOnEmergency;
    @SerializedName("lastName")
    @Expose
    private String lastName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("profilePhoto")
    @Expose
    private String profilePhoto;
    @SerializedName("medicalCoreId")
    @Expose
    private long medicalCoreId;
    @SerializedName("mcAddress")
    @Expose
    private String mcAddress;
    @SerializedName("specialities")
    @Expose
    private List<SpecialityCommon> specialities = new ArrayList<SpecialityCommon>();

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getProfessionalStatement() {
        return professionalStatement;
    }

    public void setProfessionalStatement(String professionalStatement) {
        this.professionalStatement = professionalStatement;
    }

    public String getFacebookUrl() {
        return facebookUrl;
    }

    public void setFacebookUrl(String facebookUrl) {
        this.facebookUrl = facebookUrl;
    }

    public String getInstagramUrl() {
        return instagramUrl;
    }

    public void setInstagramUrl(String instagramUrl) {
        this.instagramUrl = instagramUrl;
    }

    public String getTwitterUrl() {
        return twitterUrl;
    }

    public void setTwitterUrl(String twitterUrl) {
        this.twitterUrl = twitterUrl;
    }

    public String getYoutubeUrl() {
        return youtubeUrl;
    }

    public void setYoutubeUrl(String youtubeUrl) {
        this.youtubeUrl = youtubeUrl;
    }

    public int getReachOnEmergency() {
        return reachOnEmergency;
    }

    public void setReachOnEmergency(int reachOnEmergency) {
        this.reachOnEmergency = reachOnEmergency;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getProfilePhoto() {
        return profilePhoto;
    }

    public void setProfilePhoto(String profilePhoto) {
        this.profilePhoto = profilePhoto;
    }

    public long getMedicalCoreId() {
        return medicalCoreId;
    }

    public void setMedicalCoreId(long medicalCoreId) {
        this.medicalCoreId = medicalCoreId;
    }

    public String getMcAddress() {
        return mcAddress;
    }

    public void setMcAddress(String mcAddress) {
        this.mcAddress = mcAddress;
    }

    public List<SpecialityCommon> getSpecialities() {
        return specialities;
    }

    public void setSpecialities(List<SpecialityCommon> specialities) {
        this.specialities = specialities;
    }
}
