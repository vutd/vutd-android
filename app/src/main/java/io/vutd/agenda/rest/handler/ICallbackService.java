package io.vutd.agenda.rest.handler;

/**
 * Created by andresmariscal on 25/06/16.
 */
public interface ICallbackService <T> {
    void onSuccess(T response);
    void onError(T response);
    void onFailure(String message);
}
