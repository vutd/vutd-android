package io.vutd.agenda.rest.handler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.AddDoctorToMCConfirmationRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 29/06/16.
 */
public class AddDoctorToMCConfirmationHandler extends AbstractHandler<BaseResponse, Rule> {

    public static final String TAG = "AddDoctorToMCConfirmationHandler";

    //Error Codes
    private final int MISSED_FIELDS      = 1201;
    private final int INVALID_REQUEST_ID = 1202;
    private final int INVALID_USER       = 1203;
    private final int INVALID_INVITATION = 1204;

    public AddDoctorToMCConfirmationHandler(IHandlerResult handlerResult) {
        super(handlerResult);
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(BaseResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_REQUEST_ID:
                Logger.d(TAG, "INVALID_REQUEST_ID");
                errors.add(new Rule(Rule.INVALID_REQUEST_ID));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_DOCTOR");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_INVITATION:
                Logger.d(TAG, "INVALID_INVITATION");
                errors.add(new Rule(Rule.INVALID_INVITATION));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String response, String requestId){
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.RESPONSE), response);
        fieldsRequired.put(new Rule(Rule.REQUEST_ID), requestId);
        return super.validateInputs(fieldsRequired);
    }

    public void confirmation(AddDoctorToMCConfirmationRequest request){
        try{
            Set errors = validateInputs(String.valueOf(request.isResponse()), String.valueOf(request.getRequestId()));
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().addDoctorToMCConfirmation(request));
        }catch (Exception e){
            Logger.e(TAG, "Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
