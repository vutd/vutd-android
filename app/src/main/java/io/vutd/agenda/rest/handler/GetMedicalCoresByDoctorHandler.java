package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.BaseRequest;
import io.vutd.agenda.rest.request.GetMedicalCoreByDoctorRequest;
import io.vutd.agenda.rest.response.GetMedicalCoreByDoctorResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 13/07/16.
 */
@EBean
public class GetMedicalCoresByDoctorHandler extends AbstractHandler<GetMedicalCoreByDoctorResponse, Rule> {

    public static final String TAG = "GetMedicalCoresByDoctorHandler";

    private final int MISSED_FIELDS     = 1301;
    private final int INVALID_USER      = 1302;
    private final int DOCTOR_WITHOUT_MC = 1303;
    private final int DATE_FILTER       = 1304;

    public GetMedicalCoresByDoctorHandler() {
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(GetMedicalCoreByDoctorResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet<>();
        switch (response.getCode()){
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_USER:
                Logger.d(TAG, "INVALID_USER");
                errors.add(new Rule(Rule.INVALID_USER));
                mHandlerResult.onBadResult(errors);
                break;
            case DOCTOR_WITHOUT_MC:
                Logger.d(TAG, "DOCTOR_WITHOUT_MC");
                errors.add(new Rule(Rule.DOCTOR_WITHOUT_MC));
                mHandlerResult.onBadResult(errors);
                break;
            case DATE_FILTER:
                Logger.d(TAG, "DATE_FILTER");
                errors.add(new Rule(Rule.DATE_FILTER));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String sessionId){
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.SESSION_ID), sessionId);
        return super.validateInputs(fieldsRequired);
    }

    public void getMedicalCores(GetMedicalCoreByDoctorRequest request){
        Set errors = validateInputs(request.getSessionId());
        if(!errors.isEmpty()){
            mHandlerResult.onBadResult(errors);
            return;
        }

        mService.debugRquest(request);
        mService.execute(mService.getVutdService().getMedicalCoresByDoctor(request));
    }
}
