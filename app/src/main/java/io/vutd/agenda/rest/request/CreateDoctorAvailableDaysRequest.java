package io.vutd.agenda.rest.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.androidannotations.annotations.EBean;

import java.util.ArrayList;
import java.util.List;

import io.vutd.agenda.rest.common.WorkdayCommon;

/**
 * Created by andresmariscal on 26/06/16.
 */
@EBean
public class CreateDoctorAvailableDaysRequest extends BaseRequest {
    @SerializedName("medicalCoreId")
    @Expose
    private int medicalCoreId;
    @SerializedName("days")
    @Expose
    private List<WorkdayCommon> days = new ArrayList<WorkdayCommon>();

    /**
     *
     * @return
     * The medicalCoreId
     */
    public int getMedicalCoreId() {
        return medicalCoreId;
    }

    /**
     *
     * @param medicalCoreId
     * The medicalCoreId
     */
    public void setMedicalCoreId(int medicalCoreId) {
        this.medicalCoreId = medicalCoreId;
    }

    /**
     *
     * @return
     * The days
     */
    public List<WorkdayCommon> getDays() {
        return days;
    }

    /**
     *
     * @param days
     * The days
     */
    public void setDays(List<WorkdayCommon> days) {
        this.days = days;
    }
}
