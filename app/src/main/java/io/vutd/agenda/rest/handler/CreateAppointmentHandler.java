package io.vutd.agenda.rest.handler;

import org.androidannotations.annotations.EBean;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import io.vutd.agenda.rest.request.CreateAppointmentRequest;
import io.vutd.agenda.rest.response.CreateAppointmentResponse;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 28/06/16.
 */
@EBean
public class CreateAppointmentHandler extends AbstractHandler<CreateAppointmentResponse, Rule> {

    public static final String TAG = "CreateAppointmentHandler";

    //Error Codes
    private final int SERVER_EXCEPTION          = 700;
    private final int MISSED_FIELDS             = 701;
    private final int INVALID_BOOK_TYPE         = 702;
    private final int INVALID_DOCTOR            = 703;
    private final int INVALID_MEDICAL_CORE      = 704;
    private final int DUE_APPOINTMENT           = 705;
    private final int APPOINTMENT_OUT_OF_RANGE  = 706;

    public CreateAppointmentHandler(){
        super();
    }

    @Override
    protected String getTag() {
        return TAG;
    }

    @Override
    protected void onHandlerError(CreateAppointmentResponse response) {
        Logger.d(TAG, "Code: " + response.getCode() + " Error list: " + getErrors(response.getErrors()));
        Set<Rule> errors = new HashSet();
        switch (response.getCode()){
            case SERVER_EXCEPTION:
                Logger.d(TAG, "SERVER_EXCEPTION");
                errors.add(new Rule(Rule.SERVER_EXCEPTION));
                mHandlerResult.onBadResult(errors);
                break;
            case MISSED_FIELDS:
                Logger.d(TAG, "MISSED_FIELDS");
                errors.add(new Rule(Rule.MISSED_FIELDS));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_BOOK_TYPE:
                Logger.d(TAG, "INVALID_BOOK_TYPE");
                errors.add(new Rule(Rule.INVALID_BOOK_TYPE));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_DOCTOR:
                Logger.d(TAG, "INVALID_DOCTOR");
                errors.add(new Rule(Rule.INVALID_DOCTOR));
                mHandlerResult.onBadResult(errors);
                break;
            case INVALID_MEDICAL_CORE:
                Logger.d(TAG, "INVALID_MEDICAL_CORE");
                errors.add(new Rule(Rule.INVALID_MEDICAL_CORE));
                mHandlerResult.onBadResult(errors);
                break;
            case DUE_APPOINTMENT:
                Logger.d(TAG, "DUE_APPOINTMENT");
                errors.add(new Rule(Rule.DUE_APPOINTMENT));
                mHandlerResult.onBadResult(errors);
                break;
            case APPOINTMENT_OUT_OF_RANGE:
                Logger.d(TAG, "APPOINTMENT_OUT_OF_RANGE");
                errors.add(new Rule(Rule.APPOINTMENT_OUT_OF_RANGE));
                mHandlerResult.onBadResult(errors);
                break;
            default:
                Logger.d(TAG, "UNHANDLED_ERROR");
                errors.add(new Rule(Rule.UNHANDLED_ERROR));
                mHandlerResult.onBadResult(errors);
                break;
        }
    }

    /**
     * Validate the entry inputs from the android component
     * @return Set with all the inputs failed
     */
    private Set validateInputs(String doctorId, String medicalCoreId, String bookDate,
                               String startTime, String bookType, String patientName, String patientPhone) {
        Map<Rule, String> fieldsRequired = new HashMap<>();
        fieldsRequired.put(new Rule(Rule.DOCTOR_ID), doctorId);
        fieldsRequired.put(new Rule(Rule.MEDICAL_CORE_ID), medicalCoreId);
        fieldsRequired.put(new Rule(Rule.BOOK_DATE), bookDate);
        fieldsRequired.put(new Rule(Rule.START_TIME), startTime);
        fieldsRequired.put(new Rule(Rule.BOOK_TYPE), bookType);
        fieldsRequired.put(new Rule(Rule.PATIENT_NAME), patientName);
        fieldsRequired.put(new Rule(Rule.PATIENT_PHONE), patientPhone);
        return super.validateInputs(fieldsRequired);
    }

    public void createAppointment(CreateAppointmentRequest request){
        try{
            Set errors = validateInputs(String.valueOf(request.getDoctorId()), String.valueOf(request.getMedicalCoreId()),
                    request.getBookDate(),request.getStartTime(),String.valueOf(request.getBookType()),
                    request.getPatientName(), request.getPatientPhone());
            if(!errors.isEmpty()){
                mHandlerResult.onBadResult(errors);
                return;
            }
            mService.debugRquest(request);
            mService.execute(mService.getVutdService().createAppointment(request));
        }catch (Exception e){
            Logger.e(TAG, "createAppointment Exception: ", e.getCause());
            Set<Rule> errors = new HashSet();
            errors.add(new Rule(Rule.INVALID_FORMAT));
            mHandlerResult.onBadResult(errors);
        }
    }
}
