package io.vutd.agenda.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 03/09/16.
 */
public class Animations {

    public static Animation VUTDfadeInAnimation(Animation.AnimationListener animationListener){
        Animation animation = new AlphaAnimation(0, 1);
        animation.setDuration(500);
        animation.setInterpolator(new LinearInterpolator());
        if(animationListener != null){
            animation.setAnimationListener(animationListener);
        }
        return animation;
    }

    public static void colorTransition(final Context context,
                                       final ImageView imageView,
                                       final int durationMillis,
                                       final int initialState) {

        final int next = initialState + 1;
        final int nextTransition = durationMillis + 10;
        Drawable backgrounds[] = new Drawable[2];
        switch (next){
            case 0:
                backgrounds[0] = ContextCompat.getDrawable(context, R.drawable.transition_step_1);
                backgrounds[1] = ContextCompat.getDrawable(context, R.drawable.transition_step_2);
                break;

            case 1:
                backgrounds[0] = ContextCompat.getDrawable(context, R.drawable.transition_step_2);
                backgrounds[1] = ContextCompat.getDrawable(context, R.drawable.transition_step_3);
                break;

            case 2:
                backgrounds[0] = ContextCompat.getDrawable(context, R.drawable.transition_step_3);
                backgrounds[1] = ContextCompat.getDrawable(context, R.drawable.transition_step_4);
                break;

            case 3:
                backgrounds[0] = ContextCompat.getDrawable(context, R.drawable.transition_step_4);
                backgrounds[1] = ContextCompat.getDrawable(context, R.drawable.transition_step_1);
                break;

            /*case 4:
                backgrounds[0] = ContextCompat.getDrawable(context, R.drawable.transition_step_5);
                backgrounds[1] = ContextCompat.getDrawable(context, R.drawable.transition_step_1);
                break;*/

            default:
                colorTransition(context, imageView, durationMillis, -1);
                return;
        }

        TransitionDrawable transitionDrawable = new TransitionDrawable(backgrounds);
        imageView.setImageDrawable(transitionDrawable);

        transitionDrawable.startTransition(durationMillis);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                imageView.clearAnimation();
                colorTransition(context, imageView, durationMillis, next);
            }
        }, nextTransition);
    }

}
