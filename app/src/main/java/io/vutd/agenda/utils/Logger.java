package io.vutd.agenda.utils;


import io.vutd.agenda.app.AppSettings;

/**
 * Wrapper for the android.util.Logger to allow convenient switching of debug logging.
 */
public class Logger
{
    private static final String DEFAULT_TAG = "Logger";
    private static final int TAG_LENGTH_LIMIT = 23;


    public static void v(String tag, String text, Throwable throwable)
    {
        if (AppSettings.BUILD_CONFIG_DEBUG)
        {
            android.util.Log.v(getProperTag(tag), text, throwable);
        }
    }

    public static void v(String tag, String text)
    {
        v(tag, text, null);
    }

    public static void v(String text)
    {
        v(DEFAULT_TAG, text);
    }

    public static void v(Throwable throwable)
    {
        v(DEFAULT_TAG, null, throwable);
    }

    public static void d(String tag, String text, Throwable throwable)
    {
        if (AppSettings.BUILD_CONFIG_DEBUG)
        {
            android.util.Log.d(getProperTag(tag), text, throwable);
        }
    }

    public static void d(String tag, String text)
    {
        d(tag, text, null);
    }

    public static void d(String text)
    {
        d(DEFAULT_TAG, text);
    }

    public static void d(Throwable throwable)
    {
        d(DEFAULT_TAG, null, throwable);
    }

    public static void e(String tag, String text, Throwable throwable)
    {
        if (AppSettings.BUILD_CONFIG_DEBUG)
        {
            android.util.Log.e(getProperTag(tag), text, throwable);
        }
    }

    public static void e(String tag, String text)
    {
        e(tag, text, null);
    }

    public static void e(String text)
    {
        e(DEFAULT_TAG, text);
    }

    public static void e(Throwable throwable)
    {
        e(DEFAULT_TAG, null, throwable);
    }

    public static void e(String tag, Throwable throwable)
    {
        e(tag, null, throwable);
    }

    public static void w(String tag, String text, Throwable throwable)
    {
        if (AppSettings.BUILD_CONFIG_DEBUG)
        {
            android.util.Log.w(getProperTag(tag), text, throwable);
        }
    }

    public static void w(String tag, String text)
    {
        w(tag, text, null);
    }

    public static void w(String text)
    {
        w(DEFAULT_TAG, text);
    }

    public static void w(Throwable throwable)
    {
        w(DEFAULT_TAG, null, throwable);
    }

    public static void w(String tag, Throwable throwable)
    {
        w(tag, null, throwable);
    }

    public static void i(String tag, String text, Throwable throwable)
    {
        if (AppSettings.BUILD_CONFIG_DEBUG)
        {
            android.util.Log.i(getProperTag(tag), text, throwable);
        }
    }

    public static void i(String tag, String text)
    {
        i(tag, text, null);
    }

    public static void i(String text)
    {
        i(DEFAULT_TAG, text);
    }

    public static void i(Throwable throwable)
    {
        i(DEFAULT_TAG, null, throwable);
    }

    private static String getProperTag(String tag)
    {
        if (tag == null || tag.trim().length() == 0) return DEFAULT_TAG;

        tag = tag.trim();
        if (tag.length() <= TAG_LENGTH_LIMIT) return tag;
        return tag.substring(0, TAG_LENGTH_LIMIT);
    }
}