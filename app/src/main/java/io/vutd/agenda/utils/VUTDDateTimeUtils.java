package io.vutd.agenda.utils;

import android.content.Context;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.format.DateTimeFormat;

import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 24/09/16.
 */
public class VUTDDateTimeUtils {

    public static final String VUTD_DATE_FORMAT = "yyyy-MM-dd";
    public static final String VUTD_MILITARY_TIME_FORMAT = "HH:mm";
    public static final String VUTD_AM_PM_FORMAT = "hh:mm aa";

    /**
     * Create a date time base on the Local Time Zone
     * @return the current date time in the VUTD_DATE_FORMAT
     */
    public static String nowLocal(){
        return convertToVUTDDateFormat(new DateTime());
    }

    /**
     * In this method we take the date time base on the device information and then
     * turn it to UTC Time Zone
     * @return Current date time in UTC Time Zone
     */
    public static String nowUTC(){
        DateTime deviceDateTime = new DateTime();
        DateTime utcDateTime = deviceDateTime.toDateTime(DateTimeZone.UTC);
        return convertToVUTDDateFormat(utcDateTime);
    }

    /**
     * In VUTD the standard format for a date is yyyy-MM-dd, so this method is the base of all
     * the conversions from <p>DateTime</p> to <p>String</p>
     * @param dateTime The given Date to parse
     * @return the Give date on the following format yyyy-MM-dd
     */
    public static String convertToVUTDDateFormat(DateTime dateTime){
        return dateTime.toString(DateTimeFormat.forPattern(VUTD_DATE_FORMAT));
    }

    /**
     * Change Time Zone to UTC
     * @param dateTime date time with the local Time Zone
     * @return the give dateTime in UTC Time Zone
     */
    public static String convertToUTC(DateTime dateTime){
        return convertToVUTDDateFormat(dateTime.toDateTime(DateTimeZone.UTC));
    }

    /**
     * Change Time Zone from UTC to Local
     * @param dateTime given date time in UTC Time Zone
     * @return dateTime in Local Time Zone
     */
    public static String convertUTCToLocal(String dateTime){
        if(dateTime.contains("00:00:00")) {
            dateTime = dateTime.replace("00:00:00", "").trim();
        }
        DateTime utcDateTime = DateTime.parse(dateTime, DateTimeFormat.forPattern(VUTD_DATE_FORMAT));
        long localMillis = DateTimeZone.getDefault().convertUTCToLocal(utcDateTime.getMillis());
        return convertToVUTDDateFormat(new DateTime(localMillis));
    }

    /**
     * Parse a given date whit the format hh:mm aa to a military time (HH:mm)
     * @param amPmTime the given time in hh:mm aa format
     * @return military time
     */
    public static String changeAmPmToMilitary(String amPmTime){
        return DateTime.parse(amPmTime, DateTimeFormat.forPattern(VUTD_AM_PM_FORMAT))
                .toString(DateTimeFormat.forPattern(VUTD_MILITARY_TIME_FORMAT));
    }

    public static String changeMilitaryToAmPm(String military){
        return DateTime.parse(military, DateTimeFormat.forPattern(VUTD_MILITARY_TIME_FORMAT))
                .toString(DateTimeFormat.forPattern(VUTD_AM_PM_FORMAT));
    }

    public static int getMinuteOfDay(String duration){
         return DateTime.parse(
                 duration,
                DateTimeFormat.forPattern(VUTD_MILITARY_TIME_FORMAT)
         ).getMinuteOfDay();
    }

    public static DateTime parseForPattern(String date, String pattern){
        return DateTime.parse(date, DateTimeFormat.forPattern(pattern));
    }

    public static String parseForPattern(DateTime date, String pattern){
        return date.toString(DateTimeFormat.forPattern(pattern));
    }

    public static String reformatForPattern(String date, String oldPattern, String newPattern){
       DateTime dateTime = parseForPattern(date, oldPattern);
        return parseForPattern(dateTime, newPattern);
    }

    /**
     * Converts 239892334 to "8 min ago"
     */
    public static String timestampToCondensedDate(long timestamp)
    {
        return android.text.format.DateUtils.getRelativeTimeSpanString(timestamp,
                System.currentTimeMillis(),
                android.text.format.DateUtils.SECOND_IN_MILLIS).toString();
    }

    /**
     * Converts 239892334 to "8 min ago"
     */
    public static String timestampToCondensedDate(String dateTime) {
        DateTime date = parseForPattern(dateTime, VUTD_DATE_FORMAT);
        return timestampToCondensedDate(date.getMillis());
    }

    public static  String notificationDateFormat(String date, Context context){
        DateTime dateTime = parseForPattern(date, VUTD_DATE_FORMAT);
        DateTime now = new DateTime();
        DateTime tw = now.plusDays(1);

        if(date.equals(parseForPattern(now, VUTD_DATE_FORMAT))) {
            return context.getString(R.string.today);
        } else if(date.equals(parseForPattern(tw, VUTD_DATE_FORMAT))){
            return context.getString(R.string.tomorrow);
        } else {
            String shortDate = "";
            switch (dateTime.getMonthOfYear()) {
                case 1:shortDate = context.getString(R.string.short_january);break;
                case 2:shortDate = context.getString(R.string.short_february);break;
                case 3:shortDate = context.getString(R.string.short_march);break;
                case 4:shortDate = context.getString(R.string.short_april);break;
                case 5:shortDate = context.getString(R.string.short_may);break;
                case 6:shortDate = context.getString(R.string.short_june);break;
                case 7:shortDate = context.getString(R.string.short_july);break;
                case 8:shortDate = context.getString(R.string.short_august);break;
                case 9:shortDate = context.getString(R.string.short_september);break;
                case 10:shortDate = context.getString(R.string.short_october);break;
                case 11:shortDate = context.getString(R.string.short_november);break;
                case 12:shortDate = context.getString(R.string.short_december);break;
            }
             return context.getString(R.string.on, dateTime.getDayOfMonth() + "", shortDate);
        }
    }

    /**
     *  This method convert the day of week to a full day name
     * @param dayOfWeek the weekday number where Monday = 1 and Sunday = 7
     * @param context The context
     * @return The week day name
     */
    public static String getWeekday(int dayOfWeek, Context context) {
        String weekdayName;
        switch (dayOfWeek) {
            case 1:
                weekdayName = context.getString(R.string.monday);
                break;
            case 2:
                weekdayName = context.getString(R.string.tuesday);
                break;
            case 3:
                weekdayName = context.getString(R.string.wednesday);
                break;
            case 4:
                weekdayName = context.getString(R.string.thursday);
                break;
            case 5:
                weekdayName = context.getString(R.string.friday);
                break;
            case 6:
                weekdayName = context.getString(R.string.saturday);
                break;
            case 7:
                weekdayName = context.getString(R.string.sunday);
                break;
            default:
                weekdayName = "invalid";
                break;
        }
        return weekdayName;
    }

    /**
     * This method convert the month of the year to a full month name
     * @param monthOfYear the corresponding month number
     * @param context The context
     * @return The full month name
     */
    public static String getMonth(int monthOfYear, Context context){
        String monthName;
        switch (monthOfYear) {
            case 1:
                monthName = context.getString(R.string.january);
                break;
            case 2:
                monthName = context.getString(R.string.february);
                break;
            case 3:
                monthName = context.getString(R.string.march);
                break;
            case 4:
                monthName = context.getString(R.string.april);
                break;
            case 5:
                monthName = context.getString(R.string.may);
                break;
            case 6:
                monthName = context.getString(R.string.june);
                break;
            case 7:
                monthName = context.getString(R.string.july);
                break;
            case 8:
                monthName = context.getString(R.string.august);
                break;
            case 9:
                monthName = context.getString(R.string.september);
                break;
            case 10:
                monthName = context.getString(R.string.october);
                break;
            case 11:
                monthName = context.getString(R.string.november);
                break;
            case 12:
                monthName = context.getString(R.string.december);
                break;
            default:
                monthName = "invalid";
                break;
        }
        return monthName;
    }

    public static String getMonthShort(int monthOfYear, Context context) {
        String monthShort;
        switch (monthOfYear) {
            case 1:
                monthShort = context.getString(R.string.short_january);
                break;
            case 2:
                monthShort = context.getString(R.string.short_february);
                break;
            case 3:
                monthShort = context.getString(R.string.short_march);
                break;
            case 4:
                monthShort = context.getString(R.string.short_april);
                break;
            case 5:
                monthShort = context.getString(R.string.short_may);
                break;
            case 6:
                monthShort = context.getString(R.string.short_june);
                break;
            case 7:
                monthShort = context.getString(R.string.short_july);
                break;
            case 8:
                monthShort = context.getString(R.string.short_august);
                break;
            case 9:
                monthShort = context.getString(R.string.short_september);
                break;
            case 10:
                monthShort = context.getString(R.string.short_october);
                break;
            case 11:
                monthShort = context.getString(R.string.short_november);
                break;
            case 12:
                monthShort = context.getString(R.string.short_december);
                break;
            default:
                monthShort = "invalid";
        }
        return monthShort;
    }

}
