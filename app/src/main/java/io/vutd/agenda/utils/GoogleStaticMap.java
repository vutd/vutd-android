package io.vutd.agenda.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import java.util.ArrayList;

/**
 * Created by andresmariscal on 11/09/16.
 */
public class GoogleStaticMap {
    private static final String API_KEY = "AIzaSyB9lbPHVIt5arRo1-N9Sbc2chrrLZQd3Nk";
    private String mHeight;
    private String mWidth;
    private String mMapType;
    private String mZoom;
    private String mScale;
    private ArrayList<GoogleMarker> mMarkers = new ArrayList<>();
    private Uri.Builder mURIBuilder = new Uri.Builder();

    private GoogleStaticMap(Builder builder){
        mURIBuilder.scheme("https")
                .authority("maps.googleapis.com")
                .appendPath("maps")
                .appendPath("api")
                .appendPath("staticmap")
                .appendQueryParameter("key",API_KEY);
        this.mHeight = builder.mHeight;
        this.mWidth = builder.mWidth;
        this.mMapType = builder.mMapType;
        this.mZoom = builder.mZoom;
        this.mScale = builder.mScale;
        this.mMarkers = builder.mMarkers;
    }

    /**
     * TODO: Improve this method, remove concatenations
     * @param lat the lat
     * @param lon the lon
     * @param context the context
     */
    public static void showMap(String lat, String lon, String titleMap, Context context){
        //http://maps.google.com/maps?q=-37.866963,144.980615
        Uri gmmIntentUri = Uri.parse("geo:0,0?q="+ lat +","+ lon + "(" + titleMap + ")" + "&z=12");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(mapIntent);
        } else {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?q=" + lat + "," + lon)));
        }
    }

    /**
     * TODO: this method needs to be improve. Markers!
     * @return Google static map's URL
     */
    public String getUrl(){

        if(!mMarkers.isEmpty()){
            for(GoogleMarker marker : mMarkers){
                mURIBuilder.appendQueryParameter("markers",
                        "color:" + marker.getColor() + "|" +
                        "size:" + marker.getSize() + "|" +
                        marker.getLat()+ "," + marker.getLon());
            }

            if(!mHeight.isEmpty() && !mWidth.isEmpty()){
                mURIBuilder.appendQueryParameter("size", mHeight + "x" + mWidth);
            }

            if(!mMapType.isEmpty()){
                mURIBuilder.appendQueryParameter("maptype", mMapType);
            }

            if(!mZoom.isEmpty()){
                mURIBuilder.appendQueryParameter("zoom", mZoom);
            }

            if(!mScale.isEmpty()){
                mURIBuilder.appendQueryParameter("scale", mScale);
            }

        }

        return mURIBuilder.build().toString();
    }

    public static class Builder {
        private String mScale = "2";
        private String mHeight = "640";
        private String mWidth = "640";
        private String mMapType = "roadmap";
        private String mZoom = "";
        private ArrayList<GoogleMarker> mMarkers = new ArrayList<>();

        public Builder setScale(String scale){
            this.mScale = scale;
            return this;
        }

        public Builder setHeigth(String height){
            this.mHeight = height;
            return this;
        }

        public Builder setWidth(String width){
            this.mWidth = width;
            return this;
        }

        public Builder setMapType(String mapType){
            this.mMapType = mapType;
            return this;
        }

        public Builder setZoom(String zoom){
            this.mZoom = zoom;
            return this;
        }

        public Builder addMarker(GoogleMarker marker){
            this.mMarkers.add(marker);
            return this;
        }

        public GoogleStaticMap build(){
            return new GoogleStaticMap(this);
        }
    }

    public static class GoogleMarker {
        private String mLat = "19.339977";
        private String mLon = "-99.190645";
        private String mColor = "red";
        private String mSize = "";
        private String mLabel = "";

        public String getLat() {
            return mLat;
        }

        public void setLat(String mLat) {
            this.mLat = mLat;
        }

        public String getLon() {
            return mLon;
        }

        public void setLon(String mLon) {
            this.mLon = mLon;
        }

        public String getColor() {
            return mColor;
        }

        public void setColor(String mColor) {
            this.mColor = mColor;
        }

        public String getSize() {
            return mSize;
        }

        public void setSize(String mSize) {
            this.mSize = mSize;
        }

        public String getLabel() {
            return mLabel;
        }

        public void setLabel(String mLabel) {
            this.mLabel = mLabel;
        }
    }
}
