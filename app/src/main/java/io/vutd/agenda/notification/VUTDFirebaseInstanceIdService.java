package io.vutd.agenda.notification;


import android.os.Build;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;

import java.util.Set;

import io.realm.Realm;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.RegisterPushNotificationTokenHandler;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.request.RegisterPushNotificationTokenRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.Utils;

/**
 * Created by andresmariscal on 24/09/16.
 */
@EService
public class VUTDFirebaseInstanceIdService extends FirebaseInstanceIdService {

    public static final String TAG = "FirebaseInstanceIdService";

    @Bean
    RegisterPushNotificationTokenHandler mHandler;

    @AfterInject
    void afterInjects(){
        Logger.d(TAG, "afterInjects()");
        mHandler.setHandlerResult(mRegisterPushNotificationTokenCallback);
    }

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Logger.d(TAG, "Refreshed token: " + refreshedToken);

        Realm realm = Realm.getDefaultInstance();
        CurrentUser currentUser = realm.where(CurrentUser.class).findFirst();
        if(currentUser != null){
            RegisterPushNotificationTokenRequest request = new RegisterPushNotificationTokenRequest();
            request.setSessionId(currentUser.sessionId);
            request.setPushNotificationToken(refreshedToken);
            request.setDeviceOs("Android " + Build.VERSION.SDK_INT);
            request.setDeviceName(Utils.getDeviceName());
            request.setDeviceModel(Build.MODEL);
            mHandler.registerToken(request);
        }
    }

    private IHandlerResult<Rule, BaseResponse> mRegisterPushNotificationTokenCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            //TODO: Use Event bus to notify rather the splash or Login/Register
            String tmp = "";
            for(Rule rule : errors){
                tmp += " " + getApplicationContext().getString(rule.getErrorStringId());
            }
            Logger.e(TAG, "onBadResult()" + tmp);
        }

        @Override
        public void onOkResult(BaseResponse result) {
            //TODO: Use Event bus to notify rather the splash or Login/Register
            Logger.e(TAG, "onOkResult()");
        }
    };
}
