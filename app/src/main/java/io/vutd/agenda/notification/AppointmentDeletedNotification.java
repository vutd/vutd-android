package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 28/09/16.
 */

public class AppointmentDeletedNotification extends AbstractPushNotification {

    public AppointmentDeletedNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        //TODO: save on notification table
        NotificationItem data = new NotificationItem(remoteMessage.getData());
        if(display) {
            String contentText;
            if(data.time.equals("13:00")){
                contentText =  mContext.getString(
                        R.string.push_notification_cancelled_appointment_1,
                        data.whoCancel,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.time));
            }else {
                contentText =  mContext.getString(
                        R.string.push_notification_cancelled_appointment,
                        data.whoCancel,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.time));
            }
            sendNotification(contentText, SplashActivity_.class);
        }
    }

    private class NotificationItem {
        String owner;
        String doctorName;
        String whoCancel;
        String mcName;
        String time;
        String date;

        public NotificationItem(Map<String, String> data) {
            this.owner = data.get("owner");
            this.doctorName = data.get("doctorName");
            this.whoCancel = data.get("whoCancel");
            this.mcName = data.get("mcName");
            this.time = data.get("time");
            this.date = data.get("date");
        }
    }
}
