package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 03/10/16.
 */

public class NewSubscriptionNotification extends AbstractPushNotification {

    public static final String TAG = "NewSubscriptionNotification";

    public NewSubscriptionNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        try{
            long appointmentId = Long.valueOf(remoteMessage.getData().get("appointmentId"));
            //TODO: get appointment's information from database

        } catch (Exception e ){
            Logger.e(TAG, "Long.valueOf()", e.getCause());
        }
    }
}
