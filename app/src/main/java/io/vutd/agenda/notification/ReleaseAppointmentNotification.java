package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 03/10/16.
 */

public class ReleaseAppointmentNotification extends AbstractPushNotification {

    public static final String TAG = "ReleaseAppointmentNotification";

    public ReleaseAppointmentNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        NotificationItem data = new NotificationItem(remoteMessage.getData());
        if (display) {
            String contentText;
            if (data.startTime.equals("13:00")) {
                contentText = mContext.getString(
                        R.string.push_notification_release_appointment_1,
                        data.doctorName,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            } else {
                contentText = mContext.getString(
                        R.string.push_notification_release_appointment,
                        data.doctorName,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            }
            sendNotification(contentText, SplashActivity_.class);
        }
    }

    private class NotificationItem {
        String doctorName;
        String mcName;
        String startTime;
        String date;

        NotificationItem(Map<String, String> data) {
            this.doctorName = data.get("doctorName");
            this.mcName = data.get("mcName");
            this.startTime = data.get("startTime");
            this.date = data.get("date");
        }
    }
}
