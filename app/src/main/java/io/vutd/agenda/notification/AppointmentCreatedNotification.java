package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 26/09/16.
 */

public class AppointmentCreatedNotification extends AbstractPushNotification {

    public static final String TAG = "AppointmentCreatedNotification";

    public AppointmentCreatedNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        //TODO: save in notification table.
        //TODO:: on English looks bad, i know you are tired but you have to deliver good code
        Map<String, String> data = remoteMessage.getData();
        if(display){
            String time = data.get("time");
            String date = data.get("date");
            String contentText;
            if(time.equals("13:00")){
                contentText =  mContext.getString(
                        R.string.push_notification_new_appointment_1,
                        data.get("patientName"),
                        VUTDDateTimeUtils.notificationDateFormat(date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(time));
            }else {
                contentText =  mContext.getString(
                        R.string.push_notification_new_appointment,
                        data.get("patientName"),
                        VUTDDateTimeUtils.notificationDateFormat(date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(time));
            }
            sendNotification(contentText, SplashActivity_.class);
        }
    }
}
