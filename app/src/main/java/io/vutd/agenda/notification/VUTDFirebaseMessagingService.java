package io.vutd.agenda.notification;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import io.realm.Realm;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 24/09/16.
 */
public class VUTDFirebaseMessagingService extends FirebaseMessagingService {

    public static final String TAG = "FirebaseMessagingService";
    private static final int APPOINTMENT_CREATED            = 1;
    private static final int APPOINTMENT_CANCELLED          = 2;
    private static final int CONFIRMATION_REQUESTED         = 3;
    private static final int CONFIRMATION_REQUEST_DELIVERED = 4;
    private static final int NEW_SUBSCRIPTION               = 5;
    private static final int RELEASE_APPOINTMENT            = 6;
    private static final int CONFIRM_APPOINTMENT            = 7;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logger.d(TAG, "From: " + remoteMessage.getFrom());
        if(remoteMessage.getNotification() != null){
            super.onMessageReceived(remoteMessage);
        }

        if(remoteMessage.getData() != null && !remoteMessage.getData().isEmpty()) {
            Logger.d(TAG, "Notification Message Body: " + remoteMessage.getData().toString());
            AbstractPushNotification pushNotification;
            int functionId = Integer.valueOf(remoteMessage.getData().get("function_id"));
            switch (functionId) {
                case APPOINTMENT_CREATED:
                    pushNotification = new AppointmentCreatedNotification();
                    break;
                case APPOINTMENT_CANCELLED:
                    pushNotification = new AppointmentDeletedNotification();
                    break;
                case CONFIRMATION_REQUESTED:
                    pushNotification = new ConfirmationRequestNotification();
                    break;
                case CONFIRMATION_REQUEST_DELIVERED:
                    pushNotification = new ConfirmationRequestDeliveredNotification();
                    break;
                case NEW_SUBSCRIPTION:
                    pushNotification = new NewSubscriptionNotification();
                    break;
                case RELEASE_APPOINTMENT:
                    pushNotification = new ReleaseAppointmentNotification();
                    break;
                case CONFIRM_APPOINTMENT:
                    pushNotification = new ConfirmAppointmentNotification();
                    break;
                default: return;
            }
            Realm realm = Realm.getDefaultInstance();
            CurrentUser currentUser = realm.where(CurrentUser.class).findFirst();
            if(currentUser != null) {
                pushNotification.setContext(this);
                pushNotification.onMessageReceived(remoteMessage);
            } else {
                Logger.d(TAG, "onMessageReceived() user logged out");
            }
            realm.close();
        }
    }
}
