package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;
import java.util.Set;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.realm.model.CurrentUser;
import io.vutd.agenda.rest.handler.DeliveredConfirmationRequestHandler;
import io.vutd.agenda.rest.handler.DeliveredConfirmationRequestHandler_;
import io.vutd.agenda.rest.handler.IHandlerResult;
import io.vutd.agenda.rest.handler.Rule;
import io.vutd.agenda.rest.request.DeliveredConfirmationRequest;
import io.vutd.agenda.rest.response.BaseResponse;
import io.vutd.agenda.utils.Logger;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 03/10/16.
 */

public class ConfirmationRequestNotification extends AbstractPushNotification {

    public static final String TAG = "ConfirmationRequestNotification";

    public ConfirmationRequestNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        NotificationItem data = new NotificationItem(remoteMessage.getData());
        if(display){
            String contentText;
            if(data.startTime.equals("13:00")){
                contentText =  mContext.getString(
                        R.string.push_notification_request_confirmation_appointment_1,
                        data.doctorName,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            }else {
                contentText =  mContext.getString(
                        R.string.push_notification_request_confirmation_appointment,
                        data.doctorName,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            }
            sendNotification(contentText, SplashActivity_.class);
        }

        //This service is used like the double check marks in facebook, notify to the doctor that the request has received
        DeliveredConfirmationRequestHandler handler = DeliveredConfirmationRequestHandler_.getInstance_(mContext);
        handler.setHandlerResult(mDeliveredRequestCallback);
        CurrentUser currentUser = mRealm.where(CurrentUser.class).findFirst();
        DeliveredConfirmationRequest request = new DeliveredConfirmationRequest();
        request.setSessionId(currentUser.sessionId);
        request.setAppointmentId(data.appointmentId);
        handler.deliveredConfirmationRequest(request);
    }

    private IHandlerResult<Rule, BaseResponse> mDeliveredRequestCallback
            = new IHandlerResult<Rule, BaseResponse>() {
        @Override
        public void onBadResult(Set<Rule> errors) {
            Logger.d(TAG, "onBadResult");
        }

        @Override
        public void onOkResult(BaseResponse result) {
            Logger.d(TAG, "onOkResult");
        }
    };

    private class NotificationItem {
        long appointmentId;
        String doctorName;
        String mcName;
        String startTime;
        String date;

        NotificationItem(Map<String, String> data) {
            try {
                this.appointmentId = Long.valueOf(data.get("appointmentId"));
                this.doctorName = data.get("doctorName");
                this.mcName = data.get("mcName");
                this.startTime = data.get("time");
                this.date = data.get("date");
            } catch (Exception e){
                Logger.e("ConfirmationRequestNotification", "Long.valueOf()", e.getCause());
            }
        }
    }
}
