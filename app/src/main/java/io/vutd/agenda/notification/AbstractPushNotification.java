package io.vutd.agenda.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;

import com.google.firebase.messaging.RemoteMessage;

import io.realm.Realm;
import io.vutd.agenda.R;

/**
 * Created by andresmariscal on 26/09/16.
 */

public abstract class AbstractPushNotification {

    private static final String DISPLAY = "display";
    protected Context mContext;
    protected Realm mRealm;

    public AbstractPushNotification() {
        mRealm = Realm.getDefaultInstance();
    }

    public abstract int getRequestCode();
    protected abstract void processNotification(RemoteMessage remoteMessage, boolean display);

    public void setContext(Context mContext) {
        this.mContext = mContext;
    }

    public void onMessageReceived(RemoteMessage remoteMessage){
        processNotification(remoteMessage, Boolean.valueOf(remoteMessage.getData().get(DISPLAY)));
        if(mRealm != null && !mRealm.isClosed()){
            mRealm.close();
        }
    }

    protected void sendNotification(String contentText, Class activityHandler)
    {
        Intent intent = new Intent(mContext, activityHandler);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(mContext, getRequestCode(), intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(mContext)
                .setSmallIcon(getNotificationIcon())
                .setColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                .setContentTitle(mContext.getString(R.string.app_name))
                .setContentText(contentText)
                .setAutoCancel(true)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(contentText))
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(getRequestCode(), notificationBuilder.build());
    }

    private int getNotificationIcon()
    {
        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon
                ? R.drawable.ic_notification
                : R.mipmap.ic_launcher;
    }
}
