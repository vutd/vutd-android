package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import io.vutd.agenda.R;
import io.vutd.agenda.controller.activity.SplashActivity_;
import io.vutd.agenda.utils.VUTDDateTimeUtils;

/**
 * Created by andresmariscal on 26/10/16.
 */

public class ConfirmAppointmentNotification extends AbstractPushNotification {

    public static final String TAG = "ConfirmAppointmentNotification";

    public ConfirmAppointmentNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        NotificationItem data = new NotificationItem(remoteMessage.getData());
        if (display) {
            String contentText;
            if(data.startTime.equals("13:00")){
                contentText =  mContext.getString(
                        R.string.push_notification_appointment_confirmed_1,
                        data.whoConfirm,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            }else {
                contentText =  mContext.getString(
                        R.string.push_notification_appointment_confirmed,
                        data.whoConfirm,
                        VUTDDateTimeUtils.notificationDateFormat(data.date, mContext),
                        VUTDDateTimeUtils.changeMilitaryToAmPm(data.startTime));
            }
            sendNotification(contentText, SplashActivity_.class);
        }
    }

    private class NotificationItem {
        String owner;
        String doctorName;
        String whoConfirm;
        String mcName;
        String startTime;
        String date;

        NotificationItem(Map<String, String> data) {
            this.owner = data.get("owner");
            this.doctorName = data.get("doctorName");
            this.whoConfirm = data.get("whoConfirm");
            this.mcName = data.get("mcName");
            this.startTime = data.get("startTime");
            this.date = data.get("date");
        }
    }
}
