package io.vutd.agenda.notification;

import com.google.firebase.messaging.RemoteMessage;

import org.greenrobot.eventbus.EventBus;

import io.vutd.agenda.eventbus.DoubleCheck;
import io.vutd.agenda.realm.viewmodel.Appointment;
import io.vutd.agenda.utils.Logger;

/**
 * Created by andresmariscal on 03/10/16.
 */

public class ConfirmationRequestDeliveredNotification extends AbstractPushNotification {

    public static final String TAG = "ConfirmationRequestDeliveredNotification";

    public ConfirmationRequestDeliveredNotification() {
        super();
    }

    @Override
    public int getRequestCode() {
        return 0;
    }

    @Override
    protected void processNotification(RemoteMessage remoteMessage, boolean display) {
        try{
            int appointmentId = Integer.valueOf(remoteMessage.getData().get("appointmentId"));
            mRealm.beginTransaction();
            Appointment appointment = mRealm.where(Appointment.class)
                    .equalTo("id", appointmentId)
                    .findFirst();
            appointment.isConfirmationDelivered = 1;
            mRealm.commitTransaction();
            EventBus.getDefault().post(new DoubleCheck(appointment.id));
        } catch (Exception e ){
            Logger.e(TAG, "Long.valueOf()", e.getCause());
        }
    }

}
